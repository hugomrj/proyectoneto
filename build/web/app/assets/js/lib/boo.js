let boo = {
    
    main: 'idmain' ,   
    
    mostrarAlerta: function (message, type) {

        // Crea un elemento de notificación de Bootstrap dinámicamente        
        var alertDiv = document.createElement('div');
        alertDiv.id = 'idalerta';  
        alertDiv.className = 'alert fade show ' + type + ' alert-dismissible'; // Agrega las clases necesarias
        alertDiv.setAttribute('role', 'alert');
    
        // Div para el mensaje
        var messageDiv = document.createElement('div');
        messageDiv.className = 'pe-5'
        messageDiv.innerHTML = message;
        alertDiv.appendChild(messageDiv);
    
        // Div para el botón de cerrar
        var closeButton = document.createElement('button');
        closeButton.type = 'button';
        closeButton.className = 'btn-close';
        closeButton.setAttribute('data-bs-dismiss', 'alert');
        closeButton.setAttribute('aria-label', 'Close');
        alertDiv.appendChild(closeButton);
            

        // Busca el elemento por su id
        var alertaElemento = document.getElementById(alertDiv.id);
        // Verifica si el elemento existe antes de intentar eliminarlo
        if (alertaElemento) {
            alertaElemento.remove();
        }

        // Se agrega clases de posicionamiento
        alertDiv.classList.add('position-absolute');             
        alertDiv.classList.add('bottom-0', 'start-0');
        alertDiv.classList.add('m-3');


        document.body.appendChild(alertDiv);

        // Agrega estilos para posicionar la alerta en la parte inferior izquierda
        /*
        alertDiv.style.position = 'fixed';
        alertDiv.style.bottom = '10px';
        alertDiv.style.left = '10px';
        */

        // Muestra la notificación
        var bootstrapAlert = new bootstrap.Alert(alertDiv);
    

    },
    




    mostrarToast : function (message, type_message) {

        // Crea un elemento de toast de Bootstrap dinámicamente
        let toastDiv = document.createElement('div');
        toastDiv.className = 'toast align-items-center text-white border-0 bg-light ' + type_message;

        // Aplica el color de fondo directamente con JavaScript
        

    
        let toastInnerDiv = document.createElement('div');
        toastInnerDiv.className = 'd-flex  bg-'+type_message;

        /*toastInnerDiv.style.backgroundColor = 'rgb(220, 53, 69)'; */

        
    
        // Div para el cuerpo del toast
        let toastBodyDiv = document.createElement('div');
        toastBodyDiv.className = 'toast-body bg-transparent';
        toastBodyDiv.innerHTML = message;
        toastInnerDiv.appendChild(toastBodyDiv);
    
        // Botón de cerrar
        let closeButton = document.createElement('button');
        closeButton.type = 'button';
        closeButton.className = 'btn-close btn-close-white me-2 m-auto';
        closeButton.setAttribute('data-bs-dismiss', 'toast');
        closeButton.setAttribute('aria-label', 'Close');
        toastInnerDiv.appendChild(closeButton);
    
        // Agrega el div interior al div de toast
        toastDiv.appendChild(toastInnerDiv);
    
        // Agrega estilos para posicionar el toast en la parte inferior derecha
        toastDiv.style.position = 'fixed';
        toastDiv.style.bottom = '10px';
        toastDiv.style.right = '10px';
    
        // Agrega el elemento de toast al cuerpo del documento
        document.body.appendChild(toastDiv);
    
        // Crea y muestra el toast
        var toast = new bootstrap.Toast(toastDiv);
        toast.show();
    },
    
    
    
    loader : {    
        dom: null,
        id:  "loader_id", 
        count: 0,

        inicio : function  ()
        {


            if ( this.count == 0 ){

                var loade = document.createElement("div");
                loade.setAttribute("id", this.id );
                loade.classList.add('loader');



                var loader_container = document.createElement("div");
                loader_container.classList.add('loader-container');

    /*
                var semicircle1 = document.createElement("div");
                semicircle1.classList.add('semicircle1');
    */
                var semicircle2 = document.createElement("div");
                //semicircle2.classList.add('semicircle2');
                semicircle2.classList.add('loader1');

                //loader_container.appendChild(semicircle1);
                loader_container.appendChild(semicircle2);

                loade.appendChild(loader_container);

                document.body.appendChild(loade);

                this.dom = loade;   

            }
            this.count ++;
        },


        fin : function  ()
        {        
            if (this.count == 1) {
                document.body.removeChild( this.dom );
            }
            this.count --;        
        }    

    },


    vista : {   
            
        lista: function( obj ) {
            
            
/*
            if (!(obj.alias === undefined)) 
            {    
                ajax.url = html.url.absolute() + obj.carpeta +'/'+ obj.alias + '/htmf/lista.html';    
            }              
            else
            {
                ajax.url = html.url.absolute() + obj.carpeta +'/'+ obj.tipo + '/htmf/lista.html';    
            }
*/

/*
            ajax.metodo = "GET";            
            document.getElementById( obj.dom ).innerHTML =  ajax.public.html();
            reflex.getTituloplu(obj);
*/
/*
            if (!(obj.pre_lista === undefined)) {    
                obj.pre_lista(obj);
            }                 
*/



  /*         var bu = "";
            bu = busqueda.getTextoBusqueda();                                                
            ajax.url = reflex.getApi( obj, page, bu );
*/


/*
            ajax.metodo = "GET";        
  */          
            // tabla.json = ajax.private.json();    
            
            // limpiar el json antes
            
            
console.log( obj.json  )            
            
            var ojson = JSON.parse( obj.json ) ;             
            boo.com.tabla.json  = JSON.stringify(ojson['datos']) ;  

            boo.com.tabla.ini(obj);
            boo.com.tabla.gene();   
            boo.com.tabla.formato(obj);

/*

            
            boo.com.tabla.set.tablaid(obj);     
            boo.com.tabla.lista_registro(obj, reflex.form_id ); 


            document.getElementById( obj.tipo + "_paginacion" ).innerHTML 
                    = paginacion.gene();     


           var buscar = busqueda.getTextoBusqueda();

            paginacion.move( obj, buscar, reflex.form_id );        
            //paginacion.move(obj, "", reflex.form_id );        


            boton.objeto = ""+obj.tipo;
            document.getElementById( obj.tipo +'_acciones_lista' ).innerHTML 
                    =  boton.basicform.get_botton_new();


            obj.funciones =  obj.botones_lista;

            boton.evento( obj ); 


            if (!(obj.post_lista === undefined)) {    
                obj.post_lista(obj);
            }         
*/

        },        
        
        
        paginacion_html: function( obj, json ) {   

            //arasa.html.paginacion.ini(json);
            boo.paginacion.ini(json);

            document.getElementById( obj.tipo + "_paginacion" ).innerHTML 
                    = boo.paginacion.gene();   
            //        = arasa.html.paginacion.gene();   

            //arasa.html.paginacion.move(obj, "", reflex.form_id);
            //boo.paginacion.move(obj, "", reflex.form_id);
            boo.paginacion.move(obj, "", null);
        },

        
        
        
        
    },
    
   
    
    util : {   
        countChar: function(str, c) {            
            var result = 0, i = 0;
            for(i;i<str.length;i++)if(str[i]==c)result++;
            return result;                
        },
    },    
    
    
       
    
    form: {
        
        name: '' ,   
        json: '' ,   
        campos: [],
        
        
        
        llenar: function(  ) {


            var oJson = JSON.parse(boo.form.json) ; 
            var campos = document.getElementById( boo.form.name ).getElementsByTagName("input");    


            for(var i=0; i< campos.length; i++) {            
                var c = campos[i];                      

                    if (typeof(c.dataset.foreign) === "undefined") {

                        if (c.type == "text"  ||  c.type == "hidden"  )
                        {                
                            if (c.className == "num")
                            {
                                c.value  = oJson[c.name]; 
                            }
                            else
                            {                                 
                                c.value  = oJson[c.name]; 
                                if (c.value === 'undefined') {
                                    c.value  = ""; 
                                }  
                            }                
                        }
                        else
                        {
                            var type= c.type;
                            switch(type) {
                                
                                case "date":                                                 
                                    c.value  = jsonToDate( oJson[c.name] );   
                                    break;                        

                                case "password":                                  
                                    c.value  = oJson[c.name];                     
                                    break;

                                case "radio":                                                                  
                                    form.radios.databool(c.name, oJson[c.name] );
                                    break;

                                case "checkbox":                                  
                                    //form.checkbox.databool(c, oJson[c.name] );
                                    boo.form.checkbox.databool(c, oJson[c.name] );
                                    
                                    break;


                                default:
                                    //code block
                            }                              



                        }            
                    }
                    else
                    {   

                        var objev = eval( "new "+c.dataset.foreign+"()");          

                        if (objev.objjson == undefined){
                            var f = c.dataset.foreign;
                        }
                        else{
                            var f = objev.objjson;
                        }

                        f = f.toLowerCase();

                        try {

                                c.value = oJson[f][objev.campoid] ;     
                                vardes = oJson[f][objev.json_descrip] ;
                                var foo = document.getElementById( objev.form_descrip ).nodeName;

                                switch (foo) {
                                  case "INPUT":
                                    document.getElementById( objev.form_descrip ).value =  vardes;
                                    break;
                                  case "OUTPUT":            

                                    document.getElementById( objev.form_descrip ).innerHTML =  vardes;
                                    break;
                                  default:
                                    console.log('default switch');
                                }                        

                            //console.log(document.getElementById( objev.form_descrip ).nodeName)           
                            //document.getElementById( objev.form_descrip ).innerHTML                                 =  vardes


                        }
                        catch(error) {

                                console.error(c.dataset.foreign);
                                console.error(error);
                                c.value = "0" ;              
                              //  document.getElementById( objev.form_descrip ).innerHTML = "";
                        }                                                  
                    }            



            }
            //this.llenar_textarea(form.json);


        },



    

        llenar_cbx: function( obj ) {

            //try {

                var oJson = JSON.parse(boo.form.json) ;      

                var campos = document.getElementById( boo.form.name ).getElementsByTagName("select");    

                for(var i=0; i< campos.length; i++) {   


                    var c = campos[i];       
                    var opt = document.createElement('option');            


                    if (!(campos[i].dataset.vista === "notGet" )){     

                        opt.value = oJson[c.name][obj.combobox[c.name]['value']];
                        opt.innerHTML = oJson[c.name][obj.combobox[c.name]['inner']];   
                        document.getElementById(   campos[i].id ).appendChild(opt);                               

                    }                        
                }
            //}
            /*
            catch (e) {
                console.log(e)
                console.log(c.name)
            } 
            */


        },

    
    
    
        llenar_textarea: function( json  ) {

            var oJson = JSON.parse(json) ;      

            var campos = document.getElementById( form.name ).getElementsByTagName("textarea");    

            for(var i=0; i< campos.length; i++) {               
                var c = campos[i];         
                    //c.value = "cargado;"
                     c.value  = oJson[c.name]; 
            }
        },



        
        
        
        getjson: function( ) {    

            var campos = document.getElementById( boo.form.name ).getElementsByTagName("input");  
    
            var arr = [];            
            var str = "";        

            for (var i=0; i< campos.length; i++) {         

                if (campos[i].name) {


                    // control d elementos no repetidos                
                    var idx = arr.indexOf(campos[i].name);
                    if (idx == -1){                    
                        if (!(typeof campos[i].dataset.vista === "notGet" )){     
                            arr.push(campos[i].name);    
                        }          
                    }
                    else{
                        break;
                    }
                                
                    var ele = campos[i];                   
            
                    var type= ele.type;
                    switch(type) {

                        case "hidden":                                                 

                            if (typeof ele.dataset.pertenece === "undefined" ){     
                                str =  str  + "";
                            }  
                            else
                            {                                
                                if (str  != ""){
                                    str =  str  + ",";
                                }                                                
                                str =   str + boo.form.elemetiq(campos[i]) ;                                   
                            }                            
                            break;                        


                        case "radio":                                                 
                            
                            if (str  != ""){
                                str =  str  + ",";
                            }                               
                            str =   str + form.radios.getdatabol(ele.name );                            
                            break;                        


                        case "checkbox":                                                 

                            if (str  != "") {
                                str =  str  + ",";
                            }                           
                            //str =   str + form.radios.getdatabolcheck(ele );
                            //str =   str + form.checkbox.getdatabool(ele);                            
                            str =   str + boo.form.checkbox.getdatabool(ele);
                            break;  


                        default:
                            
                            if (typeof ele.dataset.vista === "undefined" ){    
                                if (str  != ""){
                                    str =  str  + ",";
                                }                                                                                
                                str =   str + boo.form.elemetiq(campos[i]) ;     
                            }    


                    }       
                }
            }
            

            
            
            
            var str2 = "";  
            var combos = document.getElementById( boo.form.name ).getElementsByTagName("select");   
            
            
            for (var y=0; y < combos.length; y++) {               
                
                if (!(combos[y].dataset.vista === "notGet" )) {     
                    if (str2  != ""){
                        str2 =  str2  + ",";
                    }                                                            
                    str2 =  str2 + boo.form.elemetcombo(combos[y]) ;    
                }                          
    
            }

            if (str2  != ""){
                if (str  != ""){
                    str =  str  + "," + str2;
                }
                else{
                    str =  str2;
                }
            }



            // textarea
            var str3 = "";  
            var areas = document.getElementById( boo.form.name ).getElementsByTagName("textarea");   
            for (var x=0; x< areas.length; x++) {                

                if (str3  != ""){
                    str3 =  str3  + ",";
                }                                                            
                str3 =  str3 + form.datos.elemenTextArea(areas[x]);                      
            }

            if (str3  != ""){
                if (str  != ""){
                    str =  str  + "," + str3;
                }
                else{
                    str =  str3;
                }
            }

            return "{" +str+ "}"  ;            
        },   
        
        
        elemen: function( ele ) {    
            
            var str = "";        
            
            str =  str  + "\"" +ele.getAttribute('name')+ "\"" ;
            str =   str + ":";            
            
            if (ele.type == "text"  ||  ele.type == "hidden"  )
            {  

                //if (ele.className == "num")
                if (ele.classList.contains('num')) 
                {                    
                    str = str + NumQP(ele.value)  ;    
                }
                else
                {


                    //str = str + "\"" +ele.value+ "\"" ;
                    //str = str + "\"" + JSON.stringify(ele.value) + "\"" ;
                    str = str + JSON.stringify(ele.value)  ;
                }                
            }
            else
            {
                
                if (ele.type == "password"){
                    str =   str + "\"" + ele.value+ "\"" ;
                }
                if (ele.type == "date"){
                
                
                    if (ele.value !=  "")
                    {  
                        str =   str + "\"" + ele.value+ "\"" ;
                    }
                    else
                    {
                        str =   str + " null " ;
                    }

                    //str =   str + "\"" + ele.value+ "\"" ;
                }                                
            }
            
            return str ;            
        },
            
        
        elemenTextArea: function( ele ) {    
            
            var str = "";                    
            str =  str  + "\"" +ele.getAttribute('name')+ "\"" ;
            str =   str + ":";   
            str = str + "\"" +ele.value+ "\"" ;
            
            return str ;            
        },
                        
                       
            
        elemetiq: function( ele ) {                
            
            var str = "";              
            
            if (typeof ele.dataset.foreign === "undefined" ){         

                str =   str + boo.form.elemen(ele).toString();            
            }
            else {
                
                var e = ele.dataset.foreign.toString().toLocaleLowerCase();
                
                if (e == ele.getAttribute('name') ){                    
                    //str =  str  + "\"" +e+ "\"" ;                
                    str =  str  + "\"" +ele.getAttribute('name')+ "\"" ;                                    
                }
                else{                    
                    str =  str  + "\"" +e+ "\"" ;                
                    //str =  str  + "\"" +ele.getAttribute('name')+ "\"" ;                
                }
                //str =  str  + "\"" +ele.dataset.foreign+ "\"" ;                
                str =   str + ":";                  
                str =  str + "{ "+  form.datos.elemen(ele) + " }" ;
            }           
            return str ;            
        },
        
        
        elemetcombo: function( ele ) {    
            
            var str = "";              
            
            if (typeof ele.dataset.foreign === "undefined" ){         
                str = str + " \"" +ele.name+ "\" " ;            
            }            
            else{                
                var e = ele.dataset.foreign.toString().toLocaleLowerCase();
                str = str + " \"" + e + "\"" ;            
            } 
            
            str =   str + ":";            
            str =  str + "{ ";  
            str =  str + " \"" +ele.name+ "\": " ;
                if (ele.className == "num")
                {
                    str = str + NumQP(ele.value)  ;    
                }
                else
                {
                    str = str + "\"" +ele.value+ "\"" ;
                }             
            str =  str + "} ";
            return str ;            
        }, 
        
    
    
        disabled: function( bool ) {

            var fcampos = document.getElementById( boo.form.name ).getElementsByTagName("input");    

            var esIgual = false;    
            for(var i=0; i< fcampos.length; i++) 
            {            
                boo.form.campos.forEach(function (elemento, indice, array) {
                    
                    if (elemento === fcampos[i].id ){                                        
                        esIgual = true;
                    }
                });        


                if (!(fcampos[i].type == 'radio'))
                {
                    // en caso que sea check
                    //if (fcampos[i].type == 'checkbox') {                
                    if (false) {                
                    }
                    else{
                        if (!( esIgual )){
                            document.getElementById(fcampos[i].id).disabled =  !bool;
                        }else{
                            document.getElementById(fcampos[i].id).disabled = bool;                
                        }
                        esIgual = false;  
                    }
                }
                else
                {
                    form.radios.disable (fcampos[i].name, !bool);
                }
            }

            var fcampos = document.getElementById( boo.form.name ).getElementsByTagName("select");            
            var esIgual = false;

            for(var i=0; i< fcampos.length; i++) 
            {            
                boo.form.campos.forEach(function (elemento, indice, array) {    
                    if (elemento === fcampos[i].id ){                                        
                        esIgual = true;
                    }
                });                

                if (!( esIgual )){
                    document.getElementById(fcampos[i].id).disabled =  !bool;
                }else{
                    document.getElementById(fcampos[i].id).disabled = bool;                
                }
                esIgual = false;            
            }


            var fcampos = document.getElementById( boo.form.name ).getElementsByTagName("textarea");            
            var esIgual = false;

            for(var i=0; i< fcampos.length; i++) 
            {            
                form.campos.forEach(function (elemento, indice, array) {    
                    if (elemento === fcampos[i].id ){                                        
                        esIgual = true;
                    }
                });                

                if (!( esIgual )){
                    document.getElementById(fcampos[i].id).disabled =  !bool;
                }else{
                    document.getElementById(fcampos[i].id).disabled = bool;                
                }
                esIgual = false;            
            }

            boo.form.campos = [];

        },




        checkbox: { 

            elementos : "",

            nombre: function ( nom ) {                   
                boo.form.checkbox.elementos = document.getElementsByName(nom);
            },


            getvalor: function ( nom ) {    

                boo.form.checkbox.nombre(nom);
                var ele = boo.form.checkbox.elementos;
                var ret = "";                
    
                for (i=0; i < ele.length; i++) {  
                    if (ele[i].checked){                    
                        ret = "true";
                    }
                    else{
                        ret = "false";
                    }
                }    

                return ret;
            },
                   


            databool: function (  ele, bool ) {    

                var myBool = Boolean(ele.value); 

                if ( myBool === bool){
                    ele.checked = true;
                }   

            },

        
            getdatabool: function (  ele ) {    

                var str = "";        
                str =  str  + "\"" + ele.name + "\"" ;
                str =   str + ":";     
                
                //str = str + form.checkbox.getvalor(ele.name) ;  
                str = str + boo.form.checkbox.getvalor(ele.name) ;  

                return str;
    
            },

         }        


        
        
        
    },         
            
    





    tabla: {
    
        json:  "",
        campos: ['uno', 'dos'],
        etiquetas: ['uno', 'dos', 'tres', 'cuatro','cinco'],
        html: "",
        linea:"",
        tbody_id:"",
        id:"",
        oculto: [],


        ini: function(obj) {        

            boo.tabla.id = obj.tipo+ "-tabla";
            boo.tabla.linea = obj.campoid;
            boo.tabla.tbody_id = obj.tipo+"-tb";
            boo.tabla.campos = obj.tablacampos;

            boo.tabla.etiquetas = obj.etiquetas;
        },


        get_html: function(    ) {
            
            var oJson = JSON.parse(boo.tabla.json) ;                                                        
            
            boo.tabla.html = "";         

            for(x=0; x < oJson.length; x++) {     

                boo.tabla.html += "<tr data-linea_id=\""+
                        oJson[x][boo.tabla.linea]
                        +"\">";                  

                boo.tabla.campos.forEach(function (elemento, indice, array) {                                

                    try { 
                      boo.tabla.html += "<td data-title=\""+boo.tabla.etiquetas[indice]+"\">";                    
                    }
                    catch (e) {
                        boo.tabla.html += "<td>";     
                    }                

                    //tabla.html += "<td data-title=\""+tabla.etiquetas[indice]+"\">";                    
                    var jsonprop;

                    if (Array.isArray( elemento )){
                        jsonprop = boo.tabla.campos_array(elemento, oJson[x]);
                    }
                    else
                    {
                        try {
                            eval("jsonprop = oJson[x]."+ elemento + ";");    
                        }
                        catch(error) {                        
                            jsonprop = "";        
                        }
                    }
                    boo.tabla.html += jsonprop;     
                    boo.tabla.html += "</td>";
                });    
                boo.tabla.html += "</tr>";    
            }
            return boo.tabla.html;
        },



        refresh: function( obj, page, buscar, fn  ) {        

            obj.lista(page); 

        }, 





        campos_array: function(miArray, json) {

            var ret = "";

            for (var i = 0; i < miArray.length; i+=1) {          
              eval("ret = ret +' '+json."+ miArray[i] + ";");              
            }        
            return ret;
        },    




        gene: function() {       
            document.getElementById( boo.tabla.tbody_id ).innerHTML = boo.tabla.get_html(); 
        },




/*
        limpiar: function() {     
            if (document.getElementById( tabla.tbody_id ))
            {
                tabla.json = '[]';
                tabla.gene();    
            }        
        },
*/



        lista_registro: function( obj ) {

            var tmptable = document.getElementById( boo.tabla.id ).getElementsByTagName('tbody')[0];
            var rows = tmptable.getElementsByTagName('tr');

            for (var i=0 ; i < rows.length; i++)
            {
                rows[i].onclick = function()
                {   
                    var linea_id = this.dataset.linea_id;                                        
                    obj.tabla_registro(linea_id)
         
                };       
            }        
        },



/*
        set: {          
            tbodyid: function( objeto  ) {
                tabla.tbody_id = objeto.tipo+ "-tb";
            },
            tablaid: function( objeto  ) {
                tabla.id = objeto.tipo+ "-tabla";
            },
        },     
 */



/*
        ocultar: function() {

            var tableHe = document.getElementById( tabla.id ).getElementsByTagName('thead')[0];
            var rows = tableHe.getElementsByTagName('tr');

            tabla.oculto.forEach(function (ele, indice, array) {                        
                for (var i=0 ; i < rows.length; i++)
                {
                    cell = tableHe.rows[i].cells[ele] ;                                                  
                    cell.style.display = "none";
                }                           
            });    



            var tableBo = document.getElementById( tabla.id ).getElementsByTagName('tbody')[0];
            var rows = tableBo.getElementsByTagName('tr');

            tabla.oculto.forEach(function (ele, indice, array) {                        
                for (var i=0 ; i < rows.length; i++)
                {
                    cell = tableBo.rows[i].cells[ele] ;                                                  
                    cell.style.display = "none";
                }                           
            });    

        },
*/


        formato: function( obj ) {

            try { 

                var table = document.getElementById( boo.tabla.id  ).getElementsByTagName('tbody')[0];
                var rows = table.getElementsByTagName('tr');

                for (var i=0 ; i < rows.length; i++)
                {

                    for (var j=0 ; j < obj.tablaformat.length; j++)
                    {
                        
                        var type= obj.tablaformat[j];
                     
                        switch(type) {

                            case 'C':                                                 
                                break;                        

                            case 'N':                                  

                                cell = table.rows[i].cells[j] ;                                  
                                cell.innerHTML = fmtNum(cell.innerHTML);                        
                                cell.style = "text-align: right";
                                break;

                                case 'M':                                  

                                cell = table.rows[i].cells[j] ;                                  
                                cell.innerHTML = fmtNum(cell.innerHTML);                        
                                /*cell.style = "text-align: right";*/
                                break;



                            case 'D':                                                 

                                cell = table.rows[i].cells[j] ;                                             
                                cell.innerHTML = fDMA (cell.innerHTML) ;                 

                                cell.style = "text-align: right";
                                break;                        

                            case 'R':                                                 

                                cell = table.rows[i].cells[j] ;                               
                                cell.style = "text-align: right";
                                break;                        


                            case 'Z':           

                                cell = table.rows[i].cells[j] ;                                                 

                                if (cell.innerHTML === 'undefined'){
                                    cell.innerHTML = "";
                                }
                                else{

                                    cell.innerHTML = fmtNum(cell.innerHTML);                        
                                    cell.style = "text-align: right";                                
                                }
                                break;                                                    


                            case 'U':           

                                cell = table.rows[i].cells[j] ;                                                 

                                if (cell.innerHTML === 'undefined'){
                                    cell.innerHTML = "";
                                }
                                break;                                                    

                            case 'E':           

                                cell = table.rows[i].cells[j] ;                                
                                cell.innerHTML = "";
                                cell.style = "border: none";                                

                                break;                            

                            default:
                                //code block
                        }      
                    }
                }
            }
            catch (e) {  
                console.log(e);
            }        


        },


    },





    paginacion: {  

        pagina:     1,    
        total_registros: 0,
        lineas:       12,
        cantidad_paginas:  0,
        pagina_inicio : 1,
        pagina_fin : 0, 
        bloques:    12,
        html: "",    

        ini: function(json) { 


            var ojson = JSON.parse(json) ;                                     

            boo.paginacion.total_registros = ojson["total_registros"];

            var pagina = ojson["pagina"];

            var cantidad_paginas;
            var total_registros = boo.paginacion.total_registros ;
            var lineas = boo.paginacion.lineas;
            var bloques = boo.paginacion.bloques;
            var pagina_inicio = boo.paginacion.pagina_inicio;
            var pagina_fin = boo.paginacion.pagina_fin;

            cantidad_paginas = total_registros / lineas;
            cantidad_paginas = Math.trunc(cantidad_paginas);

            var calresto  = cantidad_paginas * lineas;
            if (calresto != total_registros) {
                cantidad_paginas = cantidad_paginas + 1;
            }


            if ((cantidad_paginas * lineas ) < total_registros){
                cantidad_paginas++;
            }        

            if (bloques > cantidad_paginas) {
                pagina_inicio = 1;
                pagina_fin = cantidad_paginas;
            }        
            else{
                if (bloques == cantidad_paginas) {
                    pagina_inicio = 1;
                    pagina_fin = bloques;
                }    
                else{ 
                    if (bloques  < cantidad_paginas) {

                        if (pagina - (bloques / 2) < 1) {
                            pagina_inicio = 1;
                            pagina_fin = pagina + bloques;
                        }
                        else{
                            pagina_inicio = pagina - (bloques / 2);
                        }

                        if (pagina + (bloques / 2) > cantidad_paginas )                        {
                            pagina_inicio = cantidad_paginas - bloques;
                            pagina_fin = cantidad_paginas;
                        }
                        else{
                            pagina_fin =  pagina + (bloques / 2);
                        }    
                    }
                }    
            }                 

            boo.paginacion.pagina = pagina;
            boo.paginacion.cantidad_paginas = cantidad_paginas;
            boo.paginacion.total_registros = total_registros;
            boo.paginacion.pagina_inicio = pagina_inicio;
            boo.paginacion.pagina_fin = pagina_fin;


        },


        gene: function() {       

            //paginacion.total_registros  = 16

            var total_registros = boo.paginacion.total_registros ;
            var cantidad_paginas = boo.paginacion.cantidad_paginas ;
            var pagina = boo.paginacion.pagina ;
            var pagina_inicio = boo.paginacion.pagina_inicio ;
            var pagina_fin = boo.paginacion.pagina_fin;

            var html;


            if (total_registros == 0){
                return "";
            }

            if (cantidad_paginas == 1){
                pagina = 1;
            }

            html = "<ul  class=\"pagination\"  data-paginaactual=\"1\" id=\"paginacion\">";

            if (pagina != pagina_inicio){            
                html += "<li class=\"page-item\" data-pagina=\"ant\" >" ;

                html +=      "<a class=\"page-link\" href=\"javascript:void(0);\"id =\"ant\">" ;
                html +=          "anterior" ;
                html +=      "</a>" ;

                html += "</li>";
            }


            for (i = pagina_inicio; i <= pagina_fin; i++) {   

                if (pagina === i)
                {   
                    html += "<li class=\"page-item\" data-pagina=\"act\">" ;
                    html += "<a class=\"page-link active\" href=\"javascript:void(0);\" > " ;
                    html += i ;
                    html += "</a>" ;
                    html += "</li>" ;
                }
                else
                {                
                    html += "<li class=\"page-item\" data-pagina= \"pag" + i + "\"  >" ;
                    html += "<a class=\"page-link\" href=\"javascript:void(0);\" id =pag"+ i +"> " ;
                    html += i ;
                    html += "</a>" ;
                    html += "</li>" ;     
                }
            }

            if (pagina != pagina_fin){            
                html += "<li class=\"page-item\" data-pagina=\"sig\" >" ;

                html +=      "<a class=\"page-link\" href=\"javascript:void(0);\"id =\"sig\">" ;
                html +=          "siguiente" ;
                html +=      "</a>" ;

                html += "</li>";
            }
            html += " </ul>"
                        
            return html;
            
        },






        move: function(  obj, busca, fn ) {       
            
            var listaUL = document.getElementById( obj.tipo + "_paginacion" );
            var uelLI = listaUL.getElementsByTagName('li');
            
            var pagina = 0;


            for (var i=0 ; i < uelLI.length; i++)
            {
                var lipag = uelLI[i];   

                if (lipag.dataset.pagina == "act"){                                     
                    pagina = lipag.firstChild.innerHTML;
                }                    
            }



            for (var i=0 ; i < uelLI.length; i++)
            {
                var datapag = uelLI[i].dataset.pagina;     

                if (!(datapag == "act"  || datapag == "det"  ))
                {
                    uelLI[i].addEventListener ( 'click',
                        function() {                                      

                            switch (this.dataset.pagina)
                            {
                               case "sig": 
                                       pagina = parseInt(pagina) +1;
                                       break;                                                                          

                               case "ant":                                     
                                       pagina = parseInt(pagina) -1;
                                       break;

                               default:  
                                       pagina = this.childNodes[0].innerHTML.toString().trim();
                                       break;
                            }
                            pagina = parseInt( pagina , 10);                                                                       

                            //arasa.html.paginacion.pagina = pagina;
                            // tabla.refresh_promise( obj, pagina, busca, fn  ) ;                            
                            boo.tabla.refresh( obj, pagina, busca, fn  ) ;
                            
                        },
                        false
                    );                
                }            
            }           

        },

    },











};





