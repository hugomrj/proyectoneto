
function NaturalezaReceptor(){
    
    this.tipo = this.constructor.name.toLowerCase();    
    this.campoid =  this.constructor.name.toLowerCase();    
    

}


NaturalezaReceptor.prototype.combobox = function( dom ) {                
    

    var dom = document.getElementById( dom );
    var idedovalue = dom.value;

    ajax.api = ajax.getserver() +  "/api/naturalezareceptor/all"        
    ajax.promise.asyn("get").then(result => {

        var ojson = JSON.parse( result.responseText ) ;    
        for( x=0; x < ojson.length; x++ ) {

            var jsonvalue = (ojson[x]['naturaleza_receptor'] );            

            if (idedovalue != jsonvalue )
            {  
                var opt = document.createElement('option');            
                opt.value = jsonvalue;
                opt.innerHTML = ojson[x]['descripcion'];                        
                dom.appendChild(opt);                     
            }
        }    
    });


};