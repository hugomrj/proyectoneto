
function Pais(){
    
    this.tipo = this.constructor.name.toLowerCase();    
    this.campoid =  this.constructor.name.toLowerCase();    
    

}


Pais.prototype.combobox = function( dom ) {                   

    var dom = document.getElementById( dom );
    var idedovalue = dom.value;


    ajax.api = ajax.getserver() +  "/api/paises/all"        
    ajax.promise.asyn("get").then(result => {

        var ojson = JSON.parse( result.responseText ) ;    
        for( x=0; x < ojson.length; x++ ) {

            var jsonvalue = (ojson[x]['pais'] );            

            if (idedovalue != jsonvalue )
            {  
                var opt = document.createElement('option');            
                opt.value = jsonvalue;
                opt.innerHTML = ojson[x]['nombre'];                        
                dom.appendChild(opt);                     
            }
        }    
        
        
        // Inicializa Choices.js en el elemento <select>
        var choices = new Choices(dom, {
            position: 'bottom'
            /* Opciones adicionales según tus necesidades */
        });            
                    
        
        
    });


};


