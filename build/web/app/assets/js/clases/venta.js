
function Venta(){
    
    this.tipo = this.constructor.name.toLowerCase();    
    this.campoid =  this.constructor.name.toLowerCase();    
    this.json = ""; 
    this.tablacampos = ['venta', 'fecha_emision', 'cliente.razon_social', 
            'cliente_documento', 'cliente.tipo_contribuyente.descripcion',
            'subtotal0', 'subtotal5', 'subtotal10', 'total_monto' ];
     

    this.tablaformat = ['N',  'D',  'C', 
                        'C', 'C', 'M', 'M', 'M', 'M' ];                                  


    this.choices_razon_social;            
}




Venta.prototype.lista = function( page ) {        
    
    var obj = this;
    boo.loader.inicio();
                                          
        
    // tal vez aca llame al la clase            
    fetch('modulos/venta/lista.html')
        .then(response => response.text())
        .then(html => {
            
            document.getElementById(boo.main).innerHTML = html;
            obj.nuevo();    

            ajax.api = ajax.getserver() +  "/api/ventas?page="+page;
            ajax.json = null;
            
            ajax.promise.asyn("get").then(result => {
                obj.json = result.responseText;
                
                var ojson = JSON.parse( obj.json ) ; 
                boo.tabla.json = JSON.stringify(ojson['datos']);     

                boo.tabla.ini(obj);                
                boo.tabla.gene();                  
                boo.tabla.formato(obj);
                
                boo.tabla.id = "venta-tabla";
                boo.tabla.lista_registro(obj); 

                
                
                boo.vista.paginacion_html(obj,  JSON.stringify(ojson['paginacion']), page  );                              
            });


            
        })
        .catch(error => {
            console.error('Error al cargar la página:', error);
        })
        .finally(() => {               
            boo.loader.fin();
        });    

};







Venta.prototype.nuevo = function( ) {                
    
    var obj = this;    
        
    var venta_nuevo = document.getElementById('venta_nuevo');              
    venta_nuevo.onclick = function(  )
    {   
        
        var contentContainer = document.getElementById('idmain');    
        contentContainer.innerHTML = '';

        boo.loader.inicio();
        
        fetch('modulos/venta/form_nuevo.html')
        .then(response => response.text())
        .then(html => {
            contentContainer.innerHTML = html;      
            
           
            
            fetch('modulos/venta/form_caberera.html')
            .then(response => response.text())
            .then(html => {            
                document.getElementById('form_caberera').innerHTML = html;


                obj.form_ini();


                // nummero de factura
                document.getElementById("establecimiento").value = "001";
                document.getElementById("punto_expedicion").value = "001";
                document.getElementById("factura_numero").value = 0;
    
    
                // fecha                        
                var fechaActual = new Date().toISOString().split('T')[0];                    
                document.getElementById("fecha").value = fechaActual;
    

                obj.carga_combos();
                obj.botones_accion_add();  
       
                document.getElementById('bloque_ruc').style.display = 'block';
                document.getElementById('bloque_digitov').style.display = 'block';
                document.getElementById('bloque_numero_documento').style.display = 'none';            
       
       
    
                obj.html_generar_linea_ventas_detalles(1);
                obj.js_generar_eventos_linea(1)
                obj.agregar_registro_ventas_detalles();


            });



            return html;
        })
        /*
        .catch(error => {
            console.error(error);
        })
        */
        .finally(() => {               
            boo.loader.fin();
        }); 
     
    }    

};


    
    
    

            

Venta.prototype.form_ini = function() {    

    obj = this;

    this.enfoque_input();

    /*
    var venta_ruc = document.getElementById('venta_ruc');          
    venta_ruc.onblur  = function() {        
        venta_ruc.value =fmtNum(venta_ruc.value);          
    }    
    venta_ruc.onblur();
    */

    var venta_digitov = document.getElementById('venta_digitov');          
    venta_digitov.onblur  = function() {           
        venta_digitov.value = fmtNum(venta_digitov.value);                                    
    }    
    venta_digitov.onblur();


    var venta_numero_documento = document.getElementById('venta_numero_documento');          
    venta_numero_documento.onblur  = function() {           
        venta_numero_documento.value = fmtNum(venta_numero_documento.value);                                    
    }    
    venta_numero_documento.onblur();



         
    
    var bloque_ruc = document.getElementById('bloque_ruc');
    var bloque_digitov = document.getElementById('bloque_digitov');
    var bloque_numero_documento = document.getElementById('bloque_numero_documento');
       
    var venta_naturaleza_receptor = document.getElementById('venta_naturaleza_receptor');

    venta_naturaleza_receptor.onchange  = function() {   


        obj.choices_razon_social.clearChoices();
        obj.choices_razon_social.clearStore();
        document.getElementById('venta_ruc').value = 0;
        document.getElementById('venta_digitov').value = 0;
        document.getElementById('venta_numero_documento').value = 0;   


        // Obtén el valor seleccionado
        var selectedValue = venta_naturaleza_receptor.value;
        var numericValue = parseInt(selectedValue, 10);
        if (numericValue === 1) {                        
            bloque_ruc.style.display = 'block';            
            bloque_digitov.style.display = 'block';            
            bloque_numero_documento.style.display = 'none';                        
        }
        else {
            if (numericValue === 2) {                            
                bloque_ruc.style.display = 'none';                        
                bloque_digitov.style.display = 'none';                        
                bloque_numero_documento.style.display = 'block';                      
            } 
        }
    };



    var venta_ruc = document.getElementById("venta_ruc");
    // Agregar un evento de presionar Enter
    venta_ruc.onblur = function(event) {        
                    
        
        
        var rucNumerico = venta_ruc.value.replace(/\D/g, '');
        rucNumerico = NumQP(rucNumerico);          

        venta_ruc.value =fmtNum(venta_ruc.value);  

        ajax.api = ajax.getserver() +  "/api/clientes/1/ruc/"+rucNumerico;
        ajax.json = null;
        
        ajax.promise.asyn("get").then(result => {
            let json = result.responseText;                


            var venta_digitov = document.getElementById("venta_digitov");

            var venta_razon_social = document.getElementById("venta_razon_social");                
            while (venta_razon_social.options.length > 0) {
                venta_razon_social.remove(0);
            }

            // Verificar si el JSON no está vacío
            if (json) {
                var ojson;
                try {
                    ojson = JSON.parse(json);
                    // Realizar operaciones con ojson
                    venta_digitov.value = ojson['digitov'];                        

                    // Ahora puedes agregar el nuevo elemento <option> 
                    var opt = new Option(ojson['razon_social'], ojson['cliente']);
                    venta_razon_social.add(opt);
                    

                    var items = [{
                        value: ojson['cliente'],  
                        label: ojson['razon_social'], 
                        selected: true
                    }];
                    obj.choices_razon_social.clearChoices();
                    obj.choices_razon_social.clearStore();
                    obj.choices_razon_social.setChoices(items, 'value', 'label', false);

                }                 
                /*
                catch (error) {
                    console.error("Error al analizar el JSON:", error);
                }
                */
                finally{
                    console.log("");
                }

            } else {
                venta_digitov.value = 0;
                obj.choices_razon_social.clearChoices();
                obj.choices_razon_social.clearStore();                
            }
        });
        
    };
    





    var venta_numero_documento = document.getElementById("venta_numero_documento");
    // Agregar un evento de presionar Enter

    venta_numero_documento.onblur = function(event) {        

        var nroNumerico = venta_numero_documento.value.replace(/\D/g, '');
        nroNumerico = NumQP(nroNumerico);          

        venta_ruc.value =fmtNum(venta_ruc.value);  

        ajax.api = ajax.getserver() +  "/api/clientes/2/doc_nro/"+nroNumerico;
        ajax.json = null;
        
        ajax.promise.asyn("get").then(result => {
            let json = result.responseText;                

            var venta_razon_social = document.getElementById("venta_razon_social");                
            while (venta_razon_social.options.length > 0) {
                venta_razon_social.remove(0);
            }

            // Verificar si el JSON no está vacío
            if (json) {
                var ojson;
                try {
                    ojson = JSON.parse(json);                 

                    // Ahora puedes agregar el nuevo elemento <option> 
                    var opt = new Option(ojson['razon_social'], ojson['cliente']);
                    venta_razon_social.add(opt);
                    

                    var items = [{
                        value: ojson['cliente'],  
                        label: ojson['razon_social'], 
                        selected: true
                    }];
                    obj.choices_razon_social.clearChoices();
                    obj.choices_razon_social.clearStore();
                    obj.choices_razon_social.setChoices(items, 'value', 'label', false);

                } 
                catch (error) {
                    console.error("Error al analizar el JSON:", error);
                }
            } 
            // si esta vacio limpiar todo elselect
            else {
                obj.choices_razon_social.clearChoices();
                obj.choices_razon_social.clearStore();
            }

        });

    };
    



/*
console.log("llega hasta aca")

    var linea1 = document.getElementById( "linea1" );
    var idedovalue = linea1.value;

    ajax.api = ajax.getserver() +  "/api/paises/all"        
    ajax.promise.asyn("get").then(result => {

        var ojson = JSON.parse( result.responseText ) ;    
        for( x=0; x < ojson.length; x++ ) {

            var jsonvalue = (ojson[x]['pais'] );            

            if (idedovalue != jsonvalue )
            {  
                var opt = document.createElement('option');            
                opt.value = jsonvalue;
                opt.innerHTML = ojson[x]['nombre'];                        
                linea1.appendChild(opt);                     
            }
        }    
        
    

        var choices = new Choices(linea1, {
            position: 'bottom'
        });       

    });
*/






};





Venta.prototype.botones_accion_add = function( ) {    
    
    var obj = this;
    
        
    var divBotonera = document.getElementById('botonera');
    /*divBotonera.classList.add('d-flex', 'justify-content-start', 'flex-row');*/

    // Crea un div con la clase 'col'
    var colDiv = document.createElement('div');
    colDiv.className = 'd-grid gap-2 d-md-flex justify-content-md-star mt-5 mb-1 ct'; 

    // Crea el botón de Guardar
    var botonGuardar = document.createElement('button');
    botonGuardar.type = 'button';
    botonGuardar.className = 'btn btn-primary me-md-2'; 
    botonGuardar.innerHTML = 'Facturar';
    

    // Crea el botón de Cancelar
    var botonCancelar = document.createElement('button');
    botonCancelar.type = 'button';
    botonCancelar.className = 'btn btn-secondary'; 
    botonCancelar.innerHTML = 'Cancelar';


    // Agrega los botones al div 'col'
    colDiv.appendChild(botonGuardar);
    colDiv.appendChild(botonCancelar);

    // Agrega el div 'col' al contenedor principal
    divBotonera.appendChild(colDiv);

    
    botonGuardar.addEventListener('click', function() {

        
        //let obj = new Cliente();                
        if ( obj.validar()){      
            
            boo.form.name = "form_cliente";

            //console.log( obj.getjson() );

            ajax.api = ajax.getserver() +  "/api/ventas/facturar"
            ajax.json =  obj.getjson() ;

            
            ajax.promise.asyn("post").then(result => {                


                if (result.status == 200){
                    // mostrar lista
                    //obj.lista(1);
                    codigo_venta = result.responseText;  
                    
                    obj.tabla_registro(codigo_venta);

                    boo.mostrarToast("Registro agregado", "success");
                }
                else {
                    boo.mostrarToast( result.responseText, "danger");
                }

            });  

        }      
    });    
    

    
    botonCancelar.onclick =  function() {    
        obj.lista(1);
    };       
    

}





Venta.prototype.carga_combos = function( dom ) {                
   
   var venta_naturaleza_receptor = new NaturalezaReceptor();
   venta_naturaleza_receptor.combobox("venta_naturaleza_receptor");
      
   this.combobox_razon_social("venta_razon_social");

   
};    

  



Venta.prototype.combobox_razon_social = function( dom ) {          
    
    obj = this;

    var dom = document.getElementById( dom );
    ajax.api = ajax.getserver() + "/api/clientes/1/all;q=";    
    
    
    // Inicializa Choices.js en el elemento <select>
    obj.choices_razon_social = new Choices(dom, {
        position: 'bottom',   
        loadingText: 'buscando..',
        noResultsText: 'no se encontraron resultados',
        noChoicesText: 'escriba para buscar',
        searchResultLimit: 10
    });







    obj.choices_razon_social.passedElement.element.addEventListener(
        'search',
        function(event) {          

            var searchText = event.detail.value;

            // Limpiar el temporizador existente (si existe)
            if (window.searchTimeout) {
                clearTimeout(window.searchTimeout);
            }

            var nat = document.getElementById('venta_naturaleza_receptor').value;

            window.searchTimeout = setTimeout(function() {

                // Realiza una solicitud AJAX (o fetch) para obtener datos del servidor
                fetch(ajax.getserver() + "/api/clientes/"+nat+"/all;q=" + searchText)
                    .then(response => response.json())
                    .then(data => {
                        // Elimina las opciones antiguas
                        dom.innerHTML = '';
                        return data;
                    })
                    .then(data => {               
                
                        // Adaptar el JSON para usar con Choices.js
                        var adaptedData = data.map(function (item) {
                            return {
                                value: item.cliente,
                                label: item.razon_social
                            };
                        });

                        obj.choices_razon_social.clearChoices();
                        obj.choices_razon_social.clearStore();
                        // obj.choices_razon_social.destroy();
                        obj.choices_razon_social.setChoices(adaptedData, 'value', 'label', false);
                    });

            }, 300); 


        },
        false,
      );









      obj.choices_razon_social.passedElement.element.addEventListener(
        'choice',
        function(event) {          

            var opcionSeleccionada = event.detail.choice;
            // Realiza acciones con la opción seleccionada
            console.log('Opción seleccionada:', opcionSeleccionada.value);


            // crear un evento que filtre el ruc y el numero de documento
            ajax.api = ajax.getserver() +  "/api/clientes/"+opcionSeleccionada.value;
            ajax.promise.asyn("get").then(result => {

                let json = result.responseText;

                if (result.status === 200) {
                    // Si el código de estado es 200, hay datos                    
                    let ojson = JSON.parse(json);

                    document.getElementById('venta_ruc').value = ojson.ruc;
                    document.getElementById('venta_digitov').value = ojson.digitov;
                    document.getElementById('venta_numero_documento').value = ojson.numero_documento;

                    

                } else if (result.status === 204) {
                    document.getElementById('venta_ruc').value = 0;
                    document.getElementById('venta_digitov').value = 0;
                    document.getElementById('venta_numero_documento').value = 0;                    

                } else {
                    // Otro código de estado, manejar según sea necesario
                    console.error('Error en la solicitud:', result.status);
                }

                return json;
            })

        },
        false,
      );      




};



    


Venta.prototype.enfoque_input = function(  ) {                
   

  // Obtén todos los elementos input dentro del formulario
  var formInputs = document.querySelectorAll('#form_ventas input[type="text"]');

  // Agrega un event listener a cada input
  formInputs.forEach(function(input) {
    input.addEventListener('focus', function() {
      // Selecciona todo el texto en el input al recibir el enfoque
      this.select();
    });
  });


    
 };    
 




 Venta.prototype.html_generar_linea_ventas_detalles = function( numeroLinea ) {   
 
    // Crear el contenedor principal
    const linea = document.createElement('div');
    linea.id = 'linea_' + numeroLinea; 
    //linea.className = 'p-2 border row mx-0';
    linea.className = 'p-1 border row mx-0';
    linea.setAttribute('data-linea', numeroLinea);
    linea.setAttribute('data-iva', 0);

    // Primer div
    const div1 = document.createElement('div');
    div1.className = 'row mx-0 ps-0 col-5';


    // icono de borrar
    const div1Del = document.createElement('div');
    div1Del.className = 'ps-0 col-1';


    const iconoBorrar = document.createElement('i');
    iconoBorrar.id = 'borrar_linea_' + numeroLinea; 
    iconoBorrar.className = 'fa fa-minus fa-2x text-danger';
    iconoBorrar.setAttribute('aria-hidden', 'true');
    iconoBorrar.setAttribute('data-id', numeroLinea);    
    iconoBorrar.style.cursor = 'pointer'; 
    div1Del.appendChild(iconoBorrar);

    
    // select 
    const div1Inner = document.createElement('div');
    div1Inner.className = 'col-11';

    const select1 = document.createElement('select');
    select1.className = 'form-control';
    select1.id = 'producto_' + numeroLinea;
    select1.name = 'producto_' + numeroLinea;
    div1Inner.appendChild(select1);

    div1.appendChild(div1Del);
    div1.appendChild(div1Inner);





    // Segundo div
    const div2 = document.createElement('div');
    div2.className = 'row mx-0 px-0 col-3';


    const div2_c1 = document.createElement('div');
    div2_c1.className = 'ps-0 col-4';  

    const input1 = document.createElement('input');
    input1.className = 'form-control form-control-sm';
    input1.type = 'text';
    input1.setAttribute('data-id', numeroLinea);    
    input1.id = 'unidadmedida_' + numeroLinea;
    input1.setAttribute('disabled', 'disabled');    

    div2_c1.appendChild(input1);
    div2.appendChild(div2_c1);




    const div2_c2 = document.createElement('div');
    div2_c2.className = 'ps-0 col-4';  

    const input2 = document.createElement('input');
    input2.className = 'form-control form-control-sm';
    input2.type = 'text';
    input2.setAttribute('data-id', numeroLinea);    
    input2.id = 'cantidad_' + numeroLinea;    
    input2.value = 1;   

    div2_c2.appendChild(input2);
    div2.appendChild(div2_c2);




    const div2_c3 = document.createElement('div');
    div2_c3.className = 'ps-0 col-4';  

    const input3 = document.createElement('input');
    input3.className = 'form-control form-control-sm';
    input3.type = 'text';
    input3.setAttribute('data-id', numeroLinea);    
    input3.id = 'preciounitario_' + numeroLinea;  
    input3.setAttribute('disabled', 'disabled');                
    input3.value = 0;  

    div2_c3.appendChild(input3);
    div2.appendChild(div2_c3);


/*
    const div2_c4 = document.createElement('div');
    div2_c4.className = 'ps-0 col-3';  

    const input4 = document.createElement('input');
    input4.className = 'form-control form-control-sm';
    input4.type = 'text';
    input4.setAttribute('data-id', numeroLinea);    
    input4.id = 'descuento_' + numeroLinea;    
    input4.value = 0;  

    div2_c4.appendChild(input4);
    div2.appendChild(div2_c4);
*/



    // Tercer div
    const div3 = document.createElement('div');
    div3.className = 'row mx-0 px-0 col-4';

    

    const div3_c1 = document.createElement('div');
    div3_c1.className = 'ps-0 col-4';

    const input5 = document.createElement('input');
    input5.className = 'form-control form-control-sm';
    input5.type = 'text';
    input5.setAttribute('disabled', 'disabled');
    input5.value = 0;  
    input5.setAttribute('data-id', numeroLinea);    
    input5.id = 'iva0_' + numeroLinea;
    input5.setAttribute('data-iva', "0"); 

    div3_c1.appendChild(input5);
    div3.appendChild(div3_c1);




    const div3_c2 = document.createElement('div');
    div3_c2.className = 'ps-0 col-4';

    const input6 = document.createElement('input');
    input6.className = 'form-control form-control-sm';
    input6.type = 'text';
    input6.setAttribute('disabled', 'disabled');
    input6.value = 0;  
    input6.setAttribute('data-id', numeroLinea);    
    input6.id = 'iva5_' + numeroLinea;
    input6.setAttribute('data-iva', "5"); 

    div3_c2.appendChild(input6);
    div3.appendChild(div3_c2);




    const div3_c3 = document.createElement('div');
    div3_c3.className = 'ps-0 col-4';

    const input7 = document.createElement('input');
    input7.className = 'form-control form-control-sm';
    input7.type = 'text';
    input7.setAttribute('disabled', 'disabled');
    input7.value = 0;  
    input7.setAttribute('data-id', numeroLinea);    
    input7.id = 'iva10_' + numeroLinea;
    input7.setAttribute('data-iva', "10"); 

    div3_c3.appendChild(input7);
    div3.appendChild(div3_c3);




    // Agregar los divs al contenedor principal
    linea.appendChild(div1);
    linea.appendChild(div2);
    linea.appendChild(div3);

   

    // eventos
    // cantidad
    input2.addEventListener('blur', function() {
                
        let cantidad = parseFloat(this.value);
        let preciounitario = parseFloat(input3.value) ;
        let resultado = Math.round(preciounitario * cantidad);

        let data_iva = linea.dataset.iva;
        switch ( parseFloat(data_iva)) {
            case 0:                
                input5.value = resultado;
                input6.value = 0;
                input7.value = 0;
                break;

            case 5:                
                input5.value = 0;
                input6.value = resultado;
                input7.value = 0;
                break;

            case 10:                
                input5.value = 0;
                input6.value = 0;
                input7.value = resultado;
                break;                
        }     

        new Venta().suma_total();

    });    



    input3.addEventListener('blur', function() {
        
        let cantidad = parseFloat(input2.value);
        let preciounitario = parseFloat(input3.value) ;
        let resultado = Math.round(preciounitario * cantidad);

        let data_iva = linea.dataset.iva;
        switch ( parseFloat(data_iva)) {
            case 0:                
                input5.value = resultado;
                input6.value = 0;
                input7.value = 0;
                break;

            case 5:                
                input5.value = 0;
                input6.value = resultado;
                input7.value = 0;
                break;

            case 10:                
                input5.value = 0;
                input6.value = 0;
                input7.value = resultado;
                break;                
        }     

        new Venta().suma_total();

    }); 


    document.getElementById('cuadricula_detalles').appendChild(linea);
    // this.js_generar_eventos_linea(numeroLinea);
    
};




Venta.prototype.js_generar_eventos_linea = function( numeroLinea ) {   

    let ojson ;

    // borrar linea
    const botonBorrar = document.getElementById('borrar_linea_' + numeroLinea);
    if (botonBorrar) {
        botonBorrar.addEventListener('click', function() {
            const elementoABorrar = document.getElementById('linea_' + numeroLinea);

            if (elementoABorrar) {
                elementoABorrar.remove();
                new Venta().suma_total();
            }
        });
    }






    // select choises.js
    var select_producto = document.getElementById( "producto_" + numeroLinea );
          
 
    // Inicializa Choices.js en el elemento <select>
    var choices_producto = new Choices(select_producto, {
        position: 'bottom',   
        loadingText: 'buscando..',
        noResultsText: 'no se encontraron resultados',
        noChoicesText: 'escriba para buscar',
        searchResultLimit: 10
    });        




    choices_producto.passedElement.element.addEventListener(
        'search',
        function(event) {
            var searchText = event.detail.value;
    
            // Limpiar el temporizador existente (si existe)
            if (window.searchTimeout) {
                clearTimeout(window.searchTimeout);
            }
    
            ajax.api = ajax.getserver() +  "/api/productos/search;q="  

            // Establecer un nuevo temporizador para realizar la búsqueda después de un breve período
            window.searchTimeout = setTimeout(function() {
                // Realizar la solicitud AJAX (o fetch) para obtener datos del servidor
                fetch(ajax.api + searchText)
                    .then(response => response.json())
                    .then(data => {
                        // Eliminar las opciones antiguas
                        select_producto.innerHTML = '';
                        return data;
                    })
                    .then(data => {
                        // Adaptar el JSON para usar con Choices.js
                        var adaptedData = data.map(function(item) {
                            return {
                                value: item.producto,
                                label: item.descripcion
                            };
                        });
    
                        choices_producto.clearChoices();
                        choices_producto.clearStore();
                        choices_producto.setChoices(adaptedData, 'value', 'label', false);
    
                        ojson = data;
                    });
            }, 300); // Establecer el tiempo de espera (en milisegundos), por ejemplo, 300ms
        },
        false
    );
    



    choices_producto.passedElement.element.addEventListener(
        'choice',
        function(event) {          

            var opcionSeleccionada = event.detail.choice;           

            productos = ojson;
            const productoElegido = productos.find(producto => producto.producto === opcionSeleccionada.value);            

            var precio_variable = productoElegido.precio_variable;

            // cargar los calores a la cadricula
            var preciounitario = document.getElementById( "preciounitario_" + numeroLinea );
            preciounitario.value = productoElegido.precio_unitario;

            
            // si es precio variable tiene que desbloquearse el campo
            if (precio_variable == true) {
                preciounitario.disabled = false;
            }
            else {
                preciounitario.disabled = true;
            }


            // iva 
            document.getElementById('linea_'+numeroLinea)
                    .setAttribute('data-iva',  productoElegido.tasa_iva.tasa_iva );


            // unidada medida
            var unidadmedida = document.getElementById( "unidadmedida_" + numeroLinea );
            unidadmedida.value = productoElegido.unidad_medida.unimed;

            
            var cantidad = document.getElementById( "cantidad_" + numeroLinea );
            //cantidad.blur();
            cantidad.focus();
            cantidad.select();   

        },
        false,
    );      



}





Venta.prototype.agregar_registro_ventas_detalles = function( numeroLinea ) {  

    let indice = 2;


    var boton_add = document.getElementById( 'boton_add');
    boton_add.onclick = function()
    {   

        // Obtener el contenedor
        const cuadriculaDetalles = document.getElementById('cuadricula_detalles');
        // Obtener todos los hijos con el atributo data-linea
        const lineasConDataLinea = cuadriculaDetalles.querySelectorAll('[data-linea]');
        let ultima_linea = 0;
        // Iterar sobre los elementos encontrados
        lineasConDataLinea.forEach((linea) => {
            // Hacer algo con cada elemento, por ejemplo, imprimir el valor de data-linea
            ultima_linea = linea.getAttribute('data-linea');    
        });
        //console.log('data-linea:', ultima_linea);
        const linea = document.getElementById('linea_'+ultima_linea);
        // Obtener el elemento específico dentro del contenedor
        const elementoSeleccionado = linea.querySelector('.choices__item--selectable');
        // Verificar si se encontró el elemento


        if (elementoSeleccionado) {
            // agregar linea nueva
            indice++;
            obj.html_generar_linea_ventas_detalles(indice);
            obj.js_generar_eventos_linea(indice)
        } 
        else {
            // no agrega la linea porque no se selcciono producto
            boo.mostrarToast("Seleccionar producto", "danger");
        }


    }    




}





Venta.prototype.suma_total = function(  ) {  



    // Obtener el contenedor
    const cuadriculaDetalles = document.getElementById('cuadricula_detalles');

    var suma_iva10 = 0;
    const elementosConIva10 = cuadriculaDetalles.querySelectorAll('[data-iva="10"]');
    elementosConIva10.forEach((elemento) => {
        // Obtener el contenido del elemento y convertirlo a número
        const valorElemento = parseFloat(elemento.value);    
        // Verificar si la conversión fue exitosa y agregar al total
        if (!isNaN(valorElemento)) {
            suma_iva10 += valorElemento;
        }
    });
    document.getElementById('suma_iva10').value = fmtNum(suma_iva10);       
    

    var suma_iva5 = 0;
    const elementosConIva5 = cuadriculaDetalles.querySelectorAll('[data-iva="5"]');
    elementosConIva5.forEach((elemento) => {
        // Obtener el contenido del elemento y convertirlo a número
        const valorElemento = parseFloat(elemento.value);            
        if (!isNaN(valorElemento)) {
            suma_iva5 += valorElemento;
        }
    });
    document.getElementById('suma_iva5').value = fmtNum(suma_iva5);       
    


    var suma_iva0 = 0;
    const elementosConIva0 = cuadriculaDetalles.querySelectorAll('[data-iva="0"]');
    elementosConIva0.forEach((elemento) => {
        // Obtener el contenido del elemento y convertirlo a número
        const valorElemento = parseFloat(elemento.value);            
        if (!isNaN(valorElemento)) {
            suma_iva0 += valorElemento;
        }
    });
    document.getElementById('suma_iva0').value = fmtNum(suma_iva0);       
    

    
    // suma general
    document.getElementById('total_general').value =  fmtNum(suma_iva0 + suma_iva5 + suma_iva10 )


}









Venta.prototype.validar = function() {   
    

    var fecha = document.getElementById("fecha");    
    if (fecha.value === "") {
        boo.mostrarToast("La fecha esta vacia", "danger")
        fecha.focus();
        fecha.select();        
        return false;
    } 



    var selectElement = document.getElementById('venta_razon_social');
    if (!selectElement || (selectElement.options.length === 0 && selectElement.selectedIndex === -1)) {
        boo.mostrarToast("Seleccionar contribuyente", "danger")
        return false;
    }


    var total_general = document.getElementById('total_general');
    if (parseInt((NumQP(total_general.value))) <= 0 ){
        boo.mostrarToast("No existe monto para facturar", "danger")
        return false;
    }


    return true;
    
};





Venta.prototype.getjson = function( ) {  

    ret = "";


    // Supongamos que tienes referencias a tus elementos de input
    var establecimiento = document.getElementById('establecimiento');
    var punto_expedicion = document.getElementById('punto_expedicion');
    var fecha = document.getElementById('fecha');
    var naturaleza_receptor = document.getElementById('venta_naturaleza_receptor');
    var cliente = document.getElementById('venta_razon_social');



    // valores fijos 
    var tipo_transaccion = 1 ;
    var factura_numero = 0 ;
    var tipo_impuesto = 1;
    var tipo_moneda = 1;

    

    // Crear el objeto JSON
    var formData = {
        establecimiento: establecimiento.value,
        punto_expedicion: punto_expedicion.value,
        fecha_emision : fecha.value,
        naturaleza_receptor:   parseInt( naturaleza_receptor.value, 10),
        cliente :  parseInt( cliente.value, 10),        

        tipo_transaccion : tipo_transaccion,
        factura_numero : factura_numero,
        tipo_impuesto : tipo_impuesto,
        tipo_moneda : tipo_moneda
    };
  
   

    // detalle    
    var detalle = [];    
    var cuadriculaDetalles = document.getElementById('cuadricula_detalles');
    var lineas = cuadriculaDetalles.children;

    // Iterar sobre los elementos <div> en el primer nivel de anidamiento
    for (var i = 0; i < lineas.length; i++) {

        var lineaHTML = lineas[i];
        var nroLinea = lineaHTML.dataset.linea;

        // producto
        var selectElement = lineaHTML.querySelector('#producto_'+nroLinea);
        var valorSelect = selectElement.value;

        var cantidad = lineaHTML.querySelector('#cantidad_'+nroLinea);
        var precio_unitario = lineaHTML.querySelector('#preciounitario_'+nroLinea);

        var iva0 = lineaHTML.querySelector('#iva0_'+nroLinea);
        var iva5 = lineaHTML.querySelector('#iva5_'+nroLinea);
        var iva10 = lineaHTML.querySelector('#iva10_'+nroLinea);

        // Crear un objeto JSON para cada línea
        var lineaJSON = {
          producto : Number( NumQP( valorSelect )), 
          cantidad : Number( NumQP( cantidad.value )),         
          precio_unitario : Number( NumQP( precio_unitario.value )),    

          iva0 : Number( NumQP( iva0.value )),            
          iva5 : Number( NumQP( iva5.value )),            
          iva10 : Number( NumQP( iva10.value ))
        };
        
        // Agregar el objeto JSON al array de líneas
        detalle.push(lineaJSON);
    }
    
    formData.detalle = detalle ;      

    ret =  JSON.stringify(formData);
    return ret;
}










Venta.prototype.tabla_registro = function( id ) {    
    var obj = this;

    boo.loader.inicio();

    fetch('modulos/venta/form_registro.html')
        .then(response => response.text())
        .then(html => {
            document.getElementById(boo.main).innerHTML = html;
            return fetch('modulos/venta/form_caberera.html');
        })
        .then(response => response.text())
        .then(html => {
            document.getElementById('form_caberera').innerHTML = html;
            return fetch('modulos/venta/form_cuadricula.html');
        })
        .then(response => response.text())
        .then(html => {
            document.getElementById('form_cuadricula').innerHTML = html;


            ajax.api = ajax.getserver() + "/api/ventas/" + id;
            return ajax.promise.asyn("get");
        })
        .then(result => {
            obj.json = result.responseText;
            var ojson = JSON.parse( obj.json ) ; 

            // cargar la pagina
            document.getElementById('establecimiento').value =  ojson.establecimiento;
            document.getElementById('punto_expedicion').value =  ojson.punto_expedicion;

            numfactura = ojson.factura_numero;
            document.getElementById('factura_numero').value =  String(numfactura).padStart(7, '0');

            const fechaEmisionJSON = ojson.fecha_emision;            
            const fechaEmision = new Date(fechaEmisionJSON);
            const fechaFormateada = fechaEmision.toISOString().split('T')[0];
            document.getElementById('fecha').value = fechaFormateada;
            document.getElementById('fecha').disabled = true;




            // Obtén el elemento select
            const selectElement = document.getElementById('venta_naturaleza_receptor');
            const inputElement = document.createElement('input');
            inputElement.type = 'text'; // Puedes cambiar 'text' por 'date' u otro tipo según lo que necesites
            inputElement.id = 'naturaleza_receptor'; // Puedes mantener el mismo ID o asignarle uno nuevo            
            inputElement.classList.add('form-control', 'num');
            selectElement.parentNode.replaceChild(inputElement, selectElement);
            document.getElementById('naturaleza_receptor').value = ojson.cliente.naturaleza_receptor.naturaleza_receptor;
            document.getElementById('naturaleza_receptor').disabled = true;


            // Obtén el valor seleccionado
            var selectedValue = document.getElementById('naturaleza_receptor').value;
            var numericValue = parseInt(selectedValue, 10);


            if (numericValue === 1) {                        
                bloque_ruc.style.display = 'block';            
                bloque_digitov.style.display = 'block';            
                bloque_numero_documento.style.display = 'none';     
                
                document.getElementById('venta_ruc').value = fmtNum(ojson.cliente.ruc) ;
                document.getElementById('venta_ruc').disabled = true;

                document.getElementById('venta_digitov').value = ojson.cliente.digitov;
                document.getElementById('venta_digitov').disabled = true;

            }
            else {
                if (numericValue === 2) {                            
                    bloque_ruc.style.display = 'none';                        
                    bloque_digitov.style.display = 'none';                        
                    bloque_numero_documento.style.display = 'block';        
                    
                    //document.getElementById('numero_documento').value =  ojson.cliente.numero_documento ;
                    document.getElementById('numero_documento').value =  ojson.cliente.numero_documento ;
                    document.getElementById('numero_documento').disabled = true;
                } 
            }

            
            // razon_social
            const selectRazon_social = document.getElementById('venta_razon_social');
            const inputRazon_social = document.createElement('input');
            inputRazon_social.type = 'text'; 
            inputRazon_social.id = 'razon_social';             
            inputRazon_social.classList.add('form-control');
            selectRazon_social.parentNode.replaceChild(inputRazon_social, selectRazon_social);
            document.getElementById('razon_social').value = ojson.cliente.razon_social;
            document.getElementById('razon_social').disabled = true;            



            // sub totales 
            

            document.getElementById('suma_iva0').value = fmtNum(ojson.subtotal0) ;
            document.getElementById('suma_iva5').value = fmtNum(ojson.subtotal5) ;
            document.getElementById('suma_iva10').value = fmtNum(ojson.subtotal10) ;
            
            document.getElementById('total_general').value = fmtNum(ojson.total_monto) ;
            

            // cargar la cuadricula
            
            ojsonDetalle = JSON.stringify(ojson['detalle']); 
            jsonDetalle =  JSON.parse( ojsonDetalle ) ; 

            for (let i = 0; i < jsonDetalle.length; i++) {
                const filaDetalle = jsonDetalle[i];
                
                obj.html_generar_linea_ventas_detalles(i);
                document.getElementById('borrar_linea_'+i).style.display = 'none';

                // razon_social
                const producto_select = document.getElementById('producto_'+i);
                const producto_input = document.createElement('input');
                producto_input.type = 'text'; 
                producto_input.id = 'producto_'+i;                 
                producto_input.classList.add('form-control' ,'form-control-sm');
                producto_select.parentNode.replaceChild(producto_input, producto_select);
                document.getElementById( 'producto_'+i ).value = filaDetalle.producto.descripcion;
                document.getElementById( 'producto_'+i).disabled = true;                 
                
                document.getElementById('unidadmedida_'+i).value = filaDetalle.producto.unidad_medida.unimed;
                document.getElementById('cantidad_'+i).value = filaDetalle.cantidad;
                document.getElementById('cantidad_'+i).disabled = true;    
                
                document.getElementById('preciounitario_'+i).value = fmtNum(filaDetalle.precio_unitario);
                document.getElementById('iva0_'+i).value = fmtNum(filaDetalle.iva0);
                document.getElementById('iva5_'+i).value = fmtNum(filaDetalle.iva5);
                document.getElementById('iva10_'+i).value = fmtNum(filaDetalle.iva10);
            }



            // botonera
            obj.botones_accion_registro(id);




            //console.log(obj.json);
        })
        /*
        .catch(error => {
            console.error('Error al cargar la página:', error);
        })
        */
        .finally(() => {
            boo.loader.fin();
        });
    
}






Venta.prototype.botones_accion_registro = function( id ) {    
    
    var obj = this;
    
        
    var divBotonera = document.getElementById('botonera');
    /*divBotonera.classList.add('d-flex', 'justify-content-start', 'flex-row');*/

    // Crea un div con la clase 'col'
    var colDiv = document.createElement('div');
    colDiv.className = 'd-grid gap-2 d-md-flex justify-content-md-star mt-5 mb-1 ct'; 

    // Crea el botón de Guardar
    var botonImprimir = document.createElement('button');
    botonImprimir.type = 'button';
    botonImprimir.className = 'btn btn-primary me-md-2'; 
    botonImprimir.innerHTML = 'Imprimir';
    

    // Crea el botón de Cancelar
    var botonLista = document.createElement('button');
    botonLista.type = 'button';
    botonLista.className = 'btn btn-secondary'; 
    //botonLista.className = 'btn btn-link';     
    botonLista.innerHTML = 'Lista';


    // Agrega los botones al div 'col'
    colDiv.appendChild(botonImprimir);
    colDiv.appendChild(botonLista);

    // Agrega el div 'col' al contenedor principal
    divBotonera.appendChild(colDiv);

    
    
    botonImprimir.addEventListener('click', function() {


/*
        
        const protocol = window.location.protocol;
        const hostname = window.location.hostname;
        const port = window.location.port; 
        
        // URL relativa
        const relativeUrl = "/kudejasper/Kude/Reporte/factura.pdf?num=" + id;
        
        // Construir la URL completa incluyendo el puerto
        const fullUrl = protocol + "//" + hostname + (port ? ":" + port : "") + relativeUrl;
        
        window.open(fullUrl, '_blank');

*/



        const url = ajax.getserver() + "/Kude/Reporte/factura.pdf?num="+id;
        window.open(url, '_blank');



    });    
    


    
    botonLista.onclick =  function() {    
        obj.lista(1);
    };       
    

}

