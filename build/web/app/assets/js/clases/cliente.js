
function Cliente(){
    
    this.tipo = this.constructor.name.toLowerCase();    
    this.campoid =  this.constructor.name.toLowerCase();    
    this.json = ""; 
    this.tablacampos = ['cliente', 'tipo_contribuyente.descripcion', 'ruc'
        , 'razon_social'];
    this.page = 1;
   
    
    this.combobox = 
        {
            "naturaleza_receptor":{
                "value":"naturaleza_receptor",
                "inner":"descripcion"} ,       
            "pais":{
                "value":"pais",
                "inner":"nombre"} ,                                             
            "tipo_contribuyente":{
                "value":"tipo_contribuyente",
                "inner":"descripcion"} ,                                                             
            "departamento": {
                "value": "departamento",
                "inner": "descripcion"
            },                
            "tipo_documento": {
                "value": "tipo_documento",
                "inner": "descripcion"
            },                           
        };   

     
     
}





Cliente.prototype.lista = function( page ) {        
    
    var obj = this;
    //boo.vista.lista(this);              
            
    boo.loader.inicio();
    //var cliente = new Cliente();                                            
        
    // tal vez aca llame al la clase            
    fetch('modulos/cliente/lista.html')
        .then(response => response.text())
        .then(html => {
            
            document.getElementById(boo.main).innerHTML = html;
            obj.nuevo();    


            ajax.api = ajax.getserver() +  "/api/clientes?page="+page;
            ajax.json = null;
            
            ajax.promise.asyn("get").then(result => {
                obj.json = result.responseText;
                
                var ojson = JSON.parse( obj.json ) ; 
                boo.tabla.json = JSON.stringify(ojson['datos']);     

                boo.tabla.ini(obj);
                //boo.tabla.campos = ['cliente', 'tipo_contribuyente.descripcion'];
                boo.tabla.gene();                    
                
                
                //boo.tabla.lista_registro(obj, reflex.form_id_promise ); 
                boo.tabla.id = "cliente-tabla";
                boo.tabla.lista_registro(obj); 
                
                boo.vista.paginacion_html(obj,  JSON.stringify(ojson['paginacion']), page  );                              
            });
            
        })
        .catch(error => {
            console.error('Error al cargar la página:', error);
        })
        .finally(() => {               
            boo.loader.fin();
        });    

};








Cliente.prototype.tabla_registro = function( id ) {    
    var obj = this;
   
    boo.loader.inicio();

    fetch('modulos/cliente/form.html')
        .then(response => response.text())
        .then(html => {
            
            document.getElementById(boo.main).innerHTML = html;
            
            ajax.api = ajax.getserver() +  "/api/clientes/"+id;
            ajax.promise.asyn("get").then(result => {
                obj.json = result.responseText;
                                

                // cargar form
                boo.form.name = "form_" + obj.tipo;
                boo.form.json = obj.json;  
                boo.form.disabled(false);   
                boo.form.llenar();                
                boo.form.llenar_cbx(obj);  

                obj.botones_accion_registro(); 

                return obj.json;
            })
            // si es contribuyente o no
            .then((json) => {
            
                var bloque_ruc = document.getElementById('bloque_ruc');
                var bloque_digitov = document.getElementById('bloque_digitov');
                var bloque_numero_documento = document.getElementById('bloque_numero_documento');
                var bloque_tipo_documento = document.getElementById('bloque_tipo_documento');


                let ojson = JSON.parse(json);
                let naturalezaReceptorValue = ojson.naturaleza_receptor.naturaleza_receptor;
                if (naturalezaReceptorValue === 1) {
                    bloque_ruc.style.display = 'block';            
                    bloque_digitov.style.display = 'block';            
                    bloque_numero_documento.style.display = 'none';  
                    bloque_tipo_documento.style.display = 'none';  
                }
                else{
                    bloque_ruc.style.display = 'none';            
                    bloque_digitov.style.display = 'none';            
                    bloque_numero_documento.style.display = 'block';  
                    bloque_tipo_documento.style.display = 'block';                      
                }
            });
        })
        .catch(error => {
            console.error('Error al cargar la página:', error);
        })
        .finally(() => {               
            boo.loader.fin();
        });  

    
}









Cliente.prototype.nuevo = function( ) {                
    
    var obj = this;
    
        
    var cliente_nuevo = document.getElementById('cliente_nuevo');              
    cliente_nuevo.onclick = function(  )
    {   
        
        var contentContainer = document.getElementById('idmain');    
        contentContainer.innerHTML = '';

        boo.loader.inicio();
        
        fetch('modulos/cliente/form.html')
        .then(response => response.text())
        .then(html => {
            contentContainer.innerHTML = html;            
            obj.form_ini();
            return html;
        })
        .then(html => {                
            // Obtén la referencia al elemento por su ID

            
            var bloqueNumeroDocumento = document.getElementById('bloque_numero_documento');
            bloqueNumeroDocumento.style.display = 'none';
            

            var bloqueTipoDocumento = document.getElementById('bloque_tipo_documento');
            bloqueTipoDocumento.style.display = 'none';

            obj.carga_combos();
            obj.botones_accion_add();            
            return html;
        })
        .then(html => {
            obj.form_ini();
            return html;
        })
        /*
        .catch(error => {
            console.error(error);
        })
        */
        .finally(() => {               
            boo.loader.fin();
        }); 
     
    }    

};













Cliente.prototype.botones_accion_registro = function( ) {    
    var obj = this;
        
    var divBotonera = document.getElementById('botonera');
    /*divBotonera.classList.add('d-flex', 'justify-content-start', 'flex-row');*/

    // Crea un div con la clase 'col'
    var colDiv = document.createElement('div');
    colDiv.className = 'd-grid gap-2 d-md-flex justify-content-md-star mt-5 mb-1 ct'; 

    
    var botonEditar = document.createElement('button');
    botonEditar.type = 'button';
    botonEditar.className = 'btn btn-secondary me-md-2'; 
    botonEditar.innerHTML = 'Editar';
    
    
    
    var botonBorrar = document.createElement('button');
    botonBorrar.type = 'button';
    botonBorrar.className = 'btn btn-secondary me-md-2'; 
    botonBorrar.innerHTML = 'Borrar';
   
      

    // Crea el botón de Cancelar
    var botonLista = document.createElement('button');
    botonLista.type = 'button';
    botonLista.className = 'btn btn-primary'; 
    botonLista.innerHTML = 'Lista';


    // Agrega los botones al div 'col'
    colDiv.appendChild(botonEditar);
    colDiv.appendChild(botonBorrar);
    colDiv.appendChild(botonLista);

    // Agrega el div 'col' al contenedor principal
    divBotonera.appendChild(colDiv);



    botonEditar.addEventListener('click', function() {                 
        obj.botones_accion_editar();        
    });    



    botonBorrar.addEventListener('click', function() {                      
        obj.botones_accion_borrrar();         
    });    
    
    

    
    botonLista.addEventListener('click', function() {              
        obj.lista(1);
    });    

}





Cliente.prototype.carga_combos = function( dom ) {                
   

   var naturaleza_receptor = new NaturalezaReceptor();
   naturaleza_receptor.combobox("cliente_naturaleza_receptor");
   
   
   var pais = new Pais();
   pais.combobox("cliente_pais");


   var tipo_contribuyente = new TipoContribuyente();
   tipo_contribuyente.combobox("cliente_tipo_contribuyente");

    
   var departamento = new Departamento();
   departamento.combobox("cliente_departamento");
        

   var tipo_documento = new TipoDocumento();
   tipo_documento.combobox("cliente_tipo_documento");



};






Cliente.prototype.validar = function() {   
    

    var cliente_razon_social = document.getElementById('cliente_razon_social');    
    if (cliente_razon_social.value.trim() === "")         
    {
        boo.mostrarToast("Razon social no puede estar vacio", "danger")
        cliente_razon_social.focus();
        cliente_razon_social.select();        
        return false;
    }       

    
    return true;
    
};



            

Cliente.prototype.form_ini = function() {    

    var cliente_cliente = document.getElementById('cliente_cliente');          
    cliente_cliente.onblur  = function() {           
        cliente_cliente.value = fmtNum(cliente_cliente.value);                                    
    }    
    cliente_cliente.onblur();



    var cliente_ruc = document.getElementById('cliente_ruc');          
    cliente_ruc.onblur  = function() {           
        cliente_ruc.value = fmtNum(cliente_ruc.value);                                    
    }    
    cliente_ruc.onblur();

    
    var cliente_digitov = document.getElementById('cliente_digitov');          
    cliente_digitov.onblur  = function() {           
        cliente_digitov.value = fmtNum(cliente_digitov.value);                                    
    }    
    cliente_digitov.onblur();


    var cliente_numero_documento = document.getElementById('cliente_numero_documento');          
    cliente_numero_documento.onblur  = function() {           
        cliente_numero_documento.value = fmtNum(cliente_numero_documento.value);                                    
    }    
    cliente_numero_documento.onblur();


    var cliente_numero_casa = document.getElementById('cliente_numero_casa');          
    cliente_numero_casa.onblur  = function() {           
        cliente_numero_casa.value = fmtNum(cliente_numero_casa.value);                                    
    }    
    cliente_numero_casa.onblur();


    

   
    var bloque_ruc = document.getElementById('bloque_ruc');
    var bloque_digitov = document.getElementById('bloque_digitov');
    var bloque_numero_documento = document.getElementById('bloque_numero_documento');
    var bloque_tipo_documento = document.getElementById('bloque_tipo_documento');
   
    var cliente_naturaleza_receptor = document.getElementById('cliente_naturaleza_receptor');
    cliente_naturaleza_receptor.addEventListener('change', function() {
        // Obtén el valor seleccionado
        var selectedValue = cliente_naturaleza_receptor.value;
        var numericValue = parseInt(selectedValue, 10);
        

        if (numericValue === 1) {            
            
            bloque_ruc.style.display = 'block';            
            bloque_digitov.style.display = 'block';            
            bloque_numero_documento.style.display = 'none';  
            bloque_tipo_documento.style.display = 'none';  
            
        }
        else {
            if (numericValue === 2) {                            
                bloque_ruc.style.display = 'none';                        
                bloque_digitov.style.display = 'none';                        
                bloque_numero_documento.style.display = 'block';      
                bloque_tipo_documento.style.display = 'block';                    
            } 
        }

    });

    


};









Cliente.prototype.botones_accion_add = function( ) {    
    
    var obj = this;
    document.getElementById('cliente_cliente').disabled = true;
        
    var divBotonera = document.getElementById('botonera');
    /*divBotonera.classList.add('d-flex', 'justify-content-start', 'flex-row');*/

    // Crea un div con la clase 'col'
    var colDiv = document.createElement('div');
    colDiv.className = 'd-grid gap-2 d-md-flex justify-content-md-star mt-5 mb-1 ct'; 

    // Crea el botón de Guardar
    var botonGuardar = document.createElement('button');
    botonGuardar.type = 'button';
    botonGuardar.className = 'btn btn-primary me-md-2'; 
    botonGuardar.innerHTML = 'Guardar';
    

    // Crea el botón de Cancelar
    var botonCancelar = document.createElement('button');
    botonCancelar.type = 'button';
    botonCancelar.className = 'btn btn-secondary'; 
    botonCancelar.innerHTML = 'Cancelar';


    // Agrega los botones al div 'col'
    colDiv.appendChild(botonGuardar);
    colDiv.appendChild(botonCancelar);

    // Agrega el div 'col' al contenedor principal
    divBotonera.appendChild(colDiv);

    
    botonGuardar.addEventListener('click', function() {
        
        //let obj = new Cliente();                
        if ( obj.validar()){      
            
            boo.form.name = "form_cliente";
            console.log( boo.form.getjson());
                        
            ajax.api = ajax.getserver() +  "/api/clientes"
            ajax.json =  boo.form.getjson();
            
            ajax.promise.asyn("post").then(result => {                
                json = result.responseText;            

                if (result.status == 200){
                    // mostrar lista
                    obj.lista(1);
                    boo.mostrarToast("Registro agregado", "success");
                }
                else {
                    boo.mostrarToast( result.responseText, "danger");
                }

            });  

        }      
    });    
    
    
    botonCancelar.onclick =  function() {    
        obj.lista(1);
    };       
    


}




Cliente.prototype.botones_accion_editar = function( ) {    
    var obj = this;
    
    boo.form.name = "form_" + obj.tipo;
    boo.form.disabled(true);    
    document.getElementById('cliente_cliente').disabled = true;

    obj.carga_combos();
    obj.form_ini();
       
    
    var divBotonera = document.getElementById('botonera');
    /*divBotonera.classList.add('d-flex', 'justify-content-start', 'flex-row');*/

    // Crea un div con la clase 'col'
    var colDiv = document.createElement('div');
    colDiv.className = 'd-grid gap-2 d-md-flex justify-content-md-star mt-5 mb-1 ct'; 

    
    var botonEditar = document.createElement('button');
    botonEditar.type = 'button';
    botonEditar.className = 'btn btn-primary me-md-2'; 
    botonEditar.innerHTML = 'Editar';
    
    

    // Crea el botón de Cancelar
    var botonCancelar = document.createElement('button');
    botonCancelar.type = 'button';
    botonCancelar.className = 'btn btn-secondary'; 
    botonCancelar.innerHTML = 'Cancelar';    
    
    

    // Agrega los botones al div 'col'
    colDiv.appendChild(botonEditar);
    colDiv.appendChild(botonCancelar);
    
    
    // Agrega el div 'col' al contenedor principal
    divBotonera.innerHTML = "";
    divBotonera.appendChild(colDiv);    
    
    
    
    botonEditar.onclick =  function() {
        
        if ( obj.validar()){                  

            var id = document.getElementById('cliente_cliente');    
            
            boo.form.name = "form_cliente";                                    
            boo.form.getjson();
            
            ajax.api = ajax.getserver() +  "/api/clientes/"+id.value
            ajax.json =  boo.form.getjson();
                        
            ajax.promise.asyn("put").then(result => {                
                json = result.responseText;            

                if (result.status == 200){
                    // mostrar lista
                    obj.lista(1);
                    boo.mostrarToast("Registro editado", "success");
                }
                else {
                    boo.mostrarToast("Error", "danger");
                }

            });  
        
        } 
        
    };    
    





    botonCancelar.addEventListener('click', function() {                      
        // cancelar volver a al pantalla anterior
        var id = document.getElementById('cliente_cliente');    
        obj.tabla_registro(id.value);        
    });       
    
    
    
}




    
    
Cliente.prototype.botones_accion_borrrar = function( ) {    
    var obj = this;
    
    
    var divBotonera = document.getElementById('botonera');
    /*divBotonera.classList.add('d-flex', 'justify-content-start', 'flex-row');*/

    // Crea un div con la clase 'col'
    var colDiv = document.createElement('div');
    colDiv.className = 'd-grid gap-2 d-md-flex justify-content-md-star mt-5 mb-1 ct'; 

    
    var botonBorrar = document.createElement('button');
    botonBorrar.type = 'button';
    botonBorrar.className = 'btn btn-primary me-md-2'; 
    botonBorrar.innerHTML = 'Borrar';
    
    

    // Crea el botón de Cancelar
    var botonCancelar = document.createElement('button');
    botonCancelar.type = 'button';
    botonCancelar.className = 'btn btn-secondary'; 
    botonCancelar.innerHTML = 'Cancelar';    
    
    

    // Agrega los botones al div 'col'
    colDiv.appendChild(botonBorrar);
    colDiv.appendChild(botonCancelar);
    
    
    // Agrega el div 'col' al contenedor principal
    divBotonera.innerHTML = "";
    divBotonera.appendChild(colDiv);    
    
    
    botonBorrar.addEventListener('click', function() {                              
        console.log("borrar")
    });   
    

    
    botonBorrar.onclick =  function() {

        //let obj = new Cliente();                
        if ( true ){                  

            var id = document.getElementById('cliente_cliente');    
            
            boo.form.name = "form_cliente";                                    

            ajax.api = ajax.getserver() +  "/api/clientes/"+id.value
            ajax.json =  null;
                        
            ajax.promise.asyn("delete").then(result => {                
                json = result.responseText;            

                if (result.status == 200){
                    // mostrar lista
                    obj.lista(1);
                    boo.mostrarToast("Registro eliminado", "success");
                }
                else {
                    boo.mostrarToast("Error", "danger");
                }

            });  
        
        } 
        
    };    
    


    botonCancelar.onclick =  function() {    
        // cancelar volver a al pantalla anterior
        var id = document.getElementById('cliente_cliente');    
        obj.tabla_registro(id.value);        
    };       
    
    
    
}

    
    
    
    

Cliente.prototype.combobox_razon_social = function( dom ) {                
    

    var dom = document.getElementById( dom );
    var idedovalue = dom.value;

    ajax.api = ajax.getserver() +  "/api/naturalezareceptor/all"        
    ajax.promise.asyn("get").then(result => {

        var ojson = JSON.parse( result.responseText ) ;    
        for( x=0; x < ojson.length; x++ ) {

            var jsonvalue = (ojson[x]['naturaleza_receptor'] );            

            if (idedovalue != jsonvalue )
            {  
                var opt = document.createElement('option');            
                opt.value = jsonvalue;
                opt.innerHTML = ojson[x]['descripcion'];                        
                dom.appendChild(opt);                     
            }
        }    
    });

    var choices = new Choices(dom, {
        position: 'bottom'
        /* Opciones adicionales según tus necesidades */
    }); 


};    