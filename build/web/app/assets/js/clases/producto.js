
function Producto(){
    
    this.tipo = this.constructor.name.toLowerCase();    
    this.campoid =  this.constructor.name.toLowerCase();    
    this.json = ""; 
    this.tablacampos = ['producto', 'descripcion', 'precio_unitario',
            'unidad_medida.descripcion', 'tasa_iva.descripcion' ];
        
    this.page = 1;
   
    
    this.combobox = 
        {
            "unidad_medida":{
                "value":"unidad_medida",
                "inner":"descripcion"} ,  

            "afectacion_iva":{
                "value":"afectacion_iva",
                "inner":"descripcion"} ,      

            "tasa_iva": {
                "value": "tasa_iva",
                "inner": "descripcion"
            }                
        };   


}





Producto.prototype.lista = function( page ) {        
    
    var obj = this;
            
    boo.loader.inicio();
        
    // tal vez aca llame al la clase            
    fetch('modulos/producto/lista.html')
        .then(response => response.text())
        .then(html => {
            
            document.getElementById(boo.main).innerHTML = html;
            obj.nuevo();    


            ajax.api = ajax.getserver() +  "/api/productos?page="+page;
            ajax.json = null;
            
            ajax.promise.asyn("get").then(result => {
                obj.json = result.responseText;
                
                var ojson = JSON.parse( obj.json ) ; 
                boo.tabla.json = JSON.stringify(ojson['datos']);     

                boo.tabla.ini(obj);                
                boo.tabla.gene();  
                
                //boo.tabla.lista_registro(obj, reflex.form_id_promise ); 
                boo.tabla.id = "producto-tabla";
                boo.tabla.lista_registro(obj); 
                
                boo.vista.paginacion_html(obj,  JSON.stringify(ojson['paginacion']), page  );                              
            });
            
        })
        .catch(error => {
            console.error('Error al cargar la página:', error);
        })
        .finally(() => {               
            boo.loader.fin();
        });    

};








Producto.prototype.tabla_registro = function( id ) {    
    var obj = this;
   
    boo.loader.inicio();

    fetch('modulos/producto/form.html')
        .then(response => response.text())
        .then(html => {
            
            document.getElementById(boo.main).innerHTML = html;
            
            ajax.api = ajax.getserver() +  "/api/productos/"+id;
            ajax.promise.asyn("get").then(result => {
                obj.json = result.responseText;                                

                // cargar form
                boo.form.name = "form_" + obj.tipo;
                boo.form.json = obj.json;   
                boo.form.disabled(false);   
                boo.form.llenar();                
                boo.form.llenar_cbx(obj);  



                obj.botones_accion_registro(); 
            });
            

        })
        .catch(error => {
            console.error('Error al cargar la página:', error);
        })
        .finally(() => {               
            boo.loader.fin();
        });  

    
}









Producto.prototype.nuevo = function( ) {                
    
    var obj = this;
        
    var producto_nuevo = document.getElementById('producto_nuevo');              
    producto_nuevo.onclick = function(  )
    {   
        
        var contentContainer = document.getElementById('idmain');    
        contentContainer.innerHTML = '';

        boo.loader.inicio();
        
        fetch('modulos/producto/form.html')
        .then(response => response.text())
        .then(html => {
            contentContainer.innerHTML = html;            
            obj.form_ini();
            return html;
        })
        .then(html => {                
    
            obj.carga_combos();
            obj.botones_accion_add();            
            return html;
        })
        .then(html => {
            //obj.init();
            return html;
        })
        /*
        .catch(error => {
            console.error(error);
        })
        */
        .finally(() => {               
            boo.loader.fin();
        }); 
     
    }    

};













Producto.prototype.botones_accion_registro = function( ) {    
    var obj = this;
        
    var divBotonera = document.getElementById('botonera');
    /*divBotonera.classList.add('d-flex', 'justify-content-start', 'flex-row');*/

    // Crea un div con la clase 'col'
    var colDiv = document.createElement('div');
    colDiv.className = 'd-grid gap-2 d-md-flex justify-content-md-star mt-5 mb-1 ct'; 

    
    var botonEditar = document.createElement('button');
    botonEditar.type = 'button';
    botonEditar.className = 'btn btn-secondary me-md-2'; 
    botonEditar.innerHTML = 'Editar';
    
    
    
    var botonBorrar = document.createElement('button');
    botonBorrar.type = 'button';
    botonBorrar.className = 'btn btn-secondary me-md-2'; 
    botonBorrar.innerHTML = 'Borrar';
   
      

    // Crea el botón de Cancelar
    var botonLista = document.createElement('button');
    botonLista.type = 'button';
    botonLista.className = 'btn btn-primary'; 
    botonLista.innerHTML = 'Lista';


    // Agrega los botones al div 'col'
    colDiv.appendChild(botonEditar);
    colDiv.appendChild(botonBorrar);
    colDiv.appendChild(botonLista);

    // Agrega el div 'col' al contenedor principal
    divBotonera.appendChild(colDiv);



    botonEditar.addEventListener('click', function() {                 
        obj.botones_accion_editar();        
    });    



    botonBorrar.addEventListener('click', function() {                      
        obj.botones_accion_borrrar();         
    });    
    
    

    
    botonLista.addEventListener('click', function() {              
        obj.lista(1);
    });    

}





Producto.prototype.carga_combos = function( dom ) {                
   

   var unidad_medida = new UnidadMedida();
   unidad_medida.combobox("producto_unidad_medida");


   var afectacion_iva = new AfectacionIVA();
   afectacion_iva.combobox("producto_fectacion_iva");
   


   var tasa_iva = new TasaIVA();
   tasa_iva.combobox("producto_tasa_iva");

};








Producto.prototype.validar = function() {   
    

    var producto_descripcion = document.getElementById('producto_descripcion');    
    if (producto_descripcion.value.trim() === "")         
    {
        boo.mostrarToast("La descripcion no puede estar vacia", "danger")
        producto_descripcion.focus();
        producto_descripcion.select();        
        return false;
    }       


              

    var producto_precio_variable = document.getElementById('producto_precio_variable');            
    if (producto_precio_variable.checked == false ) {

        var producto_precio_unitario = document.getElementById('producto_precio_unitario'); 
        if (parseInt((producto_precio_unitario.value)) <= 0 )         
        {        
            boo.mostrarToast("Error precio unitario", "danger")
            producto_precio_unitario.focus();
            producto_precio_unitario.select();                
            return false;
        }       

    }
    else{
        var producto_precio_unitario = document.getElementById('producto_precio_unitario'); 
        producto_precio_unitario.value = 0;
    }


    return true;
    
    
};




            

Producto.prototype.form_ini = function() {    

    var producto_producto = document.getElementById('producto_producto');          
    producto_producto.onblur  = function() {           
        producto_producto.value = fmtNum(producto_producto.value);                                    
    }    
    producto_producto.onblur();



    var producto_precio_unitario = document.getElementById('producto_precio_unitario');          
    producto_precio_unitario.onblur  = function() {           
        producto_precio_unitario.value = fmtNum(producto_precio_unitario.value);                                    
    }    
    producto_precio_unitario.onblur();

    

};









Producto.prototype.botones_accion_add = function( ) {    
    var obj = this;
    document.getElementById('producto_producto').disabled = true;
        
    var divBotonera = document.getElementById('botonera');
    /*divBotonera.classList.add('d-flex', 'justify-content-start', 'flex-row');*/

    // Crea un div con la clase 'col'
    var colDiv = document.createElement('div');
    colDiv.className = 'd-grid gap-2 d-md-flex justify-content-md-star mt-5 mb-1 ct'; 

    // Crea el botón de Guardar
    var botonGuardar = document.createElement('button');
    botonGuardar.type = 'button';
    botonGuardar.className = 'btn btn-primary me-md-2'; 
    botonGuardar.innerHTML = 'Guardar';
    

    // Crea el botón de Cancelar
    var botonCancelar = document.createElement('button');
    botonCancelar.type = 'button';
    botonCancelar.className = 'btn btn-secondary'; 
    botonCancelar.innerHTML = 'Cancelar';


    // Agrega los botones al div 'col'
    colDiv.appendChild(botonGuardar);
    colDiv.appendChild(botonCancelar);

    // Agrega el div 'col' al contenedor principal
    divBotonera.appendChild(colDiv);

    
    botonGuardar.addEventListener('click', function() {
        
        //let obj = new Cliente();                
        if ( obj.validar()){      
            
            boo.form.name = "form_producto";
                        
            ajax.api = ajax.getserver() +  "/api/productos"
            ajax.json =  boo.form.getjson();
            

            ajax.promise.asyn("post").then(result => {                
                json = result.responseText;            

                if (result.status == 200){
                    // mostrar lista
                    obj.lista(1);
                    boo.mostrarToast("Registro agregado", "success");
                }
                else {
                    boo.mostrarToast( result.responseText, "danger");
                }

            });  

        }      
    });    
    
    
    botonCancelar.onclick =  function() {    
        obj.lista(1);
    };       
    


}




Producto.prototype.botones_accion_editar = function( ) {    
    var obj = this;
    
    boo.form.name = "form_" + obj.tipo;
    boo.form.disabled(true);    
    document.getElementById('producto_producto').disabled = true;

    obj.carga_combos();
       
    
    var divBotonera = document.getElementById('botonera');
    /*divBotonera.classList.add('d-flex', 'justify-content-start', 'flex-row');*/

    // Crea un div con la clase 'col'
    var colDiv = document.createElement('div');
    colDiv.className = 'd-grid gap-2 d-md-flex justify-content-md-star mt-5 mb-1 ct'; 

    
    var botonEditar = document.createElement('button');
    botonEditar.type = 'button';
    botonEditar.className = 'btn btn-primary me-md-2'; 
    botonEditar.innerHTML = 'Editar';
    
    

    // Crea el botón de Cancelar
    var botonCancelar = document.createElement('button');
    botonCancelar.type = 'button';
    botonCancelar.className = 'btn btn-secondary'; 
    botonCancelar.innerHTML = 'Cancelar';    
    
    

    // Agrega los botones al div 'col'
    colDiv.appendChild(botonEditar);
    colDiv.appendChild(botonCancelar);
    
    
    // Agrega el div 'col' al contenedor principal
    divBotonera.innerHTML = "";
    divBotonera.appendChild(colDiv);    
    
    
    
    botonEditar.onclick =  function() {
        
        if ( obj.validar()){                  

            var id = document.getElementById('producto_producto');    
            
            boo.form.name = "form_producto";                                    
            boo.form.getjson();


            ajax.api = ajax.getserver() +  "/api/productos/"+id.value
            ajax.json =  boo.form.getjson();
                        
            ajax.promise.asyn("put").then(result => {                
                json = result.responseText;            

                if (result.status == 200){
                    // mostrar lista
                    obj.lista(1);
                    boo.mostrarToast("Registro editado", "success");
                }
                else {
                    boo.mostrarToast("Error", "danger");
                }

            });  
        
        } 
        
    };    
    





    botonCancelar.addEventListener('click', function() {                      
        // cancelar volver a al pantalla anterior
        var id = document.getElementById('producto_producto');    
        obj.tabla_registro(id.value);        
    });       
    
    
    
}




    
    
Producto.prototype.botones_accion_borrrar = function( ) {    
    var obj = this;
    
    
    var divBotonera = document.getElementById('botonera');
    /*divBotonera.classList.add('d-flex', 'justify-content-start', 'flex-row');*/

    // Crea un div con la clase 'col'
    var colDiv = document.createElement('div');
    colDiv.className = 'd-grid gap-2 d-md-flex justify-content-md-star mt-5 mb-1 ct'; 

    
    var botonBorrar = document.createElement('button');
    botonBorrar.type = 'button';
    botonBorrar.className = 'btn btn-primary me-md-2'; 
    botonBorrar.innerHTML = 'Borrar';
    
    

    // Crea el botón de Cancelar
    var botonCancelar = document.createElement('button');
    botonCancelar.type = 'button';
    botonCancelar.className = 'btn btn-secondary'; 
    botonCancelar.innerHTML = 'Cancelar';    
    
    

    // Agrega los botones al div 'col'
    colDiv.appendChild(botonBorrar);
    colDiv.appendChild(botonCancelar);
    
    
    // Agrega el div 'col' al contenedor principal
    divBotonera.innerHTML = "";
    divBotonera.appendChild(colDiv);    
    
    
    botonBorrar.addEventListener('click', function() {                              
        console.log("borrar")
    });   
    

    
    botonBorrar.onclick =  function() {

        //let obj = new Cliente();                
        if ( true ){                  

            var id = document.getElementById('producto_producto');    
            
            boo.form.name = "form_producto";                                    

            ajax.api = ajax.getserver() +  "/api/productos/"+id.value
            ajax.json =  null;
                        
            ajax.promise.asyn("delete").then(result => {                
                json = result.responseText;            

                if (result.status == 200){
                    // mostrar lista
                    obj.lista(1);
                    boo.mostrarToast("Registro eliminado", "success");
                }
                else {
                    boo.mostrarToast("Error", "danger");
                }

            });  
        
        } 
        
    };    
    


    botonCancelar.onclick =  function() {    
        // cancelar volver a al pantalla anterior
        var id = document.getElementById('producto_producto');    
        obj.tabla_registro(id.value);        
    };       
    
    
    
}

    