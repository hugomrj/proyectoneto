/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



package nebuleuse.util;

/**
 *
 * @author hugom_000
 */


public abstract class Webinf {
    
    public static String path () {
    
        String path = Webinf.class.getProtectionDomain().getCodeSource().getLocation().getPath();     
        
        
        int index = path.indexOf("WEB-INF");           
        if (index != -1) {    
            
            path = path.substring(0, index + "WEB-INF".length());            
        } 
        
        return path;        
    }
        
}
