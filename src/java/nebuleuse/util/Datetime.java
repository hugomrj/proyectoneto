/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nebuleuse.util;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 *
 * @author hugo romero
 */
public abstract class  Datetime {    
    
    
    public static String toSQLDate(Date fecha) {   
        
        if (fecha == null) 
        {
            return "";                    
        }
        else 
        {
            java.sql.Date sqlDate = new java.sql.Date(fecha.getTime());
            return sqlDate.toString();        
        }        
    }

    
    public static String toSQLDate(Object objeto) {
        
        Date fecha = new Date();
        fecha = (Date) objeto;        
        return  toSQLDate(fecha);
        
    }
        
    
    public static Date castDate(String strFecha) {  

        strFecha = strFecha.replaceAll("\"", "");
        
        java.util.Date date = new java.util.Date();
        
         if (  (strFecha.trim().equals(""))   ||  ((strFecha.trim().equals("null"))) ) 
         {
            date = null;
         }
         else
         {
             try 
            {
                SimpleDateFormat formato = null;
                if ( Cadena.contChr( strFecha, '/' ) == 2 ){
                    formato = new SimpleDateFormat("dd/MM/yyyy");                    
                }
                if ( Cadena.contChr( strFecha, '-' ) == 2 ){
                    formato = new SimpleDateFormat("yyyy-MM-dd");
                }
                if ( Cadena.contChr( strFecha, ' ' ) == 2 ){
                    formato = new SimpleDateFormat("dd MMM yyyy");
                }
                
                
                date = formato.parse(strFecha);

/*
                if ( !(strFecha.trim().equals(  Datetime.toSQLDate(date).trim()))
                        &&
                        !(strFecha.trim().equals(  Datetime.toSQLDateFormatoDiagonal(date).trim()))
                        )
                {
                    date = null;
                }
*/          
                
            } 
            catch (Exception ex) 
            {
                date = null;
                System.out.println("error sala aca");
                System.out.println(ex.getMessage());
                
                
                //Logger.getLogger(Datetime.class.getName()).log(Level.SEVERE, null, ex);
            }
         }        
         
         return date;            
    }    

    
    public static String toSQLDateFormatoDiagonal(Date fecha) {   
        
        if (fecha == null) 
        {
            return "";                    
        }
        else 
        {
            java.text.SimpleDateFormat sdf=new java.text.SimpleDateFormat("dd/MM/yyyy");
            String restorno = sdf.format(fecha);            

            return restorno;        
        }        
    }    

    
    
    public static String nombreMes(Integer mes) {   
        
        String ret = "";
        
        switch (mes) {
            case 1:
              ret = "enero";
              break;
            case 2:
              ret = "febrero";
              break;
            case 3:
              ret = "marzo";
              break;
            case 4:
              ret = "abril";
              break;
            case 5:
              ret = "mayo";
              break;
            case 6:
              ret = "junio";
              break;
            case 7:
              ret = "julio";
              break;
            case 8:
              ret = "agosto";
              break;
            case 9:
              ret = "septiembre";
              break;
            case 10:
              ret = "octubre";
              break;
            case 11:
              ret = "noviembre";
              break;
            case 12:
              ret = "diciembre";
              break;
        }

        return ret;
    }       
    
    
    
}





















