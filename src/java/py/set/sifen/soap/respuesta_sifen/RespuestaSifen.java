
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
 
package py.set.sifen.soap.respuesta_sifen;




public class RespuestaSifen {
    
    private Integer id;
    private Integer venta;
    private String cdc;
    private String lote;
    private String codigo_qr;
    private String estado;
    private String mensaje_lote;
    private String estado_lote;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVenta() {
        return venta;
    }

    public void setVenta(Integer venta) {
        this.venta = venta;
    }

    public String getCdc() {
        return cdc;
    }

    public void setCdc(String cdc) {
        this.cdc = cdc;
    }

    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    public String getCodigo_qr() {
        return codigo_qr;
    }

    public void setCodigo_qr(String codigo_qr) {
        this.codigo_qr = codigo_qr;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMensaje_lote() {
        return mensaje_lote;
    }

    public void setMensaje_lote(String mensaje_lote) {
        this.mensaje_lote = mensaje_lote;
    }

    public String getEstado_lote() {
        return estado_lote;
    }

    public void setEstado_lote(String estado_lote) {
        this.estado_lote = estado_lote;
    }
    


}


