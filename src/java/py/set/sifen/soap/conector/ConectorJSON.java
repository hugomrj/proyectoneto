
package py.set.sifen.soap.conector;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import nebuleuse.ORM.postgres.Conexion;
import org.json.JSONArray;
import org.json.JSONObject;
//import py.set.sifen.Util;


public class ConectorJSON {
    
    //conexionBD conexion = new conexionBD();    
    Conexion conexion = new Conexion();    
    Statement  statement ;
    ResultSet resultset;          
    
    
    
    public String jsonEnviar ( Integer idVenta   ) throws Exception {

        String jsonResult = "";
                               
        ConectorSQL facturacionSQL = new ConectorSQL();        
        
        String sql = facturacionSQL.facturaDE(idVenta);
                
        String sqldetalle = facturacionSQL.facturaDEtalles(idVenta);
                
        JSONObject jsonObject = null;
        
        try {
            
            JSONArray detalle = this.jsonDetalle(sqldetalle);
            
            jsonObject = this.jsonObjetCabecera(sql);
            
            jsonObject.put("Detalles", detalle);
                        
            jsonResult = jsonObject.toString();
            
        } catch (Exception ex) {
            Logger.getLogger(ConectorJSON.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            
            return jsonResult;
        }
        
        
    }
    
      
    


    public JSONObject jsonObjetCabecera(String sql) throws Exception {
        conexion.conectar();

        try (Statement statement = conexion.getConexion().createStatement();
             ResultSet resultSet = statement.executeQuery(sql)) {

            JSONObject jsonObject = new JSONObject();

            if (resultSet.next()) {
                int totalColumns = resultSet.getMetaData().getColumnCount();

                for (int i = 1; i <= totalColumns; i++) {
                    String columnName = resultSet.getMetaData().getColumnLabel(i);

                    // Manejo especial para valores numéricos
                    Object columnValue = resultSet.getObject(i);
                    if (columnValue != null) {
                        if (columnValue instanceof Number) {
                            jsonObject.put(columnName, (Number) columnValue);
                        } else {
                            // Puedes agregar más lógica para manejar otros tipos de datos
                            jsonObject.put(columnName, columnValue.toString());
                        }
                    }
                    // No hagas nada si el valor es nulo
                }
            }

            return jsonObject;
        } finally {
            // Cierra la conexión
            conexion.desconectar();
        }
    }

    
    
    
    public JSONArray jsonDetalle(String sql) throws Exception {
        conexion.conectar();

        try (Statement statement = conexion.getConexion().createStatement();
             ResultSet resultSet = statement.executeQuery(sql)) {

            JSONArray jsonArray = new JSONArray();

            while (resultSet.next()) {
                JSONObject jsonObject = new JSONObject();

                int totalColumns = resultSet.getMetaData().getColumnCount();

                for (int i = 1; i <= totalColumns; i++) {
                    String columnName = resultSet.getMetaData().getColumnLabel(i);

                    // Manejo especial para valores numéricos
                    Object columnValue = resultSet.getObject(i);
                    if (columnValue != null) {
                        if (columnValue instanceof Number) {
                            jsonObject.put(columnName, (Number) columnValue);
                        } else {
                            
                            jsonObject.put(columnName, columnValue.toString());
                        }
                    }
                    // No hagas nada si el valor es nulo
                }      
                jsonArray.put(jsonObject);
            }

            return jsonArray;
        } finally {
            // Cierra la conexión
            conexion.desconectar();
        }
    }

    


    
    

    
}
