/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.set.sifen.soap.conector;

import py.set.sifen.soap.respuesta_sifen.*;
import nebuleuse.ORM.db.Persistencia;
import py.set.sifen.soap.conector.Conector;

/**
 *
 * @author hugo
 */
public class ConectorSOAP {
    
    Persistencia persistencia = new Persistencia();  
    
    
    public RespuestaSifen insertar( Integer venta) throws Exception {
        
                
        Conector conector = new Conector();        
        conector.enviar(venta);
        
               
        RespuestaSifen respuestaSifen = new RespuestaSifen();    
        
        respuestaSifen.setVenta(venta);
        respuestaSifen.setLote(conector.getNumero_lote());
        respuestaSifen.setCodigo_qr(conector.getCodigo_qr());
        respuestaSifen.setCdc(conector.getCdc());
        
        
        respuestaSifen =  (RespuestaSifen) this.persistencia.insert(respuestaSifen) ;   
        
        
        
    
        return respuestaSifen;
    }
    
    
}
