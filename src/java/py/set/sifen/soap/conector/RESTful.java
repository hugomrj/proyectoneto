
package py.set.sifen.soap.conector;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;



public class RESTful {


    
    private HttpClient httpClient = HttpClient.newBuilder().build();
    //private String url = Servidor.SERVER_URL +"/soap/inpace/async/recibe";        */    
    
    private String ambiente = "soap";
    private String owner = "magna";
    
    private Integer respuesta_codigo ;
    private String respuesta_mensaje ;
    public String nombre_api ;
    public HttpResponse<String> response ;
    
        
    
    
    public void conectar (String json) {
      
       
        
        String apiUrl =  Servidor.SERVER_URL +"/"+ this.ambiente +"/"+ this.owner
                + nombre_api;   
/*        
        System.out.println(apiUrl);
        System.out.println(new Seguridad().getTokken());
*/
        
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(apiUrl))
                .header("Content-Type", "application/json")
                .header("token", new Seguridad().getTokken())
                .POST(HttpRequest.BodyPublishers.ofString(json))
                .build();            
                  
        
        try {
            // Envía la solicitud y obtén la respuesta
            this.response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            
        }
        catch (IOException | InterruptedException e) {
            //e.printStackTrace();
            System.err.println(e.getCause());
        }
                        
        
    
    }

    
    
    public void setAmbiente(String ambiente) {
        this.ambiente = ambiente;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
    
    public Integer getRespuesta_codigo() {
        return respuesta_codigo;
    }

    public String getRespuesta_mensaje() {
        return respuesta_mensaje;
    }    
    
}
