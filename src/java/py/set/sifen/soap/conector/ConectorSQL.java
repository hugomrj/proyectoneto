/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.set.sifen.soap.conector;

/**
 *
 * @author hugo
 */
public class ConectorSQL {
    
    
    
    
    
    
 
            
    
    
    public String facturaDE ( Integer venta ) 
            throws Exception {
    
        String sql = "";                                  
                
        String no_moneda_guarani = "1 as dCondTiCam, ";                  
        

        sql = """          

        SELECT 
            1 AS "iTipEmi",    'Normal' AS "dDesTipEmi",
            lpad(floor(random() * 999999999 + 1)::bigint::text, 9, '0') AS "dCodSeg",
            1 AS "iTiDE",    'Factura electrónica' AS "dDesTiDE",
            numero_timbrado AS "dNumTim",    establecimiento AS "dEst", punto_expedicion AS "dPunExp",
            lpad(factura_numero::text, 7, '0') AS "dNumDoc", emisor.fecha_inicio "dFeIniT", 
            to_char(fecha_emision, 'YYYY-MM-DD"T"HH24:MI:SS') AS "dFeEmiDE",
            tipos_transacciones.tipo_transaccion "iTipTra", tipos_transacciones.descripcion "dDesTipTra",
            tipos_impuestos.tipo_impuesto "iTImp", 	tipos_impuestos.descripcion "dDesTImp",
            tipos_monedas.moneda_operacion  "cMoneOpe", tipos_monedas.descripcion "dDesMoneOpe",
            emisor.ruc_emisor "dRucEm", emisor.digitov_emisor "dDVEmi", emisor.tipo_contribuyente "iTipCont",
            emisor.nombre_emisor  "dNomEmi", emisor.direccion_emisor  "dDirEmi", emisor.numero_casa  "dNumCas",
            emisor.departamento_emisor  "cDepEmi", emisor.departamento_descripcion_emisor "dDesDepEmi", 
            distrito_emisor  "cDisEmi", distrito_descripcion_emisor "dDesDisEmi", ciudad_emisor "cCiuEmi",
            ciudad_descripcion_emisor "dDesCiuEmi", telefono_emisor "dTelEmi", email_emisor "dEmailE",
            actividad_emisor "cActEco", actividad_descripcion_emisor "dDesActEco",
            clientes.naturaleza_receptor "iNatRec", 2 "iTiOpe", paises.pais "cPaisRec", paises.nombre "dDesPaisRe",			
            CASE WHEN clientes.naturaleza_receptor = 1 then clientes.tipo_contribuyente  ELSE NULL END AS "iTiContRec",
            CASE WHEN clientes.naturaleza_receptor = 1 then clientes.ruc  ELSE NULL END AS "dRucRec",
            CASE WHEN clientes.naturaleza_receptor = 1 then clientes.digitov  ELSE NULL END AS "dDVRec",
            CASE WHEN clientes.naturaleza_receptor = 2 then clientes.tipo_documento  ELSE NULL END AS "iTipIDRec",
            CASE WHEN clientes.naturaleza_receptor = 2 then tipos_documentos.descripcion  ELSE NULL END AS "dDTipIDRec",
            CASE WHEN clientes.naturaleza_receptor = 2 then clientes.numero_documento  ELSE NULL END AS "dNumIDRec",
            CASE WHEN clientes.naturaleza_receptor = 2 then clientes.numero_documento  ELSE NULL END AS "dNomRec",
            clientes.razon_social "dNomRec", 
            1 "iIndPres", 'Operación presencial' "dDesIndPres",
            1 "iCondOpe", 'Contado' "dDCondOpe", 1 "iTiPago", 'Efectivo' "dDesTiPag",
            total_monto "dMonTiPag", 'PYG' "cMoneTiPag",  tipos_monedas.descripcion "dDMoneTiPag",
            ventas.subtotal0  "dSubExe", 0 "dSubExo",
            ventas.subtotal5  "dSub5", ventas.subtotal10  "dSub10",
            (subtotal0 + subtotal5 + subtotal10) "dTotOpe", 
            0 "dTotDesc", 0 "dTotDescGlotem", 0 "dTotAntItem", 0 "dTotAnt",
            0 "dPorcDescTotal", 0 "dDescTotal", 0 "dAnticipo", 0 "dRedon",
            ( total_monto - (0+0) )   "dTotGralOpe", liquidacion5 "dIVA5", liquidacion10 "dIVA10",
            total_iva "dTotIVA",  (subtotal5 - liquidacion5) "dBaseGrav5", (subtotal10 - liquidacion10) "dBaseGrav10",
            ((subtotal5 - liquidacion5) + (subtotal10 - liquidacion10)) "dTBasGraIVA"
        FROM  
            public.ventas, public.emisor, public.tipos_transacciones,
            public.tipos_impuestos, public.tipos_monedas, public.clientes,
            public.paises, public.tipos_documentos 
        WHERE 
            emisor.id = 1
            and ventas.tipo_transaccion = tipos_transacciones.tipo_transaccion
            and ventas.tipo_impuesto = tipos_impuestos.tipo_impuesto
            and ventas.tipo_moneda = tipos_monedas.tipo_moneda
            and ventas.cliente = clientes.cliente
            and clientes.pais = paises.pais
            and clientes.tipo_documento = tipos_documentos.tipo_documento
            AND venta = %s ;      

        """;              

            
        sql = String.format(sql, venta );
        
        
        return sql ;                 
    }      
            
            
    
                
    
    public String facturaDEtalles ( Integer venta ) 
            throws Exception {
    
        String sql = "";                                  
                

        sql = """          

            SELECT id "dCodInt", productos.descripcion "dDesProSer", productos.unidad_medida "cUniMed",
                unidades_medidas.unimed "dDesUniMed",  cantidad "dCantProSer", 
                ventas_detalles.precio_unitario "dPUniProSer", (cantidad * ventas_detalles.precio_unitario)  "dTotBruOpeItem",
                (cantidad * ventas_detalles.precio_unitario) "dTotOpeItem",  
                productos.afectacion_iva "iAfecIVA", afectacion_iva.descripcion  "dDesAfecIVA",
                100 "dPropIVA", tasa_iva "dTasaIVA",  base_gravada_iva "dBasGravIVA", liquidacion_iva "dLiqIVAItem",
                CASE WHEN productos.afectacion_iva = 4 THEN 0 ELSE 0 END AS "dBasExe"
            FROM public.ventas_detalles, public.productos, public.unidades_medidas,
                public.afectacion_iva 
            WHERE ventas_detalles.producto = productos.producto 
                and productos.unidad_medida = unidades_medidas.unidad_medida
                and productos.afectacion_iva = afectacion_iva.afectacion_iva 
                and venta = %s         
        """;              

            
        sql = String.format(sql, venta );
        
        
        return sql ;                 
    }      
            
            
    
    
    
}
