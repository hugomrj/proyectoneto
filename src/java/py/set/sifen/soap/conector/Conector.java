package py.set.sifen.soap.conector;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;



/**
 *
 * @author Usuario
 */
public class Conector {
    
    private RESTful rest = new RESTful();
    private String json;
    private String numero_lote;    
    private String codigo_qr;  
    private String lote_estado;
    private String lote_respuesta;
    private String cdc ;
    private String cancelar_respuesta;
    private String cancelar_mensaje;
    private String persona_nombres;
    private Integer ruc;
    private Integer digito_verificador;
    
            
    
    
    public void enviar (  Integer idVenta  ) throws Exception{
        
        try {
            
            ConectorJSON objetoJson = new ConectorJSON();            
            
            this.json = objetoJson.jsonEnviar( idVenta );           
            
    
            this.rest.setAmbiente("test");
                    
            this.rest.nombre_api = "/async/recibe";
            this.rest.conectar(this.json);
            //int statusCode = this.rest.response.statusCode();
            
            String responseBody = this.rest.response.body();
//System.out.println(responseBody);

            
            Gson gson = new Gson();
            JsonObject jsonObject = gson.fromJson(responseBody, JsonObject.class);

            this.numero_lote = jsonObject.getAsJsonObject("rResEnviLoteDe").get("dProtConsLote").getAsString();
            this.codigo_qr = jsonObject.getAsJsonObject("rResEnviLoteDe").get("codigoQR").getAsString();
            
            this.obtenerCdc(this.codigo_qr);
            
            
        }
        catch (JsonSyntaxException e) {
            //e.printStackTrace();
            System.err.println(e.getCause());
        }
           
    
    }


    
    
    
    

    
    public void enviarJSON ( String strjson   ){
        
        try {
               
            this.json = strjson;
            
 System.out.println(this.json);            
    
            this.rest.nombre_api = "/async/recibe";
            this.rest.conectar(this.json);
            //int statusCode = this.rest.response.statusCode();
            
            String responseBody = this.rest.response.body();
System.out.println(responseBody);

            
            Gson gson = new Gson();
            JsonObject jsonObject = gson.fromJson(responseBody, JsonObject.class);

            this.numero_lote = jsonObject.getAsJsonObject("rResEnviLoteDe").get("dProtConsLote").getAsString();
            this.codigo_qr = jsonObject.getAsJsonObject("rResEnviLoteDe").get("codigoQR").getAsString();
            
            this.obtenerCdc(this.codigo_qr);
            
            
        }
        catch (JsonSyntaxException e) {
            //e.printStackTrace();
            System.err.println(e.getCause());
        }
           
    
    }
    
    
    
    
    

    
    
    public void cosultar_lote ( String numero_lote   ){
        
        try {
                        
            this.json = "{  \n" +
                        "   \"lote\" : \""+numero_lote+"\"\n" +
                        "}   " ;

            
            this.rest.nombre_api = "/consulta/lote";
            this.rest.conectar(this.json);

            //int statusCode = this.rest.response.statusCode();            
            String responseBody = this.rest.response.body();
            


            Gson gson = new Gson();
            JsonObject jsonObject = gson.fromJson(responseBody, JsonObject.class);

            
                                                          
            this.lote_estado = jsonObject.getAsJsonObject("rResEnviConsLoteDe")
                                .getAsJsonObject("gResProcLote")
                                .get("dEstRes")
                                .getAsString();
            
            
            this.lote_respuesta = jsonObject.getAsJsonObject("rResEnviConsLoteDe")
                                .getAsJsonObject("gResProcLote")
                                .getAsJsonObject("gResProc")
                                .get("dMsgRes")
                                .getAsString();

            
        }
        catch (JsonSyntaxException e) {
            System.err.println(e.getCause());
        }
           
    
    }




    

    
    
    public void cancelar ( String cdc, String motivo   ){
        
        try {
                        
            this.json = "{  \n" +
                        "   \"Id\" : \""+cdc+"\",\n" +
                        "   \"mOtEve\":\""+motivo+"\"\n" +
                        "}    " ;

            
            this.rest.nombre_api = "/evento/cancelar";
            this.rest.conectar(this.json);

            //int statusCode = this.rest.response.statusCode();            
            String responseBody = this.rest.response.body();
                        
            Gson gson = new Gson();
            JsonObject jsonObject = gson.fromJson(responseBody, JsonObject.class);

            
            this.cancelar_respuesta =  jsonObject.getAsJsonObject("rRetEnviEventoDe")
                                      .getAsJsonObject("gResProcEVe")
                                      .get("dEstRes")
                                      .getAsString();
            
      
            
            this.cancelar_mensaje = jsonObject.getAsJsonObject("rRetEnviEventoDe")
                                      .getAsJsonObject("gResProcEVe")
                                      .getAsJsonObject("gResProc")
                                      .get("dMsgRes")
                                      .getAsString();
                                                          
            
        }
        catch (JsonSyntaxException e) {
            System.err.println(e.getCause());
        }
           
    
    }

    


    

public void consulta_ruc(Integer ruc) {
    String responseBody = "";

    // Verificar que ruc sea mayor a 4 dígitos
    if (ruc != null && ruc.toString().length() > 4) {
        try {
            this.json = "{\n" +
                        "   \"ruc\" : \"" + ruc + "\"\n" +
                        "}   ";

            this.rest.nombre_api = "/consulta/ruc";
            this.rest.conectar(this.json);

            responseBody = this.rest.response.body();
            System.out.println(responseBody);
         
            Gson gson = new Gson();
            JsonObject jsonObject = gson.fromJson(responseBody, JsonObject.class);         
         
            
            String dCodRes = jsonObject.getAsJsonObject("rResEnviConsRUC")
                                      .get("dCodRes")
                                      .getAsString();

            
            if (dCodRes.equals("0502")){
            
                
                
                this.persona_nombres = jsonObject.getAsJsonObject("rResEnviConsRUC")
                                      .getAsJsonObject("xContRUC")
                                      .get("dRazCons")
                                      .getAsString();                

                
                
                this.digito_verificador = jsonObject.getAsJsonObject("rResEnviConsRUC")
                                         .getAsJsonObject("xContRUC")
                                         .get("RUCdv")
                                         .getAsInt();   
                this.ruc = ruc;
                
            }
            else{
                this.persona_nombres = "";
                this.digito_verificador = 0;
                this.ruc = 0;
            }
            
            
            
        } catch (JsonSyntaxException e) {
            System.err.println(e.getCause());
        }
    } else {
        System.err.println("El valor de ruc debe ser mayor a 4 dígitos.");
    }
}
    
    
    
    

    public String obtener_factura_xml ( String cdc   ){
        
        String responseBody = "";
        try {
                        
            this.json = "{  \n" +
                        "   \"cdc\" : \""+cdc+"\"\n" +
                        "}   " ;

            
            this.rest.nombre_api = "/consulta/de/xml";
            this.rest.conectar(this.json);

            //int statusCode = this.rest.response.statusCode();            
            responseBody = this.rest.response.body();

            
        }
        catch (JsonSyntaxException e) {
            System.err.println(e.getCause());
        }
        finally{
            return responseBody;
        }
           
    }
        
    
    
        
    
    public String getJson() {
        return json;
    }

    public String getNumero_lote() {
        return numero_lote;
    }

    public String getCodigo_qr() {
        return codigo_qr;
    }


    public String getCdc() {
        return cdc;
    }
    

    public void obtenerCdc(String urlqr) {
        
        String ret = "";
                
        int idIndex = urlqr.indexOf("Id=");        
        if (idIndex != -1) {
          
            idIndex += 3;
            int ampersandIndex = urlqr.indexOf('&', idIndex);

            // Si no se encuentra "&", tomar todo el resto de la cadena
            if (ampersandIndex == -1) {
                ampersandIndex = urlqr.length();
            }
            ret = urlqr.substring(idIndex, ampersandIndex);
        } 
        
        this.cdc = ret;
    }

    public String getCancelar_respuesta() {
        return cancelar_respuesta;
    }

    public String getCancelar_mensaje() {
        return cancelar_mensaje;
    }

    public String getLote_estado() {
        return lote_estado;
    }

    public String getLote_respuesta() {
        return lote_respuesta;
    }

    public String getPersona_nombres() {
        return persona_nombres;
    }

    public void setPersona_nombres(String persona_nombres) {
        this.persona_nombres = persona_nombres;
    }

    public Integer getRuc() {
        return ruc;
    }

    public Integer getDigito_verificador() {
        return digito_verificador;
    }

    
    
    
    
    
}
