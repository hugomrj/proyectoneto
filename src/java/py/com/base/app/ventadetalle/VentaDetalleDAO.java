
package py.com.base.app.ventadetalle;

import nebuleuse.ORM.db.Persistencia;
import org.json.JSONArray;
import org.json.JSONObject;
import py.com.base.app.producto.Producto;

/**
 *
 * @author hugo
 */
public class VentaDetalleDAO {
        
    Persistencia persistencia = new Persistencia();
    
    
    public Integer insertardet (Integer venta,  JSONArray detalleArray ) throws Exception {    

        Integer ret = 0;
        
        // Ahora puedes trabajar con el array detalleArray
        for (int i = 0; i < detalleArray.length(); i++) {
            
            JSONObject detalleObj = detalleArray.getJSONObject(i);
                        
            VentaDetalle ventaDetalle = new VentaDetalle();  
            
            ventaDetalle.setId(0);
            ventaDetalle.setVenta(venta);
            
            Producto producto = new Producto();
            producto.setProducto(detalleObj.getInt("producto"));
            ventaDetalle.setProducto(producto);
            
            ventaDetalle.setPrecio_unitario(detalleObj.getLong("precio_unitario"));            
            
            ventaDetalle.setCantidad(detalleObj.getInt("cantidad"));            
            ventaDetalle.setIva0(detalleObj.getLong("iva0"));
            ventaDetalle.setIva5(detalleObj.getLong("iva5"));
            ventaDetalle.setIva10(detalleObj.getLong("iva10"));
            
     
            if (ventaDetalle.getIva0() != 0) {
                ventaDetalle.setBase_gravada_iva(0.0f);
                ventaDetalle.setLiquidacion_iva(0.0f);       
            }
            else if (ventaDetalle.getIva5() != 0) {                
                ventaDetalle.setBase_gravada_iva((float) (  (ventaDetalle.getIva5() * (100 / 100)) / 1.05 )  );                
                ventaDetalle.setLiquidacion_iva( (float) (ventaDetalle.getBase_gravada_iva() * (5.0 / 100)) );                   
            }   
            else if (ventaDetalle.getIva10() != 0) {                
                ventaDetalle.setBase_gravada_iva((float) (  (ventaDetalle.getIva10() * (100 / 100)) / 1.1 )  );
                ventaDetalle.setLiquidacion_iva( (float) (ventaDetalle.getBase_gravada_iva() * (10.0 / 100)) );                   
            }
            
                       
            ventaDetalle = (VentaDetalle) this.persistencia.insert(ventaDetalle) ; 
            
            ret = i;
        }        

        return ret;
    }
    
    
    
}


    
    
















