
package py.com.base.app.ventadetalle;

import py.com.base.app.producto.Producto;

/**
 *
 * @author hugo
 */
public class VentaDetalle {
    
    private Integer id;
    private Integer venta;
    private Producto producto;
    private Integer cantidad;
    private Long precio_unitario;
    private Long iva0;
    private Long iva5;
    private Long iva10;
    
    private Float base_gravada_iva;
    private Float liquidacion_iva;
       
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Long getPrecio_unitario() {
        return precio_unitario;
    }

    public void setPrecio_unitario(Long precio_unitario) {
        this.precio_unitario = precio_unitario;
    }

    public Long getIva0() {
        return iva0;
    }

    public void setIva0(Long iva0) {
        this.iva0 = iva0;
    }

    public Long getIva5() {
        return iva5;
    }

    public void setIva5(Long iva5) {
        this.iva5 = iva5;
    }

    public Long getIva10() {
        return iva10;
    }

    public void setIva10(Long iva10) {
        this.iva10 = iva10;
    }

    public Integer getVenta() {
        return venta;
    }

    public void setVenta(Integer venta) {
        this.venta = venta;
    }

    public Float getBase_gravada_iva() {
        return base_gravada_iva;
    }

    public void setBase_gravada_iva(Float base_gravada_iva) {
        this.base_gravada_iva = base_gravada_iva;
    }

    public Float getLiquidacion_iva() {
        return liquidacion_iva;
    }

    public void setLiquidacion_iva(Float liquidacion_iva) {
        this.liquidacion_iva = liquidacion_iva;
    }


    
    
}

