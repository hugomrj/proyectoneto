/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.app.ventadetalle;


import java.io.IOException;
import java.sql.ResultSet;
import java.util.List;
import nebuleuse.ORM.db.Coleccion;
import nebuleuse.ORM.db.JsonObjeto;
import nebuleuse.ORM.db.ResultadoSet;
import nebuleuse.ORM.sql.SentenciaSQL;
import org.json.JSONArray;
import org.json.JSONObject;


public class VentaDetalleJSON  {


    
    
    public VentaDetalleJSON ( ) throws IOException  {
        
        
        
    }
      
  
    
            

    public JSONArray  detalle ( Integer venta ) {
        
        JSONArray jsonArray = new JSONArray();                
        
        try 
        {               
            
            ResultadoSet resSet = new ResultadoSet();                      
            
            String sql = "";
                                  
            sql =  new VentaDetalleSQL().detalle(venta);
            
            
            ResultSet rsData = resSet.resultset(sql);                            
            
            List<VentaDetalle>  lista = new Coleccion<VentaDetalle>().resultsetToList(
                    new VentaDetalle(),
                    rsData );     
                                    
            // Crear un array JSON            
            for (VentaDetalle registro : lista) {                
                JSONObject registroJson = new JSONObject(registro);                            
                jsonArray.put(registroJson);
            }
       
               

            resSet.close();
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonArray ;         
        }
    }      
    
    
        
    
        
}
