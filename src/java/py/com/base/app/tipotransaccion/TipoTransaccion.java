/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
 
package py.com.base.app.tipotransaccion;


public class TipoTransaccion {
    
    private Integer tipo_transaccion;
    private String descripcion;

    public Integer getTipo_transaccion() {
        return tipo_transaccion;
    }

    public void setTipo_transaccion(Integer tipo_transaccion) {
        this.tipo_transaccion = tipo_transaccion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }


}
