/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.app.emisor;

import java.util.Date;

/**
 *
 * @author hugo
 */
public class Emisor {
    
    private Integer id;
    private Integer numero_timbrado;
    private Date fecha_inicio;
    private Integer ruc_emisor;
    private Integer digitov_emisor;
    private Integer tipo_contribuyente;
    private String nombre_emisor;    
    private String nombre_fantasia;
    private String direccion_emisor;
    private Integer numero_casa;
    private Integer departamento_emisor;
    private String departamento_descripcion_emisor;
    private Integer distrito_emisor;
    private String distrito_descripcion_emisor;
    private Integer ciudad_emisor;
    private String ciudad_descripcion_emisor;
    private String telefono_emisor;
    private String email_emisor;
    private String actividad_emisor ;
    private String actividad_descripcion_emisor;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNumero_timbrado() {
        return numero_timbrado;
    }

    public void setNumero_timbrado(Integer numero_timbrado) {
        this.numero_timbrado = numero_timbrado;
    }

    public Date getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(Date fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public Integer getRuc_emisor() {
        return ruc_emisor;
    }

    public void setRuc_emisor(Integer ruc_emisor) {
        this.ruc_emisor = ruc_emisor;
    }

    public Integer getDigitov_emisor() {
        return digitov_emisor;
    }

    public void setDigitov_emisor(Integer digitov_emisor) {
        this.digitov_emisor = digitov_emisor;
    }

    public Integer getTipo_contribuyente() {
        return tipo_contribuyente;
    }

    public void setTipo_contribuyente(Integer tipo_contribuyente) {
        this.tipo_contribuyente = tipo_contribuyente;
    }

    public String getNombre_emisor() {
        return nombre_emisor;
    }

    public void setNombre_emisor(String nombre_emisor) {
        this.nombre_emisor = nombre_emisor;
    }

    public String getNombre_fantasia() {
        return nombre_fantasia;
    }

    public void setNombre_fantasia(String nombre_fantasia) {
        this.nombre_fantasia = nombre_fantasia;
    }

    public String getDireccion_emisor() {
        return direccion_emisor;
    }

    public void setDireccion_emisor(String direccion_emisor) {
        this.direccion_emisor = direccion_emisor;
    }

    public Integer getNumero_casa() {
        return numero_casa;
    }

    public void setNumero_casa(Integer numero_casa) {
        this.numero_casa = numero_casa;
    }

    public Integer getDepartamento_emisor() {
        return departamento_emisor;
    }

    public void setDepartamento_emisor(Integer departamento_emisor) {
        this.departamento_emisor = departamento_emisor;
    }

    public String getDepartamento_descripcion_emisor() {
        return departamento_descripcion_emisor;
    }

    public void setDepartamento_descripcion_emisor(String departamento_descripcion_emisor) {
        this.departamento_descripcion_emisor = departamento_descripcion_emisor;
    }

    public Integer getDistrito_emisor() {
        return distrito_emisor;
    }

    public void setDistrito_emisor(Integer distrito_emisor) {
        this.distrito_emisor = distrito_emisor;
    }

    public String getDistrito_descripcion_emisor() {
        return distrito_descripcion_emisor;
    }

    public void setDistrito_descripcion_emisor(String distrito_descripcion_emisor) {
        this.distrito_descripcion_emisor = distrito_descripcion_emisor;
    }

    public Integer getCiudad_emisor() {
        return ciudad_emisor;
    }

    public void setCiudad_emisor(Integer ciudad_emisor) {
        this.ciudad_emisor = ciudad_emisor;
    }

    public String getCiudad_descripcion_emisor() {
        return ciudad_descripcion_emisor;
    }

    public void setCiudad_descripcion_emisor(String ciudad_descripcion_emisor) {
        this.ciudad_descripcion_emisor = ciudad_descripcion_emisor;
    }

    public String getTelefono_emisor() {
        return telefono_emisor;
    }

    public void setTelefono_emisor(String telefono_emisor) {
        this.telefono_emisor = telefono_emisor;
    }

    public String getEmail_emisor() {
        return email_emisor;
    }

    public void setEmail_emisor(String email_emisor) {
        this.email_emisor = email_emisor;
    }

    public String getActividad_emisor() {
        return actividad_emisor;
    }

    public void setActividad_emisor(String actividad_emisor) {
        this.actividad_emisor = actividad_emisor;
    }

    public String getActividad_descripcion_emisor() {
        return actividad_descripcion_emisor;
    }

    public void setActividad_descripcion_emisor(String actividad_descripcion_emisor) {
        this.actividad_descripcion_emisor = actividad_descripcion_emisor;
    }
    
}

