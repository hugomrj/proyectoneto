/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.com.base.app.emisor;



import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.seguridad.Autentificacion;



/**
 * REST Web Service
 * @author hugo
 */


@Path("emisores")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)



public class EmisorWS {

    @Context
    private UriInfo context;    
    private Persistencia persistencia = new Persistencia();   
    private Autentificacion autorizacion = new Autentificacion();
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   
    private Response.Status status  = Response.Status.OK;
    
    String json = "";    
    Emisor com = new Emisor();       
                         
    public EmisorWS() {
    }

            
    
    @GET
    // @Path("/")
    public Response get() {
                     
        try 
        {                  
            //if (autorizacion.verificar(strToken))
            if (true)
            {
                //autorizacion.actualizar();        
                
                                
                this.com = (Emisor) persistencia.filtrarId(this.com, 1 );  
                
                String json = gson.toJson(this.com);
                
                if (this.com == null){
                    this.status = Response.Status.NO_CONTENT;                           
                }
                
                return Response
                        .status( this.status )                        
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .build();       
            }
            else{
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build(); 
            }
        
        }     
        catch (Exception ex) {
           
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")                    
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }        
        
    }    
      
        
        

    
    
}