/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
 
package py.com.base.app.tipoimpuesto;


public class TipoImpuesto {
    
    private Integer tipo_impuesto;    
    private String descripcion;

    public Integer getTipo_impuesto() {
        return tipo_impuesto;
    }

    public void setTipo_impuesto(Integer tipo_impuesto) {
        this.tipo_impuesto = tipo_impuesto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }



}
