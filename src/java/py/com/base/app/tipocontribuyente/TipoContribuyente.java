/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
 
package py.com.base.app.tipocontribuyente;



public class TipoContribuyente {
    
    private Integer tipo_contribuyente;
    private String descripcion;

    public Integer getTipo_contribuyente() {
        return tipo_contribuyente;
    }

    public void setTipo_contribuyente(Integer tipo_contribuyente) {
        this.tipo_contribuyente = tipo_contribuyente;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }


}
