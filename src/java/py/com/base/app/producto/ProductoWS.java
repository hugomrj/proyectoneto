/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.com.base.app.producto;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.sql.SQLException;
import java.util.List;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.MatrixParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.seguridad.Autentificacion;


/**
 * REST Web Service
 * @author hugo
 */


@Path("productos")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)



public class ProductoWS {

    @Context
    private UriInfo context;    
    private Persistencia persistencia = new Persistencia();   
    private Autentificacion autorizacion = new Autentificacion();
    private Gson gson = new Gson();          
    private Response.Status status  = Response.Status.OK;
    
    Producto componente = new Producto();       
                         
    
    
    public ProductoWS() {
    }

    
    
    
    @GET    
    public Response lista ( 
            @HeaderParam("token") String strToken,
            @MatrixParam("q") String q,            
            @QueryParam("page") Integer page) {
        
        
            if (page == null) {                
                page = 1;
            }

        try {                    
           
            //if (autorizacion.verificar(strToken))
            if (true)
            {                
                // autorizacion.actualizar();                                
                
                JsonObject jsonObject ;                                                
                if (q == null) {                                
                    jsonObject = new ProductoJSON().lista(page, null);
                } 
                else{
                    jsonObject = new ProductoJSON().lista(page, q);
                }                
                
                
                return Response
                        .status(Response.Status.OK)
                        .entity(jsonObject.toString())
                        //.header("token", autorizacion.encriptar())
                        .build();                       
            }
            else{
                
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();   
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", null)
                    .build();                                        
        }      
    }    
    
      
    
    

    
    
    
    @GET
    @Path("/{id}")
    public Response get(     
            @HeaderParam("token") String strToken,
            @PathParam ("id") Integer id ) {
                     
        try 
        {                  
            if (true)
            //if (autorizacion.verificar(strToken))
            {
                //autorizacion.actualizar();       
                
                this.componente  = (Producto) persistencia.filtrarId(this.componente, id);  
                
                String json = gson.toJson(this.componente);    
                
                                
                if (this.componente == null){
                    this.status = Response.Status.NO_CONTENT;                           
                }

                
                return Response
                        .status( this.status )                        
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .build();       
            }
            else{
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();   
            }
        
        }     
        catch (Exception ex) {            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .header("token", autorizacion.encriptar())
                    .build();           
        }          
    }    
      
        
    
    


 
 
    @POST
    public Response add( 
            @HeaderParam("token") String strToken,
             String json ) {
                     
        
        try {                    
           
            
            if (true)
            //if (autorizacion.verificar(strToken))
            {                
                //autorizacion.actualizar();    
                
                Producto req = gson.fromJson(json, Producto.class);                   
                
                this.componente = (Producto) persistencia.insert(req);                 
                
                if (this.componente == null){
                    this.status = Response.Status.NO_CONTENT;
                }                                
                
                json = gson.toJson(this.componente);
                
                return Response
                        .status(this.status)
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .build();                       
            }
            else
            {                                
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();                   
                
            }
        
        }     
        catch (Exception ex) {
            
            System.err.println(ex.getMessage());
            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }        

    }    
 
    
        
    

         

    @PUT    
    @Path("/{id}")    
    public Response edit (            
            @HeaderParam("token") String strToken,
            @PathParam ("id") Integer id,
            String json 
            ) {

        try {  
            
            System.out.println("recibe");
            System.out.println(json);
           
            //if (autorizacion.verificar(strToken))
            if (true)
            {                
                //autorizacion.actualizar();    
                
                Producto req = new Gson().fromJson(json, Producto.class);                                      
                req.setProducto(id);
                
                this.componente = (Producto) persistencia.update(req);
                json = gson.toJson(this.componente);    
            
                
    
            System.out.println("retorna");
            System.out.println(json);
                       
                
                
                return Response
                        .status(Response.Status.OK)
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .build();       
            }
            else{    
            
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();                   
                
            }
        
        }     
        
        
        
        catch (SQLException ex) {   
            return Response
                    .status(Response.Status.BAD_GATEWAY)
                    .entity(ex.getMessage())
                    .header("token", autorizacion.encriptar())
                    .build();    
        }        
                
        
        
        catch (Exception ex) {

            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();         
        }        
        
        
        
    }    
    
    
    

    

    @DELETE  
    @Path("/{id}")    
    public Response delete (            
            @HeaderParam("token") String strToken,
            @PathParam ("id") Integer id) {
            
        try {                    
           
            //if (autorizacion.verificar(strToken))
            if (true)                
            {                
                //autorizacion.actualizar();    
            
                Integer filas = 0;
                filas = persistencia.delete(this.componente, id) ;
                                
                if (filas != 0){
                    
                    return Response
                            .status(Response.Status.OK)
                            .entity(null)
                            .header("token", autorizacion.encriptar())
                            .build();                       
                }
                else{                    
                    
                    return Response
                            .status(Response.Status.NO_CONTENT)
                            .entity(null)
                            .header("token", autorizacion.encriptar())
                            .build();          
                    
                    
                }
            }
            else
            {  
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();              
            }        
        } 

        catch (Exception ex) {            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .header("token", autorizacion.encriptar())
                    .build();           
        }  
        
    }    
        
    
    
    
 

    
    

    
    @GET    
    @Path("/search") 
    public Response all (
        @HeaderParam("token") String strToken,
        @MatrixParam("q") String q
    ) {        
        
        try {                    
           
            //if (autorizacion.verificar(strToken))
            if (true)
            {                
                //autorizacion.actualizar();
                String jsonString = new ProductoJSON().search(q);                
                
                return Response
                        .status(Response.Status.OK)
                        .entity(jsonString)
                        //.header("token", autorizacion.encriptar())                        
                        .build();                       
            }
            else
            {                
                return Response
                        .status(Response.Status.UNAUTHORIZED)
                        .header("token", null)
                        .build();    
            }        
        }   
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .header("token", null)
                    .build();                                        
        }     
        
    }    
    

    
        
    
    
    

    
}