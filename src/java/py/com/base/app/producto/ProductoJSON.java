/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.app.producto;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.List;
import nebuleuse.ORM.db.Coleccion;
import nebuleuse.ORM.db.JsonObjeto;
import nebuleuse.ORM.db.ResultadoSet;
import nebuleuse.ORM.sql.SentenciaSQL;
import org.json.JSONArray;
import org.json.JSONObject;


public class ProductoJSON  {


    
    
    public ProductoJSON ( ) throws IOException  {
        
        
        
    }
      
            

    public JsonObject  lista ( Integer page, String buscar ) {
        
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
        JsonObject jsonObject = new JsonObject();
        
        try 
        {               
            
            ResultadoSet resSet = new ResultadoSet();                      
            
            String sql = "";
            String sqlFiltro = "";            
            String sqlOrder = "";
            
            if (buscar == null) {                
                sql = SentenciaSQL.select(new Producto());    
            }
            else {                
                sql =  SentenciaSQL.select(new Producto());   
                sqlFiltro =  new ProductoSQL().filtro(buscar);   ;       
            }        
            
            sqlOrder =  " order by producto";            
            sql = sql + sqlFiltro + sqlOrder ;           
            
            ResultSet rsData = resSet.resultset(sql, page);                            
            
            List<Producto>  lista = new Coleccion<Producto>().resultsetToList(
                    new Producto(),
                    rsData );     
            String jsonlista = gson.toJson( lista ); 
            
            //JsonArray jsonarrayDatos = new JsonArray();
            JsonArray jsonarrayDatos = JsonParser.parseString(jsonlista).getAsJsonArray();
            
            // paginacipon
            JsonObject jsonPaginacion = new JsonObject();            
            jsonPaginacion = new JsonObjeto().json_paginacion(sql, page);
                        
            // union de partes
            jsonObject.add("paginacion", jsonPaginacion);            
            jsonObject.add("datos", jsonarrayDatos);    
            //jsonObject.add("summary", jsonarraySuma);        
            
            
            resSet.close();
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    
    
    
    

    public String  search ( String buscar ) {

        String retorno = "";         
        
        try 
        {                           
            ResultadoSet resSet = new ResultadoSet();                      
            
            String sql = new ProductoSQL().search(buscar);

            
            ResultSet rsData = resSet.resultset(sql);                            

            List<Producto>  lista = new Coleccion<Producto>().resultsetToList(
                    new Producto(),
                    rsData );               
            
            // Crear un array JSON
            JSONArray jsonArray = new JSONArray();
            for (Producto registro : lista) {                
                JSONObject registroJson = new JSONObject(registro);                            
                jsonArray.put(registroJson);
            }            
            
            retorno = jsonArray.toString();    
            

            resSet.close();
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {            
            return retorno ;         
        }
    }      
    
    
            
    
    
        
}
