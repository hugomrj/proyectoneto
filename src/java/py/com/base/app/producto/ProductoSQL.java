/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.app.producto;

import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;

/**
 *
 * @author hugo
 */
public class ProductoSQL {
    
    
    public String search ( String buscar )
            throws Exception {
    
        String sql = "";       
                
        if (buscar == null || buscar.isEmpty() || buscar.length() == 0) {            
            buscar = "1 = 2 " ;   
        } 
        else {            
            buscar = buscar.replace(" ", "%") ;               
        }        
        
        sql = SentenciaSQL.select( new Producto(), buscar );      
               
        
        return sql ;             
    }        
           
    
        
     
    public String filtro (String buscar )
            throws Exception {
    
        String sql = "";                                 
        
        if (buscar != null) {
            buscar = buscar.replace(" ", "%") ;    
        }        
        
        ReaderT reader = new ReaderT("Producto");
        reader.fileExt = "filtro.sql";
        
        sql = reader.get( buscar );    
        
        return sql ;             
    
    }   
       
    
    
}
