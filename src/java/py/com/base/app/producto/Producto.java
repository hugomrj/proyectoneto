/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
 
package py.com.base.app.producto;

import py.com.base.app.afectacioniva.AfectacionIva;
import py.com.base.app.tasaiva.TasaIva;
import py.com.base.app.unidadmedida.UnidadMedida;


public class Producto {
    
    private Integer producto;
    private String descripcion;
    private UnidadMedida unidad_medida;
    private Long precio_unitario;
    private AfectacionIva afectacion_iva;
    private TasaIva tasa_iva ;
    private Boolean precio_variable; 

    public Integer getProducto() {
        return producto;
    }

    public void setProducto(Integer producto) {
        this.producto = producto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public UnidadMedida getUnidad_medida() {
        return unidad_medida;
    }

    public void setUnidad_medida(UnidadMedida unidad_medida) {
        this.unidad_medida = unidad_medida;
    }

    public Long getPrecio_unitario() {
        return precio_unitario;
    }

    public void setPrecio_unitario(Long precio_unitario) {
        this.precio_unitario = precio_unitario;
    }

    public AfectacionIva getAfectacion_iva() {
        return afectacion_iva;
    }

    public void setAfectacion_iva(AfectacionIva afectacion_iva) {
        this.afectacion_iva = afectacion_iva;
    }

    public TasaIva getTasa_iva() {
        return tasa_iva;
    }

    public void setTasa_iva(TasaIva tasa_iva) {
        this.tasa_iva = tasa_iva;
    }

    public Boolean getPrecio_variable() {
        return precio_variable;
    }

    public void setPrecio_variable(Boolean precio_variable) {
        this.precio_variable = precio_variable;
    }
    
    
    



}

//SELECT producto, descripcion, unidad_medida, precio_unitario, codigo_iva, tasa_iva