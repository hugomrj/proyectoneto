/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
 
package py.com.base.app.naturalezareceptor;

public class NaturalezaReceptor {
    
       
    private Integer naturaleza_receptor;
    private String descripcion;

    public Integer getNaturaleza_receptor() {
        return naturaleza_receptor;
    }

    public void setNaturaleza_receptor(Integer naturaleza_receptor) {
        this.naturaleza_receptor = naturaleza_receptor;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
