/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.app.tipodocumento;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.List;
import nebuleuse.ORM.db.Coleccion;
import nebuleuse.ORM.db.ResultadoSet;
import nebuleuse.ORM.sql.SentenciaSQL;


public class TipoDocumentoJSON  {


    
    
    public TipoDocumentoJSON ( ) throws IOException  {
       
        
    }
      
            

    public String  all ( ) {
        
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
        JsonObject jsonObject = new JsonObject();
        String jsonlista = ""; 
        
        try 
        {               
            
            ResultadoSet resSet = new ResultadoSet();                      
            
            String sql = "";
            String sqlFiltro = " where tipo_documento <> 0  ";            
            String sqlOrder = "";                                
            sql = SentenciaSQL.select(new TipoDocumento());    
            
            //sqlOrder =  " order by cliente";            
            sql = sql + sqlFiltro + sqlOrder ;           
            
            ResultSet rsData = resSet.resultset(sql);                            
            
            List<TipoDocumento>  lista = new Coleccion<TipoDocumento>().resultsetToList(
                    new TipoDocumento(),
                    rsData );     
            jsonlista = gson.toJson( lista ); 
            
     
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {            
            return jsonlista ;         
        }
    }      
    
    
    
        
}
