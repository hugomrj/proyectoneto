/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
 
package py.com.base.app.tipodocumento;


public class TipoDocumento {
    
    private Integer tipo_documento;    
    private String descripcion;

    public Integer getTipo_documento() {
        return tipo_documento;
    }

    public void setTipo_documento(Integer tipo_documento) {
        this.tipo_documento = tipo_documento;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }


}
