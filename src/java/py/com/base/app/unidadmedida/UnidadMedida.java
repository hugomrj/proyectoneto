/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
 
package py.com.base.app.unidadmedida;


public class UnidadMedida {
    
    private Integer unidad_medida;
    private String unimed;
    private String descripcion;

    public Integer getUnidad_medida() {
        return unidad_medida;
    }

    public void setUnidad_medida(Integer unidad_medida) {
        this.unidad_medida = unidad_medida;
    }

    public String getUnimed() {
        return unimed;
    }

    public void setUnimed(String unimed) {
        this.unimed = unimed;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}

