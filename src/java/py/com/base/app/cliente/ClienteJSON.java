/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.app.cliente;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.List;
import nebuleuse.ORM.db.Coleccion;
import nebuleuse.ORM.db.JsonObjeto;
import nebuleuse.ORM.db.ResultadoSet;
import nebuleuse.ORM.sql.SentenciaSQL;
import org.json.JSONArray;
import org.json.JSONObject;



public class ClienteJSON  {

   
    
    public ClienteJSON ( ) throws IOException  {
        
        
        
    }
      
            

    public JsonObject  lista ( Integer page, String buscar ) {
        
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
        JsonObject jsonObject = new JsonObject();
        
        try 
        {               
            
            ResultadoSet resSet = new ResultadoSet();                      
            
            String sql = "";
            String sqlFiltro = "";            
            String sqlOrder = "";
            
            if (buscar == null) {                
                sql = SentenciaSQL.select(new Cliente());    
            }
            else {                
                sql =  SentenciaSQL.select(new Cliente());   
                sqlFiltro =  new ClienteSQL().filtro(buscar);   ;       
            }        
            
            sqlOrder =  " order by cliente";            
            sql = sql + sqlFiltro + sqlOrder ;           
            
            
            ResultSet rsData = resSet.resultset(sql, page);                            
            
            List<Cliente>  lista = new Coleccion<Cliente>().resultsetToList(
                    new Cliente(),
                    rsData );     
            String jsonlista = gson.toJson( lista ); 
            
            //JsonArray jsonarrayDatos = new JsonArray();
            JsonArray jsonarrayDatos = JsonParser.parseString(jsonlista).getAsJsonArray();
            
            // paginacipon
            JsonObject jsonPaginacion = new JsonObject();            
            jsonPaginacion = new JsonObjeto().json_paginacion(sql, page);
                    
            
            // union de partes
            jsonObject.add("paginacion", jsonPaginacion);            
            jsonObject.add("datos", jsonarrayDatos);    
            //jsonObject.add("summary", jsonarraySuma);                    
            
            resSet.close();
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    
    
    
       
      
            

    public String  all ( Integer naturalesa, String buscar ) {

        String retorno = "";         
        
        try 
        {                           
            ResultadoSet resSet = new ResultadoSet();                      
            
            String sql = new ClienteSQL().all(naturalesa, buscar);

            
            ResultSet rsData = resSet.resultset(sql);                            

            List<Cliente>  lista = new Coleccion<Cliente>().resultsetToList(
                    new Cliente(),
                    rsData );               
            
            // Crear un array JSON
            JSONArray jsonArray = new JSONArray();
            for (Cliente registro : lista) {                
                JSONObject registroJson = new JSONObject(registro);                            
                jsonArray.put(registroJson);
            }            
            
            retorno = jsonArray.toString();    
            

            resSet.close();
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {            
            return retorno ;         
        }
    }      
    
    
        
    
    
    public String  por_ruc ( Integer ruc ) {    
        String retorno = "";     
        
                
        try 
        {                           
            ResultadoSet resSet = new ResultadoSet();                      
            
            String sql = new ClienteSQL().por_ruc(ruc);                                
            
            ResultSet rsData = resSet.resultset(sql);                            

            
            
            List<Cliente>  lista = new Coleccion<Cliente>().resultsetToList(
                    new Cliente(),
                    rsData );               
            
            // Crear un array JSON
            JSONArray jsonArray = new JSONArray();
            for (Cliente registro : lista) {                
                JSONObject registroJson = new JSONObject(registro);                            
                jsonArray.put(registroJson);
            }            
            
            //retorno = jsonArray.toString();    
            
            // Verificar si el JSONArray no está vacío
            if (!jsonArray.isEmpty()) {
                // Obtener el primer y único JSONObject del JSONArray            
                JSONObject jsonObject = jsonArray.getJSONObject(0);
                retorno = jsonObject.toString();              
            } 


            resSet.close();
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {            
            return retorno ;         
        }
        
        
    }
    
     
    
    
    public String  por_doc_nro ( Integer doc_nro ) {    
        String retorno = "";     
        
                
        try 
        {                           
            ResultadoSet resSet = new ResultadoSet();                      
            
            String sql = new ClienteSQL().por_doc_nro(doc_nro);                                
            
            ResultSet rsData = resSet.resultset(sql);                            
            
            List<Cliente>  lista = new Coleccion<Cliente>().resultsetToList(
                    new Cliente(),
                    rsData );               
            
            // Crear un array JSON
            JSONArray jsonArray = new JSONArray();
            for (Cliente registro : lista) {                
                JSONObject registroJson = new JSONObject(registro);                            
                jsonArray.put(registroJson);
            }            
            
            // Verificar si el JSONArray no está vacío
            if (!jsonArray.isEmpty()) {
                // Obtener el primer y único JSONObject del JSONArray            
                JSONObject jsonObject = jsonArray.getJSONObject(0);
                retorno = jsonObject.toString();              
            } 
            
            resSet.close();
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {            
            return retorno ;         
        }
        
        
    }
    
            
    
}
