/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.app.cliente;

import py.com.base.app.departamento.Departamento;
import py.com.base.app.naturalezareceptor.NaturalezaReceptor;
import py.com.base.app.pais.Pais;
import py.com.base.app.tipocontribuyente.TipoContribuyente;
import py.com.base.app.tipodocumento.TipoDocumento;

/**
 *
 * @author hugo
 */
public class Cliente {
    
    private Integer cliente;
    private NaturalezaReceptor naturaleza_receptor;
    private Pais pais;
    private TipoContribuyente tipo_contribuyente;
    private Integer ruc;
    private Integer digitov;
    private TipoDocumento tipo_documento;
    private Integer numero_documento;
    private String razon_social;
    private String nombre_fantasia;    
    private String direccion;
    private String numero_casa;
    private Departamento departamento;
    private Integer distrito;
    private Integer ciudad;
    private String telefono;
    private String celular;
    private String correo;

    public Integer getCliente() {
        return cliente;
    }

    public void setCliente(Integer cliente) {
        this.cliente = cliente;
    }
        

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public TipoContribuyente getTipo_contribuyente() {
        return tipo_contribuyente;
    }

    public void setTipo_contribuyente(TipoContribuyente tipo_contribuyente) {
        this.tipo_contribuyente = tipo_contribuyente;
    }

    public Integer getRuc() {
        return ruc;
    }

    public void setRuc(Integer ruc) {
        this.ruc = ruc;
    }

    public Integer getDigitov() {
        return digitov;
    }

    public void setDigitov(Integer digitov) {
        this.digitov = digitov;
    }



    public Integer getNumero_documento() {
        return numero_documento;
    }

    public void setNumero_documento(Integer numero_documento) {
        this.numero_documento = numero_documento;
    }

    public String getRazon_social() {
        return razon_social;
    }

    public void setRazon_social(String razon_social) {
        this.razon_social = razon_social;
    }

    public String getNombre_fantasia() {
        return nombre_fantasia;
    }

    public void setNombre_fantasia(String nombre_fantasia) {
        this.nombre_fantasia = nombre_fantasia;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getNumero_casa() {
        return numero_casa;
    }

    public void setNumero_casa(String numero_casa) {
        this.numero_casa = numero_casa;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public Integer getDistrito() {
        return distrito;
    }

    public void setDistrito(Integer distrito) {
        this.distrito = distrito;
    }

    public Integer getCiudad() {
        return ciudad;
    }

    public void setCiudad(Integer ciudad) {
        this.ciudad = ciudad;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public NaturalezaReceptor getNaturaleza_receptor() {
        return naturaleza_receptor;
    }

    public void setNaturaleza_receptor(NaturalezaReceptor naturaleza_receptor) {
        this.naturaleza_receptor = naturaleza_receptor;
    }

    public TipoDocumento getTipo_documento() {
        return tipo_documento;
    }

    public void setTipo_documento(TipoDocumento tipo_documento) {
        this.tipo_documento = tipo_documento;
    }

        
}




