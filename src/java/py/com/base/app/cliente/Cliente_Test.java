/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.base.app.cliente;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import java.io.IOException;
import nebuleuse.ORM.db.Persistencia;





public class Cliente_Test 
{ 
    public static void main(String[] args) throws IOException, Exception 
    { 
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   
        Persistencia persistencia = new Persistencia();   
        String json = "";

       
        json = "{\n" +
"   \"cliente\":\"0\",\n" +
"   \"ruc\":\"0\",\n" +
"   \"digitov\":\"0\",\n" +
"   \"numero_documento\":\"0\",\n" +
"   \"razon_social\":\"\",\n" +
"   \"nombre_fantasia\":\"\",\n" +
"   \"direccion\":\"\",\n" +
"   \"numero_casa\":\"0\",\n" +
"   \"naturaleza_receptor\":{\n" +
"      \"naturaleza_receptor\":\"1\"\n" +
"   },\n" +
"   \"pais\":{\n" +
"      \"pais\":\"PRY\"\n" +
"   },\n" +
"   \"tipo_contribuyente\":{\n" +
"      \"tipo_contribuyente\":\"1\"\n" +
"   },\n" +
"   \"departamento\":{\n" +
"      \"departamento\":\"1\"\n" +
"   }\n" +
"}";
        
        
        Cliente cliente = new Cliente();
                
        
        Cliente req = gson.fromJson(json, Cliente.class);                   
        cliente = (Cliente) persistencia.insert(req);            
        
        
        /*
        JsonObject jsonObject ;                                                        
        jsonObject = new ClienteJSON().lista(1, null);
        System.out.println(jsonObject.toString());
        */

        
        
    }
}




