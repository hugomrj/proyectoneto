/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.app.cliente;


import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;

/**
 *
 * @author hugo
 */
public class ClienteSQL {
    
    
    public String search ( String busqueda )
            throws Exception {
    
        String sql = "";                                 
        sql = SentenciaSQL.select( new Cliente(), busqueda );        
        
        return sql ;             
    }        
           
    
        
     
    public String filtro (String buscar )
            throws Exception {
    
        String sql = "";                                 
        
        if (buscar != null) {
            buscar = buscar.replace(" ", "%") ;    
        }        
        
        ReaderT reader = new ReaderT("Categoria");
        reader.fileExt = "filtro.sql";
        
        sql = reader.get( buscar );    
        
        return sql ;             
        
        
    
    }   
    
    
    
    
    public String all (Integer naturaleza, String buscar )
            throws Exception {
    
        String sql = "";                  
                
                
        if (buscar == null || buscar.isEmpty() || buscar.length() == 0 ) {
            
            
            sql = """
                SELECT cliente, naturaleza_receptor,  tipo_contribuyente, ruc, digitov, 
                tipo_documento, numero_documento, 
                razon_social
                FROM public.clientes
                where 1 = 2
            """;      

            
        } 
        else {
            
            buscar = buscar.replace(" ", "%") ;   
            
            sql = """
                SELECT cliente, naturaleza_receptor,  tipo_contribuyente, ruc, digitov, 
                tipo_documento, numero_documento, 
                razon_social
                FROM public.clientes
                where clientes.naturaleza_receptor  = %s
                and razon_social ilike  '%%%s%%'
            """;               
            
            sql = String.format(sql, naturaleza, buscar);
            
        }

        return sql ;                 
    }       
       
    
    
    
    
    public String por_ruc (Integer ruc )
            throws Exception {
    
        String sql = "";                  
                
        
        if (ruc == null || ruc.equals(0)) {
            
            
            sql = """                  
                SELECT *
                FROM public.clientes
                where 1 = 2
            """;               
            
        } 
        else {

            sql = """                  
                SELECT cliente, naturaleza_receptor,  tipo_contribuyente, ruc, digitov, 
                tipo_documento, numero_documento, 
                razon_social
                FROM public.clientes
                where clientes.naturaleza_receptor  = 1
                and ruc =  %s
            """;              

            sql = String.format(sql, ruc);
            
        }
        
        return sql ;                 
    }       
       
        
    
    
    
    public String por_doc_nro (Integer doc_nro )
            throws Exception {
    
        String sql = "";                  
                
        
        if (doc_nro == null ) {

            sql = """                  
                SELECT *
                FROM public.clientes
                where 1 = 2
            """;               

        } 
        else {

            sql = """                  
                SELECT cliente, naturaleza_receptor,  tipo_contribuyente, ruc, digitov, 
                tipo_documento, numero_documento, 
                razon_social
                FROM public.clientes
                where clientes.naturaleza_receptor  = 2                
                and numero_documento  =  %s
            """;              

            
            sql = String.format(sql, doc_nro);
            
        }
        
        return sql ;                 
    }       
       
        
    
    
    
}
