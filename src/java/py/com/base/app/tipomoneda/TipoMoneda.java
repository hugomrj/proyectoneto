/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
 
package py.com.base.app.tipomoneda;


public class TipoMoneda {
    
    private Integer tipo_moneda;
    private String moneda_operacion;
    private String descripcion;

    public Integer getTipo_moneda() {
        return tipo_moneda;
    }

    public void setTipo_moneda(Integer tipo_moneda) {
        this.tipo_moneda = tipo_moneda;
    }

    public String getMoneda_operacion() {
        return moneda_operacion;
    }

    public void setMoneda_operacion(String moneda_operacion) {
        this.moneda_operacion = moneda_operacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }


}
