
package py.com.base.app.afectacioniva;

/**
 *
 * @author hugo
 */
public class AfectacionIva {
    
    private Integer afectacion_iva;
    private String descripcion;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getAfectacion_iva() {
        return afectacion_iva;
    }

    public void setAfectacion_iva(Integer afectacion_iva) {
        this.afectacion_iva = afectacion_iva;
    }
    
    
    
}
