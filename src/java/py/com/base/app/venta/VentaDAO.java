
package py.com.base.app.venta;


import java.sql.ResultSet;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.db.ResultadoSet;
import org.json.JSONObject;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import org.json.JSONArray;
import py.com.base.app.cliente.Cliente;
import py.com.base.app.tipoimpuesto.TipoImpuesto;
import py.com.base.app.tipomoneda.TipoMoneda;
import py.com.base.app.tipotransaccion.TipoTransaccion;
import py.com.base.app.ventadetalle.VentaDetalleDAO;

/**
 *
 * @author hugo
 */
public class VentaDAO {
        
    Persistencia persistencia = new Persistencia();  
    
    
    public Integer insertar( String json) throws Exception {
               
        Venta venta = new Venta();        
        venta.setVenta(0);
                
        JSONObject jsonObject = new JSONObject(json);
        
        
        venta.setEstablecimiento(jsonObject.getString("establecimiento") );        
        venta.setPunto_expedicion( jsonObject.getString("punto_expedicion") );
        
        Integer facturaNumero = this.getUltimoNumeroFactura(
                jsonObject.getString("establecimiento"), 
                jsonObject.getString("punto_expedicion") );
        
        // colocar un control al llegar al 9,999,999
        facturaNumero ++;        
        venta.setFactura_numero( facturaNumero );

        
        
        TipoTransaccion tipotransaccion = new TipoTransaccion();
        tipotransaccion.setTipo_transaccion( jsonObject.getInt("tipo_transaccion"));        
        venta.setTipo_transaccion( tipotransaccion );
        
        
                
        String fecha_emision = jsonObject.getString("fecha_emision");        
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate fechaOriginal = LocalDate.parse(fecha_emision, formatter);
        LocalDateTime fechaEmisionConHoraActual = LocalDateTime.of(fechaOriginal, LocalTime.now());    
        venta.setFecha_emision(fechaEmisionConHoraActual);
        
        
        
        //tipo_impuesto
        TipoImpuesto tipoimpuesto = new TipoImpuesto();
        tipoimpuesto.setTipo_impuesto( jsonObject.getInt("tipo_impuesto"));
        venta.setTipo_impuesto(tipoimpuesto);
        
        
        
        TipoMoneda tipomoneda = new TipoMoneda();
        tipomoneda.setTipo_moneda( jsonObject.getInt("tipo_moneda") );
        venta.setTipo_moneda(tipomoneda);
        
        
        
        Cliente cliente = new Cliente();
        cliente.setCliente( jsonObject.getInt("cliente") );
        venta.setCliente(cliente);

        
        // insertar cabecera
        venta = (Venta) this.persistencia.insert(venta) ;            
        
        // detalles 
        // Supongamos que tienes el objeto JSONObject llamado jsonObject
        JSONArray detalleArray = jsonObject.getJSONArray("detalle");

        VentaDetalleDAO detalleDAO = new VentaDetalleDAO();                  
        detalleDAO.insertardet( venta.getVenta(),  detalleArray);
        
        
        
        // calculos totales
        this.calculo_totales(venta);
        
        
        
        return venta.getVenta();
    }
    
    
    
    
    
    
    
    public Integer getUltimoNumeroFactura( String establecimiento, String punto_expedicion ) 
            throws Exception {
    
        Integer facturaNumero = 0;                
        try 
        {                           
            ResultadoSet resSet = new ResultadoSet();                      
            
            String sql = new VentaSQL().getUltimoNumeroFactura(establecimiento, punto_expedicion);                       
            
            ResultSet rsData = resSet.resultset(sql);                                        
            
            if (rsData.next()) {
                facturaNumero = rsData.getInt("factura_numero");
            }            
            resSet.close();
        }         
        catch (Exception ex) {                               
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {            
            return facturaNumero ;         
        }
        
    }
    
    
    
    
    
    
    public boolean calculo_totales ( Venta venta ) 
            throws Exception {
    
        String sql = new VentaSQL().calculo_totales(venta.getVenta());        
        
        boolean bool = persistencia.ejecutarSQL(sql);
        
        return bool;
    }    
  
    
}















