/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.app.venta;

import java.time.LocalDateTime;
import py.com.base.app.cliente.Cliente;
import py.com.base.app.tipoimpuesto.TipoImpuesto;
import py.com.base.app.tipomoneda.TipoMoneda;
import py.com.base.app.tipotransaccion.TipoTransaccion;


/**
 *
 * @author hugo
 */
public class Venta {
    
    private Integer venta;
    
    private String establecimiento;
    private String punto_expedicion;
    private Integer factura_numero;
    
    private TipoTransaccion tipo_transaccion;
    private LocalDateTime fecha_emision;
    private TipoImpuesto tipo_impuesto;
    private TipoMoneda tipo_moneda;
    private Cliente cliente;

    private Long subtotal0;
    private Long subtotal5;    
    private Long subtotal10;    
    private Long total_monto;    
    private Long liquidacion5;    
    private Long liquidacion10;    
    private Long total_iva;   
    
    
    
    public Integer getVenta() {
        return venta;
    }

    public void setVenta(Integer venta) {
        this.venta = venta;
    }

    public TipoTransaccion getTipo_transaccion() {
        return tipo_transaccion;
    }

    public void setTipo_transaccion(TipoTransaccion tipo_transaccion) {
        this.tipo_transaccion = tipo_transaccion;
    }

    public LocalDateTime getFecha_emision() {
        return fecha_emision;
    }

    public void setFecha_emision(LocalDateTime fecha_emision) {
        
        //System.out.println("-- entra a metodo set fecha");        
        //System.out.println(fecha_emision);                
        this.fecha_emision = fecha_emision;        
        ///System.out.println(this.fecha_emision);        
    }

    
    public TipoImpuesto getTipo_impuesto() {
        return tipo_impuesto;
    }

    public void setTipo_impuesto(TipoImpuesto tipo_impuesto) {
        this.tipo_impuesto = tipo_impuesto;
    }

    
    public TipoMoneda getTipo_moneda() {
        return tipo_moneda;
    }

    public void setTipo_moneda(TipoMoneda tipo_moneda) {
        this.tipo_moneda = tipo_moneda;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }


    public String getPunto_expedicion() {
        return punto_expedicion;
    }

    public void setPunto_expedicion(String punto_expedicion) {
        this.punto_expedicion = punto_expedicion;
    }

    public Integer getFactura_numero() {
        return factura_numero;
    }

    public void setFactura_numero(Integer factura_numero) {
        this.factura_numero = factura_numero;
    }

    public Long getSubtotal0() {
        return subtotal0;
    }

    public void setSubtotal0(Long subtotal0) {
        this.subtotal0 = subtotal0;
    }

    public Long getSubtotal5() {
        return subtotal5;
    }

    public void setSubtotal5(Long subtotal5) {
        this.subtotal5 = subtotal5;
    }

    public Long getSubtotal10() {
        return subtotal10;
    }

    public void setSubtotal10(Long subtotal10) {
        this.subtotal10 = subtotal10;
    }

    public Long getTotal_monto() {
        return total_monto;
    }

    public void setTotal_monto(Long total_monto) {
        this.total_monto = total_monto;
    }

    public Long getLiquidacion5() {
        return liquidacion5;
    }

    public void setLiquidacion5(Long liquidacion5) {
        this.liquidacion5 = liquidacion5;
    }

    public Long getLiquidacion10() {
        return liquidacion10;
    }

    public void setLiquidacion10(Long liquidacion10) {
        this.liquidacion10 = liquidacion10;
    }

    public Long getTotal_iva() {
        return total_iva;
    }

    public void setTotal_iva(Long total_iva) {
        this.total_iva = total_iva;
    }

    public String getEstablecimiento() {
        return establecimiento;
    }

    public void setEstablecimiento(String establecimiento) {
        this.establecimiento = establecimiento;
    }

    
        
}





/*
SELECT venta, tipo_transaccion, fecha_emision, tipo_impuesto, codigo_moneda
	FROM public.ventas;

*/