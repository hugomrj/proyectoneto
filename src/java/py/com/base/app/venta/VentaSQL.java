/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.app.venta;

import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;

/**
 *
 * @author hugo
 */
public class VentaSQL {
    
    
    public String search ( String busqueda )
            throws Exception {
    
        String sql = "";                                 
        sql = SentenciaSQL.select( new Venta(), busqueda );        
        
        return sql ;             
    }        
           
            
     
    public String filtro (String buscar )
            throws Exception {
    
        String sql = "";                                 
        
        if (buscar != null) {
            buscar = buscar.replace(" ", "%") ;    
        }        
        
        ReaderT reader = new ReaderT("Venta");
        reader.fileExt = "filtro.sql";
        
        sql = reader.get( buscar );    
        
        return sql ;             
    
    }   
     
    
    
    
    
    
    public String getUltimoNumeroFactura ( String establecimiento, String punto_expedicion ) 
            throws Exception {
    
        String sql = "";                  
                

        sql = """          
            SELECT max(factura_numero) factura_numero
              FROM public.ventas
              where establecimiento like '%s'
              and punto_expedicion like '%s'                            
        """;              

            
        sql = String.format(sql, establecimiento, punto_expedicion);
        
        
        
        
        return sql ;                 
    }       
       
        
            
    
    
    public String calculo_totales ( Integer venta ) 
            throws Exception {
    
        String sql = "";                  
                

        sql = """          
              
              
            UPDATE public.ventas v
              SET  
                subtotal0 = subquery.subtotal0,
                subtotal5 = subquery.subtotal5,
                subtotal10 = subquery.subtotal10,  
                total_monto = subquery.total_monto,
                liquidacion5 = subquery.liquidacion5,
                liquidacion10 = subquery.liquidacion10,
                total_iva = subquery.liquidacion5 + subquery.liquidacion10
              FROM (
                SELECT 
                  SUM(iva0) AS subtotal0,
                  SUM(iva5) AS subtotal5,
                  SUM(iva10) AS subtotal10,
                  SUM(iva0) + SUM(iva5) + SUM(iva10)  as total_monto,
                  SUM(iva5) / 21 as liquidacion5,
                  SUM(iva10) / 11 as liquidacion10
                FROM public.ventas_detalles
                WHERE venta = %s
              ) AS subquery
              WHERE v.venta = %s;              
             
                         
        """;              

            
        sql = String.format(sql, venta, venta );
        
        
        
        
        return sql ;                 
    }      
            
            
    
    
    
}
