/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.base.app.venta;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import java.io.IOException;
import nebuleuse.ORM.db.Persistencia;
import org.json.JSONArray;
import org.json.JSONObject;
import py.com.base.app.ventadetalle.VentaDetalleJSON;





public class Venta_Test_Get 
{ 
    public static void main(String[] args) throws IOException, Exception 
    { 
        Persistencia persistencia = new Persistencia();
        
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();  
        
        
        
        String json = "";
       
        
        Venta venta = new Venta();        
        
        venta  = (Venta) persistencia.filtrarId(venta, 43);                  
        
        JSONObject jsonVenta = new JSONObject(venta);
        
        VentaDetalleJSON jsondetalle = new VentaDetalleJSON();
       
        JSONArray jsonarray = jsondetalle.detalle(43);
        
        
        
        jsonVenta.put("detalle", jsonarray);
        
        json = jsonVenta.toString();
        
        System.out.println(json);
        
    }
}




