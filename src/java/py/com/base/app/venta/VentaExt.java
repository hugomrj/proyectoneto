/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.base.app.venta;

import java.io.IOException;

/**
 *
 * @author Usuario
 */
public class VentaExt  extends Venta{
    
    private String cliente_documento ;
            
    
    public void extender() throws IOException {
        
        VentaExt ext = this;               
        
        if (ext.getCliente().getNaturaleza_receptor().getNaturaleza_receptor() == 1 ){
            this.cliente_documento = ext.getCliente().getRuc().toString() + "-" +
                  this.getCliente().getDigitov()   ;        
        }
        else{
            // NO es contribuyente
            if (ext.getCliente().getNaturaleza_receptor().getNaturaleza_receptor() == 2 ){
                this.cliente_documento = ext.getCliente().getNumero_documento().toString() ;        
            }       
        
        }
        
        
        
    }

    public String getCliente_documento() {
        return cliente_documento;
    }


    
}
