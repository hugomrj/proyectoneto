/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.app.venta;




import java.io.IOException;
import java.sql.ResultSet;
import java.util.List;
import nebuleuse.ORM.db.Coleccion;
import nebuleuse.ORM.db.JsonObjeto;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.db.ResultadoSet;
import nebuleuse.ORM.sql.SentenciaSQL;
import org.json.JSONArray;
import org.json.JSONObject;
import py.com.base.app.ventadetalle.VentaDetalleJSON;


public class VentaJSON  {

    Persistencia persistencia = new Persistencia();  
    
    
    public VentaJSON ( ) throws IOException  {
        
    }
      
  
                

    public JSONObject  lista ( Integer page, String buscar ) {
        
                
        JSONObject jsonObject = new JSONObject();        
        
        try 
        {               
            
            ResultadoSet resSet = new ResultadoSet();                      
            
            String sql = "";
            String sqlFiltro = "";            
            String sqlOrder = "";
            
            if (buscar == null) {                
                sql = SentenciaSQL.select(new Venta());    
            }
            else {                
                sql =  SentenciaSQL.select(new Venta());   
                sqlFiltro =  new VentaSQL().filtro(buscar);   ;       
            }        
            
            sqlOrder =  " order by venta";            
            sql = sql + sqlFiltro + sqlOrder ;           
            
            ResultSet rsData = resSet.resultset(sql, page);                            
            
            List<VentaExt>  lista = new Coleccion<VentaExt>().resultsetToList(
                    new VentaExt(),
                    rsData );     
            
                        
            // Crear un array JSON
            JSONArray jsonArrayDatos = new JSONArray();
            for (Venta registro : lista) {                
                JSONObject registroJson = new JSONObject(registro);                            
                jsonArrayDatos.put(registroJson);
            }

            // Crear un objeto JSON para la paginación (simulando json_paginacion)
            JSONObject jsonPaginacion = new JSONObject();            
            jsonPaginacion = new JsonObjeto().JSON_paginacion(sql, page);
                        

            jsonObject.put("paginacion", jsonPaginacion);
            jsonObject.put("datos", jsonArrayDatos);

               

            resSet.close();
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    
    
        
    
    
    public JSONObject  registro ( Integer codigo_venta ) throws Exception {   
    
       
        Venta venta = new Venta();        
        
        venta  = (Venta) persistencia.filtrarId(venta, codigo_venta);                  
        
        JSONObject jsonVenta = new JSONObject(venta);
        
        VentaDetalleJSON jsondetalle = new VentaDetalleJSON();
       
        JSONArray jsonarray = jsondetalle.detalle(codigo_venta);
        
        jsonVenta.put("detalle", jsonarray);
        
        return jsonVenta;
    
    }
    
    
    
    
    
    
        
}
