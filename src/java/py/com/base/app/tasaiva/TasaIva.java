/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
 
package py.com.base.app.tasaiva;



public class TasaIva {
    
    private Integer tasa_iva;
    private String descripcion;

    public Integer getTasa_iva() {
        return tasa_iva;
    }

    public void setTasa_iva(Integer tasa_iva) {
        this.tasa_iva = tasa_iva;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}



