/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.com.jasper.reporte.kude;



import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.File;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import nebuleuse.ORM.postgres.Conexion;

//import nebuleuse.seguridad.Autentificacion;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import py.com.jasper.reporte.qr.QRCodeGenerator;



@WebServlet(name = "Kude_Reporte", 
        urlPatterns = {"/Kude/Reporte/factura.pdf"})


public class Kude_Reporte extends HttpServlet {


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

              
        
            HttpSession sesion = request.getSession();       
            String qrFilePath = "";

            
        
        try { 
        
                Integer par_numero = 0;
                par_numero = Integer.parseInt( request.getParameter("num").toString() ) ;  
                            
                /*
                Autentificacion autorizacion = new Autentificacion();
                String strToken =  (String) sesion.getAttribute("sesiontoken");
                */
                                
                //if (autorizacion.verificar(strToken))
                if (true)
                {                      

                    
                    String archivo_jrxml = "Kude01.jrxml";
                    
                    String archivo_pdf = "Kude01";
                    response.setHeader("Content-disposition","inline; filename="+archivo_pdf+".pdf");
                    response.setContentType("application/pdf");

            //response.setContentType("image/jpeg");            
            //Response.Status status  = Response.Status.OK;

            
                    Conexion cnx = new Conexion();
                    cnx.conectar();
                                                    

                    // Obtener la ruta del archivo Jasper en la carpeta WEB-INF/jasper
                    String jasperPath = "/WEB-INF/jasper/"+archivo_jrxml;                    
                    InputStream jrxmlStream = getServletContext().getResourceAsStream(jasperPath);
                    
                    
                    if (jrxmlStream != null) {

                        JasperDesign jasperDesign = JRXmlLoader.load(jrxmlStream);
                        JasperReport report = JasperCompileManager.compileReport(jasperDesign);
                       
                    
                        // parametros
                        Map<String, Object> parameters = new HashMap<String, Object>();
                        parameters.put("par_numero", par_numero );  
                        //parameters.put("report_path", "/WEB-INF/jasper" );    
                        
                        
                        String report_path = request.getServletContext().getRealPath("/WEB-INF")+"/jasper";
                        report_path = report_path.replace("\\", "/") ;                                                
                        parameters.put("report_path", report_path );    
                        
                        
                        
                        // obtener codigo link qr
                        
                        String sql = "SELECT cdc, codigo_qr FROM respuesta_sifen WHERE venta = " + par_numero;                        
                        Statement statement = cnx.getConexion().createStatement();                                     
                        ResultSet resultset = statement.executeQuery(sql);
                        String codigoQR = "";
                        String cdc = "";
                        
                        // Iterar sobre los resultados
                        while (resultset.next()) {                            
                            cdc = resultset.getString("cdc");
                            codigoQR = resultset.getString("codigo_qr");
                        }                                                
                        
                        // QR                        
                        
                        int width = 400;
                        int height = 400;   
                        String qrfileName = cdc+".png";                         
                        String folderPath = report_path+ "/qr";                         
                        qrFilePath = QRCodeGenerator.generateQRCode(codigoQR, folderPath, qrfileName, width, height);                        
                        
                        parameters.put("qrFilePath", qrFilePath ); 
                                                
/*                                                
                        Convercion conversion = new Convercion();            
                        parameters.put("par_monto_letras", "" );
*/

                        // Llenar el informe Jasper
                        JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, cnx.getConexion());

                        // Obtener el flujo de salida del servlet
                        ServletOutputStream servletOutputStream = response.getOutputStream();

                        // Exportar el informe Jasper a PDF y escribirlo en el flujo de salida
                        JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);

                        // Cerrar el flujo de salida
                        servletOutputStream.flush();
                        servletOutputStream.close();

                        // Cerrar la conexión a la base de datos
                        cnx.desconectar();                        
                        
                        
/*
                        ServletOutputStream servletOutputStream = response.getOutputStream();
                        byte[] reportePdf = JasperExportManager.exportReportToPdf(jasperPrint);

                        response.setContentLength(reportePdf.length);

                        servletOutputStream.write(reportePdf, 0, reportePdf.length);
                        servletOutputStream.flush();
                        servletOutputStream.close();                            

                        cnx.desconectar();            
*/
                        
                    } 
                    
                }
                else
                {  
  //                  response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                }
                
                
//                autorizacion.actualizar();

        
            
        }         
        /*
        catch (JRException ex) 
        {
            Logger.getLogger(Kude_Reporte.class.getName()).log(Level.SEVERE, null, ex);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        } 
        */
        catch (Exception ex) {
            Logger.getLogger(Kude_Reporte.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {          
            
            File qrFile = new File(qrFilePath);
            new File(qrFilePath).delete();            
            
            sesion.invalidate();
        }

            
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Kude_Reporte.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Kude_Reporte.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
