
package py.com.jasper.reporte.qr;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;

public class QRCodeGenerator {

    
    public static String generateQRCode(String data, String folderPath, String fileName, int width, int height) {
        
        Path pathfile = null;
        try {
            
            BitMatrix bitMatrix = new MultiFormatWriter().encode(data, BarcodeFormat.QR_CODE, width, height);
            
            // Crea un directorio si no existe
            File folder = new File(folderPath);
            if (!folder.exists()) {
                folder.mkdirs();
            }
            
            pathfile = FileSystems.getDefault().getPath(folderPath, fileName);
            MatrixToImageWriter.writeToPath(bitMatrix, "PNG", pathfile);
            
            // Establecer permisos de lectura para todos
            File qrFile = pathfile.toFile();
            if (qrFile.exists()) {
                qrFile.setReadable(true, false);                
            } 
            
        } catch (WriterException | IOException e) {
            e.printStackTrace();
        }
        finally{
            return pathfile.toString();
        }
    }

    

    public static void main(String[] args) {

        //if (args.length == 0) {
        if (false) {
            System.out.println("Debes proporcionar un parámetro con el contenido para generar el QR.");        

        } 
        else {

            //String data = args[0]; // El primer argumento es el contenido del QR
            
            String  data = "mara";
            
            String url = data ;
            String idValue = "err";        
            String[] urlParts = url.split("&");        

            for (String part : urlParts) {
                if (part.startsWith("Id=")) {        
                    idValue = part.substring(3); // 3 es la longitud de "Id="
                    break;
                }
            }

            String folderPath = "imagenqr"; // Nombre de la carpeta donde se guardarán los archivos QR        
            String fileName = idValue+".png"; // Nombre del archivo QR
            int width = 400;
            int height = 400;

            generateQRCode(data, folderPath, fileName, width, height);
        }
    }
    
    
}
