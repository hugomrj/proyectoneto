/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.api.config.REST;

import java.util.Set;
import jakarta.ws.rs.core.Application;

/**
 *
 * @author hugo
 */


@jakarta.ws.rs.ApplicationPath("api")
public class ApplicationJAXRS extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }
    
    

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        
              
        resources.add(py.com.base.app.afectacioniva.AfectacionIvaWS.class);
        resources.add(py.com.base.app.cliente.ClienteWS.class);
        resources.add(py.com.base.app.departamento.DepartamentoWS.class);
        resources.add(py.com.base.app.emisor.EmisorWS.class);
        resources.add(py.com.base.app.naturalezareceptor.NaturalezaReceptorWS.class);
        resources.add(py.com.base.app.pais.PaisWS.class);
        resources.add(py.com.base.app.producto.ProductoWS.class);
        resources.add(py.com.base.app.tasaiva.TasaIvaWS.class);
        resources.add(py.com.base.app.tipocontribuyente.TipoContribuyenteWS.class);
        resources.add(py.com.base.app.tipodocumento.TipoDocumentoWS.class);
        resources.add(py.com.base.app.tipoimpuesto.TipoImpuestoWS.class);
        resources.add(py.com.base.app.tipomoneda.TipoMonedaWS.class);
        resources.add(py.com.base.app.tipotransaccion.TipoTransaccionWS.class);
        resources.add(py.com.base.app.unidadmedida.UnidadMedidaWS.class);
        resources.add(py.com.base.app.usuario.UsuarioWS.class);
        resources.add(py.com.base.app.venta.VentaWS.class);
        resources.add(py.com.grafico.panelinicial.PanelInicialWS.class);
        
        
        
    }
    
}
