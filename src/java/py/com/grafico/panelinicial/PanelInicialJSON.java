/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.grafico.panelinicial;


import java.io.IOException;
import java.sql.ResultSet;
import java.util.List;
import nebuleuse.ORM.db.Coleccion;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.db.ResultadoSet;
import org.json.JSONArray;
import org.json.JSONObject;
import java.sql.ResultSetMetaData;


public class PanelInicialJSON  {

    Persistencia persistencia = new Persistencia();  
    
    
    public PanelInicialJSON ( ) throws IOException  {
        
    }
      
  
                

    public JSONArray  grafico1 ( Integer anio ) {
        
        
        JSONArray jsonArray = new JSONArray();
        
        try 
        {               
            
            ResultadoSet resSet = new ResultadoSet();                                  
            String sql = new PanelInicialSQL().grafico1(anio);
            
            ResultSet resultSet = resSet.resultset(sql);                            
            
            
            
            ResultSetMetaData metaData = resultSet.getMetaData();
            int columnCount = metaData.getColumnCount();

            

            while (resultSet.next()) {
                
                JSONObject jsonObject = new JSONObject();        

                for (int i = 1; i <= columnCount; i++) {
                    String columnName = metaData.getColumnLabel(i);
                    Object value = resultSet.getObject(i);

                    jsonObject.put(columnName, value);
                }

                jsonArray.put(jsonObject);
            }            



            resSet.close();
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonArray ;         
        }
    }      
    
    
        
    

    
    
    
    
        
}
