/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.grafico.panelinicial;

import py.com.base.app.venta.*;
import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;

/**
 *
 * @author hugo
 */
public class PanelInicialSQL {
    
    
    
    
            
    
    
    public String grafico1 ( Integer anio ) 
            throws Exception {
    
        String sql = "";                  
                

        sql = """          
                            
            WITH series AS (
                  SELECT generate_series('%s-01-01'::date, '%s-12-31'::date, '1 month')::date AS fecha
              )
              SELECT
                  EXTRACT(YEAR FROM s.fecha) AS anio,
                  EXTRACT(MONTH FROM s.fecha) AS mes,
                  COALESCE(SUM(v.total_monto), 0) AS total_monto
              FROM
                  series s
              LEFT JOIN
                  public.ventas v ON EXTRACT(YEAR FROM v.fecha_emision) = EXTRACT(YEAR FROM s.fecha) 
                                  AND EXTRACT(MONTH FROM v.fecha_emision) = EXTRACT(MONTH FROM s.fecha)
              GROUP BY
                  anio, mes
              ORDER BY
                  anio, mes;
                                                    
        """;              

            
        sql = String.format(sql, anio, anio );
        
        
        
        
        return sql ;                 
    }      
            
            
    
    
    
}
