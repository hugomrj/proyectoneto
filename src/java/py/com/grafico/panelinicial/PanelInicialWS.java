/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.com.grafico.panelinicial;

import py.com.base.app.venta.*;
import com.google.gson.Gson;
import java.sql.SQLException;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.MatrixParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.seguridad.Autentificacion;
import org.json.JSONArray;
import org.json.JSONObject;


/**
 * REST Web Service
 * @author hugo
 */


@Path("panelinicial")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)



public class PanelInicialWS {

    @Context
    private UriInfo context;    
    private Persistencia persistencia = new Persistencia();   
    private Autentificacion autorizacion = new Autentificacion();
    private Gson gson = new Gson();          
    private Response.Status status  = Response.Status.OK;
    
    Venta venta = new Venta();       
                         
    
    public PanelInicialWS() {
    }

        
    
    @GET
    @Path("/grafico1/{anio}")    
    public Response get(     
            @HeaderParam("token") String strToken,
            @PathParam ("anio") Integer anio ) {
                     
        try 
        {                  
            if (true)
            //if (autorizacion.verificar(strToken))
            {
                //autorizacion.actualizar();       
                
                
                PanelInicialJSON panel = new PanelInicialJSON();                
                JSONArray json  = panel.grafico1(anio);
                
                
                return Response
                        .status( this.status )                        
                        .entity(json.toString())
                        .header("token", autorizacion.encriptar())
                        .build();  
                
                
                
            }
            else{
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();   
            }
        
        }     
        catch (Exception ex) {            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .header("token", autorizacion.encriptar())
                    .build();           
        }          
    }    
      
        
    
    


 
 

    
    
    
    

    
}