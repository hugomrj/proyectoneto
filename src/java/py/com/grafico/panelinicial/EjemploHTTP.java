package py.com.grafico.panelinicial;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;

public class EjemploHTTP {

    public static void main(String[] args) {
        try {
            
            // URL de la página a la que quieres realizar la solicitud
            String urlString = "https://www.cambioschaco.com.py/";

            // Crear objeto URI y luego convertirlo a URL
            URI uri = new URI(urlString);
            URL url = uri.toURL();

            // Abrir conexión
            HttpURLConnection con = (HttpURLConnection) url.openConnection();

            // Configurar la solicitud GET
            con.setRequestMethod("GET");

            // Obtener la respuesta del servidor
            int responseCode = con.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {
                // Leer la respuesta del servidor
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuilder response = new StringBuilder();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                // Imprimir la respuesta
                System.out.println(response.toString());
            } else {
                System.out.println("Error al realizar la solicitud. Código de respuesta: " + responseCode);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
