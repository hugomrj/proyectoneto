--
-- PostgreSQL database dump
--

-- Dumped from database version 12.17 (Ubuntu 12.17-0ubuntu0.20.04.1)
-- Dumped by pg_dump version 12.17 (Ubuntu 12.17-0ubuntu0.20.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: afectacion_iva; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.afectacion_iva (
    afectacion_iva integer NOT NULL,
    descripcion character varying NOT NULL
);


ALTER TABLE public.afectacion_iva OWNER TO postgres;

--
-- Name: clientes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.clientes (
    cliente integer NOT NULL,
    naturaleza_receptor integer,
    pais character varying,
    tipo_contribuyente integer,
    ruc integer,
    digitov integer,
    tipo_documento integer,
    numero_documento integer,
    razon_social character varying,
    nombre_fantasia character varying,
    direccion character varying,
    numero_casa character varying,
    departamento integer,
    distrito integer,
    ciudad integer,
    telefono character varying,
    celular character varying,
    correo character varying
);


ALTER TABLE public.clientes OWNER TO postgres;

--
-- Name: clientes_cliente_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.clientes_cliente_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.clientes_cliente_seq OWNER TO postgres;

--
-- Name: clientes_cliente_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.clientes_cliente_seq OWNED BY public.clientes.cliente;


--
-- Name: condicion_operacion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.condicion_operacion (
    condicion_operacion integer NOT NULL,
    descripcion character varying
);


ALTER TABLE public.condicion_operacion OWNER TO postgres;

--
-- Name: departamentos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.departamentos (
    departamento integer NOT NULL,
    descripcion character varying
);


ALTER TABLE public.departamentos OWNER TO postgres;

--
-- Name: emisor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.emisor (
    id integer NOT NULL,
    numero_timbrado bigint,
    fecha_inicio date,
    ruc_emisor bigint,
    digitov_emisor integer,
    tipo_contribuyente integer,
    nombre_emisor character varying,
    nombre_fantasia character varying,
    direccion_emisor character varying,
    numero_casa integer,
    departamento_emisor integer,
    departamento_descripcion_emisor character varying,
    distrito_emisor integer,
    distrito_descripcion_emisor character varying,
    ciudad_emisor integer,
    ciudad_descripcion_emisor character varying,
    telefono_emisor character varying,
    email_emisor character varying,
    actividad_emisor character varying,
    actividad_descripcion_emisor character varying
);


ALTER TABLE public.emisor OWNER TO postgres;

--
-- Name: emisor_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.emisor_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.emisor_id_seq OWNER TO postgres;

--
-- Name: emisor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.emisor_id_seq OWNED BY public.emisor.id;


--
-- Name: indicador_presencia; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.indicador_presencia (
    indicador_presencia integer NOT NULL,
    descripcion character varying
);


ALTER TABLE public.indicador_presencia OWNER TO postgres;

--
-- Name: naturaleza_receptor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.naturaleza_receptor (
    naturaleza_receptor integer NOT NULL,
    descripcion character varying
);


ALTER TABLE public.naturaleza_receptor OWNER TO postgres;

--
-- Name: paises; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.paises (
    pais character varying NOT NULL,
    nombre character varying
);


ALTER TABLE public.paises OWNER TO postgres;

--
-- Name: productos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.productos (
    producto integer NOT NULL,
    descripcion character varying NOT NULL,
    unidad_medida integer NOT NULL,
    precio_unitario bigint NOT NULL,
    afectacion_iva integer NOT NULL,
    tasa_iva integer NOT NULL,
    precio_variable boolean DEFAULT false
);


ALTER TABLE public.productos OWNER TO postgres;

--
-- Name: productos_producto_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.productos_producto_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.productos_producto_seq OWNER TO postgres;

--
-- Name: productos_producto_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.productos_producto_seq OWNED BY public.productos.producto;


--
-- Name: proporcion_iva; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.proporcion_iva (
    proporcion_iva integer NOT NULL,
    descripcion character varying
);


ALTER TABLE public.proporcion_iva OWNER TO postgres;

--
-- Name: respuesta_sifen; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.respuesta_sifen (
    id integer NOT NULL,
    venta integer,
    cdc character varying,
    lote character varying,
    codigo_qr character varying,
    estado character varying,
    mensaje_lote character varying,
    estado_lote character varying
);


ALTER TABLE public.respuesta_sifen OWNER TO postgres;

--
-- Name: respuesta_sifen_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.respuesta_sifen_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.respuesta_sifen_id_seq OWNER TO postgres;

--
-- Name: respuesta_sifen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.respuesta_sifen_id_seq OWNED BY public.respuesta_sifen.id;


--
-- Name: tasa_iva; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tasa_iva (
    tasa_iva integer NOT NULL,
    descripcion character varying
);


ALTER TABLE public.tasa_iva OWNER TO postgres;

--
-- Name: tipos_contribuyentes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipos_contribuyentes (
    tipo_contribuyente integer NOT NULL,
    descripcion character varying
);


ALTER TABLE public.tipos_contribuyentes OWNER TO postgres;

--
-- Name: tipos_documentos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipos_documentos (
    tipo_documento integer NOT NULL,
    descripcion character varying
);


ALTER TABLE public.tipos_documentos OWNER TO postgres;

--
-- Name: tipos_impuestos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipos_impuestos (
    tipo_impuesto integer NOT NULL,
    descripcion character varying NOT NULL
);


ALTER TABLE public.tipos_impuestos OWNER TO postgres;

--
-- Name: tipos_monedas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipos_monedas (
    tipo_moneda integer NOT NULL,
    codigo_moneda integer NOT NULL,
    moneda_operacion character varying(5) NOT NULL,
    descripcion character varying NOT NULL
);


ALTER TABLE public.tipos_monedas OWNER TO postgres;

--
-- Name: tipos_operacion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipos_operacion (
    tipo_operacion integer NOT NULL,
    descripcion character varying NOT NULL
);


ALTER TABLE public.tipos_operacion OWNER TO postgres;

--
-- Name: tipos_pagos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipos_pagos (
    tipo_pago integer NOT NULL,
    descripcion character varying
);


ALTER TABLE public.tipos_pagos OWNER TO postgres;

--
-- Name: tipos_transacciones; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipos_transacciones (
    tipo_transaccion integer NOT NULL,
    descripcion character varying
);


ALTER TABLE public.tipos_transacciones OWNER TO postgres;

--
-- Name: unidades_medidas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.unidades_medidas (
    unidad_medida integer NOT NULL,
    unimed character varying(8) NOT NULL,
    descripcion character varying NOT NULL
);


ALTER TABLE public.unidades_medidas OWNER TO postgres;

--
-- Name: usuarios; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usuarios (
    usuario integer NOT NULL,
    cuenta character varying(100),
    clave character varying(150),
    token_iat character varying
);


ALTER TABLE public.usuarios OWNER TO postgres;

--
-- Name: usuarios_usuario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.usuarios_usuario_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuarios_usuario_seq OWNER TO postgres;

--
-- Name: usuarios_usuario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.usuarios_usuario_seq OWNED BY public.usuarios.usuario;


--
-- Name: ventas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ventas (
    venta integer NOT NULL,
    establecimiento character varying(3),
    punto_expedicion character varying(3),
    factura_numero integer,
    tipo_transaccion integer NOT NULL,
    fecha_emision timestamp with time zone NOT NULL,
    tipo_impuesto integer NOT NULL,
    tipo_moneda integer NOT NULL,
    cliente integer,
    subtotal0 bigint,
    subtotal5 bigint,
    subtotal10 bigint,
    total_monto bigint,
    liquidacion5 bigint,
    liquidacion10 bigint,
    total_iva bigint,
    condicion_operacion bigint DEFAULT 1
);


ALTER TABLE public.ventas OWNER TO postgres;

--
-- Name: ventas_detalles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ventas_detalles (
    id integer NOT NULL,
    producto integer,
    cantidad integer,
    precio_unitario bigint,
    descuento bigint DEFAULT 0,
    iva0 bigint,
    iva5 bigint,
    iva10 bigint,
    venta integer,
    base_gravada_iva numeric(18,8),
    liquidacion_iva numeric(16,8)
);


ALTER TABLE public.ventas_detalles OWNER TO postgres;

--
-- Name: ventas_detalles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ventas_detalles_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ventas_detalles_id_seq OWNER TO postgres;

--
-- Name: ventas_detalles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ventas_detalles_id_seq OWNED BY public.ventas_detalles.id;


--
-- Name: ventas_venta_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ventas_venta_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ventas_venta_seq OWNER TO postgres;

--
-- Name: ventas_venta_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ventas_venta_seq OWNED BY public.ventas.venta;


--
-- Name: clientes cliente; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clientes ALTER COLUMN cliente SET DEFAULT nextval('public.clientes_cliente_seq'::regclass);


--
-- Name: emisor id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.emisor ALTER COLUMN id SET DEFAULT nextval('public.emisor_id_seq'::regclass);


--
-- Name: productos producto; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.productos ALTER COLUMN producto SET DEFAULT nextval('public.productos_producto_seq'::regclass);


--
-- Name: respuesta_sifen id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.respuesta_sifen ALTER COLUMN id SET DEFAULT nextval('public.respuesta_sifen_id_seq'::regclass);


--
-- Name: usuarios usuario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios ALTER COLUMN usuario SET DEFAULT nextval('public.usuarios_usuario_seq'::regclass);


--
-- Name: ventas venta; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ventas ALTER COLUMN venta SET DEFAULT nextval('public.ventas_venta_seq'::regclass);


--
-- Name: ventas_detalles id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ventas_detalles ALTER COLUMN id SET DEFAULT nextval('public.ventas_detalles_id_seq'::regclass);


--
-- Data for Name: afectacion_iva; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.afectacion_iva (afectacion_iva, descripcion) FROM stdin;
1	Gravado IVA
2	Exonerado (Art. 83- Ley 125/91)
3	Exento
4	Gravado parcial (Grav-Exento)
\.


--
-- Data for Name: clientes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.clientes (cliente, naturaleza_receptor, pais, tipo_contribuyente, ruc, digitov, tipo_documento, numero_documento, razon_social, nombre_fantasia, direccion, numero_casa, departamento, distrito, ciudad, telefono, celular, correo) FROM stdin;
89	1	PRY	2	80029940	0	1	0	MISIONES CRISTIANAS EN EL PARAGUAY			0	1	\N	\N	\N	\N	\N
90	1	PRY	2	80084373	8	1	0	MV ACEROS S.A.			0	1	\N	\N	\N	\N	\N
91	1	PRY	2	80009085	3	1	0	ELECTROPAR S.A			0	1	\N	\N	\N	\N	\N
92	1	PRY	1	655483	0	1	0	ALBERTO GODOY			0	1	\N	\N	\N	\N	\N
93	1	PRY	1	5605685	0	1	0	RUTH ELIZABETH BAEZ RIOS			0	1	\N	\N	\N	\N	\N
94	1	PRY	1	2923218	0	1	0	BENITA GIMENEZ CRISTALDO			0	1	\N	\N	\N	\N	\N
95	1	PRY	1	3545742	2	1	0	OSCAR VEGA			0	1	\N	\N	\N	\N	\N
96	1	PRY	2	6362665	9	1	0	WIPAR			0	1	\N	\N	\N	\N	\N
97	1	PRY	1	2040748	3	1	0	LUIS DIAZ REISSNER			0	1	\N	\N	\N	\N	\N
99	1	PRY	1	5067275	4	1	0	LIZA MABEL ORTELLADO			0	1	\N	\N	\N	\N	\N
100	1	PRY	1	5610028	0	1	0	ARMANDO ESPINOLA			0	1	\N	\N	\N	\N	\N
101	1	PRY	1	3401779	8	1	0	VICTOR ROA			0	1	\N	\N	\N	\N	\N
102	1	PRY	1	6082371	2	1	0	WILMA AMARILLA			0	1	\N	\N	\N	\N	\N
103	1	PRY	1	2553920	5	1	0	ADA NANCY FRANCO DUARTE			0	1	\N	\N	\N	\N	\N
104	1	PRY	1	5133801	7	1	0	CLAUDIA BENITEZ			0	1	\N	\N	\N	\N	\N
106	1	PRY	1	3939295	3	1	0	ANDREA PAOLA YBAÑEZ VERA			0	1	\N	\N	\N	\N	\N
107	1	PRY	1	3932241	6	1	0	MARIA EMANUALA PORTILLO			0	1	\N	\N	\N	\N	\N
1	2	PRY	1	0	4	1	0	sin nombre			0	1	\N	\N	\N	\N	\N
109	1	PRY	1	4235658	0	1	0	DERLIS ANBRES GIMENES			0	1	\N	\N	\N	\N	\N
110	1	PRY	1	1474010	9	1	0	DANIEL VERA BAEZ			0	1	\N	\N	\N	\N	\N
112	1	PRY	1	2609442	8	1	0	CARLO VILLALBA			0	1	\N	\N	\N	\N	\N
113	1	PRY	1	6135886	0	1	0	ROSANA OCAMPOS			0	1	\N	\N	\N	\N	\N
114	1	PRY	1	5260191	9	1	0	PAOLA ROBRI GUES			0	1	\N	\N	\N	\N	\N
115	1	PRY	1	5586015	0	1	0	MARIELA BRITOS ORTIZ			0	1	\N	\N	\N	\N	\N
88	1	PRY	1	3694014	3	1	0	ANTONIO AQUINO PEREZ			0	1	\N	\N	\N	\N	\N
87	1	PRY	1	2188107	3	1	0	MARIA ELENA PENAYO			0	1	\N	\N	\N	\N	\N
55	1	PRY	1	3437941	1	0	0	Hugo Romero	fanta editado	direcc	0	1	\N	\N	\N	\N	\N
116	2	PRY	1	0	0	1	8880000	Mara Anahi			0	1	\N	\N	\N	\N	\N
\.


--
-- Data for Name: condicion_operacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.condicion_operacion (condicion_operacion, descripcion) FROM stdin;
1	Contado
2	Crédito
\.


--
-- Data for Name: departamentos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.departamentos (departamento, descripcion) FROM stdin;
1	CAPITAL
2	CONCEPCION
3	SAN PEDRO
4	CORDILLERA
5	GUAIRA
6	CAAGUAZU
7	CAAZAPA
8	ITAPUA
9	MISIONES
10	PARAGUARI
11	ALTO PARANA
12	CENTRAL
13	NEEMBUCU
14	AMAMBAY
15	PTE. HAYES
16	BOQUERON
17	ALTO PARAGUAY
18	CANINDEYU
19	CHACO
20	NUEVA ASUNCION
\.


--
-- Data for Name: emisor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.emisor (id, numero_timbrado, fecha_inicio, ruc_emisor, digitov_emisor, tipo_contribuyente, nombre_emisor, nombre_fantasia, direccion_emisor, numero_casa, departamento_emisor, departamento_descripcion_emisor, distrito_emisor, distrito_descripcion_emisor, ciudad_emisor, ciudad_descripcion_emisor, telefono_emisor, email_emisor, actividad_emisor, actividad_descripcion_emisor) FROM stdin;
1	12560666	2023-03-21	4575802	6	1	DE generado en ambiente de prueba - sin valor comercial ni fiscal	Nombre de fantasia	CALLE, ESPAÑA NRO. 737 C/ 10 DE AGOSTO	737	12	CENTRAL	164	SAN LORENZO	6010	SAN LORENZO	0985867878	magnaprieto87@gmail.com	47411	COMERCIO AL POR MENOR DE EQUIPOS INFORMÁTICOS Y SOFTWARE
\.


--
-- Data for Name: indicador_presencia; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.indicador_presencia (indicador_presencia, descripcion) FROM stdin;
1	Operación presencial
2	Operación electrónica
3	Operación telemarketing
4	Venta a domicilio
5	Operación bancaria
6	Operación cíclica
9	Otro
\.


--
-- Data for Name: naturaleza_receptor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.naturaleza_receptor (naturaleza_receptor, descripcion) FROM stdin;
1	contribuyente
2	no contribuyente
\.


--
-- Data for Name: paises; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.paises (pais, nombre) FROM stdin;
PRY	Paraguay
ARG	Argentina
BRA	Brasil
URY	Uruguay
VEN	Venezuela (República Bolivariana de)
BOL	Bolivia (Estado Plurinacional de)
CHL	Chile
COL	Colombia
ECU	Ecuador
PER	Perú
USA	Estados Unidos de América
MEX	México
\.


--
-- Data for Name: productos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.productos (producto, descripcion, unidad_medida, precio_unitario, afectacion_iva, tasa_iva, precio_variable) FROM stdin;
5	Smartphone Samsung Galaxy	77	3120000	1	10	t
8	Televisor Sony Bravia	77	5000000	1	10	f
7	Cámara Canon EOS	77	1500000	1	10	f
4	Laptop HP Pavilion	77	3500000	1	10	f
6	Zapatos Nike Air Max	77	2080000	1	10	f
9	Portátil Dell Inspiron	77	3640000	1	10	f
10	Tablet Samsung Galaxy Tab	77	1560000	1	10	f
11	Zapatillas Adidas UltraBoost	77	3120000	1	10	f
15	Teléfono iPhone 13 Pro	77	3900000	1	10	f
16	Zapatos Converse Chuck Taylor	77	2340000	1	10	f
18	Smartwatch Garmin Fenix 6	77	1820000	1	10	f
20	Bicicleta de Montaña	77	1300000	1	10	f
21	Kit de Pinturas al Óleo	77	130000	1	5	f
22	Telescopio Celestron AstroMaster	77	780000	1	10	f
24	Refrigerador Samsung de Doble Puerta	77	3120000	1	10	f
25	Juego de Sartenes Antiadherentes	77	156000	1	5	f
26	Batería de Cocina de Acero Inoxidable	77	208000	1	5	f
27	Mochila para Senderismo	77	182000	1	5	f
12	Cámara Nikon D750	77	380000	1	10	f
13	Smart TV LG OLED	77	500000	1	10	f
14	Portátil ASUS ROG Strix	77	360000	1	10	f
17	Cámara Sony Alpha 7 III	77	440000	1	10	f
51	Horno de Convección Empotrable	77	1560000	1	10	f
52	Maleta de Viaje con Ruedas	77	182000	1	5	f
53	Linterna Recargable de Alta Potencia	77	104000	1	5	f
54	Teclado Mecánico para Gaming	77	208000	1	10	f
55	Máquina de Coser Singer	77	390000	1	5	f
56	Set de Tazas de Café de Porcelana	77	78000	1	5	f
57	Bicicleta Eléctrica Plegable	77	1820000	1	10	f
58	Aspiradora de Mano Inalámbrica	77	130000	1	5	f
59	Escritorio de Estilo Industrial	77	312000	1	5	f
60	Kit de Herramientas para Jardinería	77	156000	1	5	f
61	Cafetera Espresso Automática	77	650000	1	10	f
63	Mochila Táctica Militar	77	130000	1	5	f
19	Libro "Cien años de soledad"	77	65000	1	5	f
23	Juego de Mesa "Catan" 	77	104000	1	5	f
28	Silla de Oficina Ergonómica	77	390000	1	5	f
29	Impresora Multifunción HP	77	260000	1	10	f
30	Máquina de Café Espresso	77	650000	1	10	f
31	Set de Herramientas para Carpintería	77	312000	1	5	f
32	Lámpara LED de Pie	77	208000	1	5	f
33	Colchón Memory Foam Queen	77	1560000	1	10	f
34	Laptop Lenovo ThinkPad	77	4160000	1	10	f
35	Robot Aspiradora Inteligente	77	780000	1	10	f
36	Set de Maletas de Viaje	77	312000	1	5	f
37	Kit de Micrófonos para Estudio	77	520000	1	10	f
38	Reloj Inteligente Apple Watch	77	1040000	1	10	f
39	Mesa de Centro Moderna	77	234000	1	5	f
40	Parrilla Eléctrica para Exteriores	77	208000	1	5	f
41	Bicicleta Estática de Ejercicio	77	650000	1	10	f
42	Lentes de Sol Ray-Ban Aviator	77	390000	1	5	f
43	Tocadiscos Vintage con Altavoces	77	468000	1	10	f
44	Auriculares Inalámbricos Sony	77	312000	1	10	f
45	Robot de Cocina Multifunción	77	650000	1	10	f
46	Set de Cuchillos de Chef	77	130000	1	5	f
47	Mesa de Ping Pong Plegable	77	780000	1	10	f
48	Cepillo Eléctrico para Dientes	77	78000	1	5	f
49	Sofá Reclinable de Cuero	77	2080000	1	10	f
50	Bolso de Cuero Genuino	77	260000	1	5	f
62	Silla Gamer Ergonómica	77	468000	1	5	f
\.


--
-- Data for Name: proporcion_iva; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.proporcion_iva (proporcion_iva, descripcion) FROM stdin;
0	0
30	30
50	50
100	100
\.


--
-- Data for Name: respuesta_sifen; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.respuesta_sifen (id, venta, cdc, lote, codigo_qr, estado, mensaje_lote, estado_lote) FROM stdin;
\.


--
-- Data for Name: tasa_iva; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tasa_iva (tasa_iva, descripcion) FROM stdin;
10	10 %
5	5 %
0	0 %
\.


--
-- Data for Name: tipos_contribuyentes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipos_contribuyentes (tipo_contribuyente, descripcion) FROM stdin;
1	Persona Física
2	Persona Jurídica
\.


--
-- Data for Name: tipos_documentos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipos_documentos (tipo_documento, descripcion) FROM stdin;
0	Ninguno
1	Cédula paraguaya
2	Pasaporte
3	Cédula extranjera
4	Carnet de residencia
5	Innominado
6	Tarjeta Diplomática de exoneración fiscal
9	Otro
\.


--
-- Data for Name: tipos_impuestos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipos_impuestos (tipo_impuesto, descripcion) FROM stdin;
1	IVA
2	ISC
3	Renta
4	Ninguno
5	IVA - Renta
\.


--
-- Data for Name: tipos_monedas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipos_monedas (tipo_moneda, codigo_moneda, moneda_operacion, descripcion) FROM stdin;
2	840	USD	Dólar estadounidense
1	600	PYG	Guarani
\.


--
-- Data for Name: tipos_operacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipos_operacion (tipo_operacion, descripcion) FROM stdin;
1	B2B
2	B2C
3	B2G
4	B2F
\.


--
-- Data for Name: tipos_pagos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipos_pagos (tipo_pago, descripcion) FROM stdin;
1	Efectivo
2	Cheque
3	Tarjeta de crédito
4	Tarjeta de débito
5	Transferencia
6	Giro
7	Billetera electrónica
8	Tarjeta empresarial
9	Vale
10	Retención
11	Pago por anticipo
12	Valor fiscal
13	Valor comercial
14	Compensación
17	Pago Móvil
16	Pago bancario
15	Permuta
18	Donación
21	Pago Electrónico
20	Consumo Interno
19	Promoción
99	Otro
\.


--
-- Data for Name: tipos_transacciones; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipos_transacciones (tipo_transaccion, descripcion) FROM stdin;
1	Venta de mercadería
\.


--
-- Data for Name: unidades_medidas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.unidades_medidas (unidad_medida, unimed, descripcion) FROM stdin;
55	m	Metros
2366	CPM	Costo por Mil
2329	UI	Unidad Internacional
110	M3	Metros cúbicos
77	UNI	Unidad
86	g	Gramos
89	LT	Litros
90	MG	Miligramos
91	CM	Centimetros
92	CM2	Centimetros cuadrados
93	CM3	Centimetros cubicos
94	PUL	Pulgadas
96	MM2	Milímetros cuadrados
79	kg/m2	Kilogramos s/ metro cuadrado
97	AA	Año
98	ME	Mes
99	TN	Tonelada
100	Hs	Hora
101	Mi	Minuto
104	DET	Determinación
103	Ya	Yardas
108	MT	Metros
109	M2	Metros cuadrados
95	MM	Milímetros
666	Se	Segundo
102	Di	Día
83	kg	Kilogramos
88	ML	Mililitros
625	Km	Kilómetros
660	ml	Metro lineal
885	GL	Unidad Medida Global
891	pm	Por Milaje
869	ha	Hectáreas
569	ración	Ración
\.


--
-- Data for Name: usuarios; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.usuarios (usuario, cuenta, clave, token_iat) FROM stdin;
1	admin	21232f297a57a5a743894a0e4a801fc3	1707865495266
2	demo	fe01ce2a7fbac8fafaed7c982a04e229	1709440145856
\.


--
-- Data for Name: ventas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ventas (venta, establecimiento, punto_expedicion, factura_numero, tipo_transaccion, fecha_emision, tipo_impuesto, tipo_moneda, cliente, subtotal0, subtotal5, subtotal10, total_monto, liquidacion5, liquidacion10, total_iva, condicion_operacion) FROM stdin;
\.


--
-- Data for Name: ventas_detalles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ventas_detalles (id, producto, cantidad, precio_unitario, descuento, iva0, iva5, iva10, venta, base_gravada_iva, liquidacion_iva) FROM stdin;
\.


--
-- Name: clientes_cliente_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.clientes_cliente_seq', 116, true);


--
-- Name: emisor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.emisor_id_seq', 1, false);


--
-- Name: productos_producto_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.productos_producto_seq', 63, true);


--
-- Name: respuesta_sifen_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.respuesta_sifen_id_seq', 3, true);


--
-- Name: usuarios_usuario_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.usuarios_usuario_seq', 2, true);


--
-- Name: ventas_detalles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ventas_detalles_id_seq', 90, true);


--
-- Name: ventas_venta_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ventas_venta_seq', 98, true);


--
-- Name: afectacion_iva afectacion_iva_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.afectacion_iva
    ADD CONSTRAINT afectacion_iva_pkey PRIMARY KEY (afectacion_iva);


--
-- Name: clientes clientes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clientes
    ADD CONSTRAINT clientes_pkey PRIMARY KEY (cliente);


--
-- Name: condicion_operacion condicion_operacion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.condicion_operacion
    ADD CONSTRAINT condicion_operacion_pkey PRIMARY KEY (condicion_operacion);


--
-- Name: departamentos departamentos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.departamentos
    ADD CONSTRAINT departamentos_pkey PRIMARY KEY (departamento);


--
-- Name: indicador_presencia indicador_presencia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.indicador_presencia
    ADD CONSTRAINT indicador_presencia_pkey PRIMARY KEY (indicador_presencia);


--
-- Name: naturaleza_receptor naturaleza_receptor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.naturaleza_receptor
    ADD CONSTRAINT naturaleza_receptor_pkey PRIMARY KEY (naturaleza_receptor);


--
-- Name: paises paises_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.paises
    ADD CONSTRAINT paises_pkey PRIMARY KEY (pais);


--
-- Name: productos productos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.productos
    ADD CONSTRAINT productos_pkey PRIMARY KEY (producto);


--
-- Name: proporcion_iva proporcion_iva_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.proporcion_iva
    ADD CONSTRAINT proporcion_iva_pkey PRIMARY KEY (proporcion_iva);


--
-- Name: respuesta_sifen respuesta_sifen_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.respuesta_sifen
    ADD CONSTRAINT respuesta_sifen_pkey PRIMARY KEY (id);


--
-- Name: emisor sifen_emisor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.emisor
    ADD CONSTRAINT sifen_emisor_pkey PRIMARY KEY (id);


--
-- Name: tasa_iva tasa_iva_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tasa_iva
    ADD CONSTRAINT tasa_iva_pkey PRIMARY KEY (tasa_iva);


--
-- Name: tipos_contribuyentes tipo_contribuyente_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipos_contribuyentes
    ADD CONSTRAINT tipo_contribuyente_pkey PRIMARY KEY (tipo_contribuyente);


--
-- Name: tipos_documentos tipo_documento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipos_documentos
    ADD CONSTRAINT tipo_documento_pkey PRIMARY KEY (tipo_documento);


--
-- Name: tipos_impuestos tipos_impuestos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipos_impuestos
    ADD CONSTRAINT tipos_impuestos_pkey PRIMARY KEY (tipo_impuesto);


--
-- Name: tipos_monedas tipos_monedas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipos_monedas
    ADD CONSTRAINT tipos_monedas_pkey PRIMARY KEY (tipo_moneda);


--
-- Name: tipos_operacion tipos_operacion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipos_operacion
    ADD CONSTRAINT tipos_operacion_pkey PRIMARY KEY (tipo_operacion);


--
-- Name: tipos_pagos tipos_pagos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipos_pagos
    ADD CONSTRAINT tipos_pagos_pkey PRIMARY KEY (tipo_pago);


--
-- Name: tipos_transacciones tipos_transacciones_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipos_transacciones
    ADD CONSTRAINT tipos_transacciones_pkey PRIMARY KEY (tipo_transaccion);


--
-- Name: unidades_medidas unidades_medida_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unidades_medidas
    ADD CONSTRAINT unidades_medida_pkey PRIMARY KEY (unidad_medida);


--
-- Name: unidades_medidas uniqueunimed; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unidades_medidas
    ADD CONSTRAINT uniqueunimed UNIQUE (unimed) INCLUDE (unidad_medida);


--
-- Name: usuarios usuarios_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios
    ADD CONSTRAINT usuarios_pkey PRIMARY KEY (usuario);


--
-- Name: ventas_detalles ventas_detalles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ventas_detalles
    ADD CONSTRAINT ventas_detalles_pkey PRIMARY KEY (id);


--
-- Name: ventas ventas_establicimiento_factura_numero_punto_expedicion_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ventas
    ADD CONSTRAINT ventas_establicimiento_factura_numero_punto_expedicion_key UNIQUE (establecimiento, factura_numero, punto_expedicion);


--
-- Name: ventas ventas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ventas
    ADD CONSTRAINT ventas_pkey PRIMARY KEY (venta);


--
-- Name: idx_unique_naturaleza_numero_documento; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX idx_unique_naturaleza_numero_documento ON public.clientes USING btree (naturaleza_receptor, numero_documento) WHERE (naturaleza_receptor = 2);


--
-- Name: idx_unique_naturaleza_ruc; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX idx_unique_naturaleza_ruc ON public.clientes USING btree (naturaleza_receptor, ruc) WHERE (naturaleza_receptor = 1);


--
-- Name: productos fk_aplicacioniva; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.productos
    ADD CONSTRAINT fk_aplicacioniva FOREIGN KEY (afectacion_iva) REFERENCES public.afectacion_iva(afectacion_iva) NOT VALID;


--
-- Name: productos fk_unidad_medida; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.productos
    ADD CONSTRAINT fk_unidad_medida FOREIGN KEY (unidad_medida) REFERENCES public.unidades_medidas(unidad_medida) NOT VALID;


--
-- Name: ventas ventas_cliente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ventas
    ADD CONSTRAINT ventas_cliente_fkey FOREIGN KEY (cliente) REFERENCES public.clientes(cliente);


--
-- Name: ventas ventas_tipo_impuesto_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ventas
    ADD CONSTRAINT ventas_tipo_impuesto_fkey FOREIGN KEY (tipo_impuesto) REFERENCES public.tipos_impuestos(tipo_impuesto);


--
-- Name: ventas ventas_tipo_moneda_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ventas
    ADD CONSTRAINT ventas_tipo_moneda_fkey FOREIGN KEY (tipo_moneda) REFERENCES public.tipos_monedas(tipo_moneda);


--
-- Name: ventas ventas_tipo_transaccion_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ventas
    ADD CONSTRAINT ventas_tipo_transaccion_fkey FOREIGN KEY (tipo_transaccion) REFERENCES public.tipos_transacciones(tipo_transaccion);


--
-- PostgreSQL database dump complete
--

