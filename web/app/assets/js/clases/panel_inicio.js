function PanelInicio(){
      
    this.bodyColor = getComputedStyle(document.body).getPropertyValue('--bs-body-color');
    this.card_title_color = 'rgb(170, 184, 197)';

    

}




PanelInicio.prototype.inicio = function( page ) {        
    
    var obj = this;
            
    boo.loader.inicio();
        
    // tal vez aca llame al la clase            
    fetch('modulos/panel_inicio/panel.html')
        .then(response => response.text())
        .then(html => {
            
            document.getElementById(boo.main).innerHTML = html;


            
            //fetch('https://www.cambioschaco.com.py/')
            fetch('http://www.santaritacambios.com.py/')            
            .then(response => response.text())
            .then(html => {
                // Imprimir el HTML en la consola
                console.log("hola");
                console.log(html);
            })
            .catch(error => {
                console.error('Error al obtener el contenido HTML:', error);
            });
    



            obj.tarjeta1();




           
        })
        .finally(() => {               
            boo.loader.fin();
        });    







};








PanelInicio.prototype.tarjeta1 = function(  ) {   

    var obj = this;

    // Obtener la fecha actual
    var fechaActual = new Date();
    // Obtener el año actual
    var anio = fechaActual.getFullYear();



    ajax.api = ajax.getserver() +  "/api/panelinicial/grafico1/"+anio;
    ajax.json = null;            
    ajax.promise.asyn("get").then(result => {
    

        var jsonData = JSON.parse( result.responseText ) ;    
        // Obtener los valores de "total_monto" en un array
        var wsdata = jsonData.map(function(item) {
            return item.total_monto;
        });
console.log(wsdata)        ;




        /*Chart.defaults.font.size = 10;        */
        /*Chart.defaults.datasets.line.showLine = false;*/
        Chart.defaults.backgroundColor = '#9BD0F5';
        /*Chart.defaults.borderColor = '#36A2EB';*/
        Chart.defaults.color = obj.card_title_color ;

        var datos = {
            labels: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Ene", "Feb", "Mar", "Abr", "May", "Jun"],
            datasets: [{
                label: 'Facturación Año ' + anio,
                data: wsdata,
                backgroundColor: 'rgba(75, 192, 192, 0.2)',   
                borderColor: 'rgba(75, 192, 192, 1)',     // Color del borde
                borderWidth: 0,
                fill: false,              

            }]
        };


        // Configuración del gráfico
        var configuracion = {
            type: 'bar',         
            data: datos,
            options: {
                scales: {
                    x: {
                        ticks: {
                            color: obj.bodyColor 
                        }
                    },
                    y: {
                        ticks: {
                            color: obj.bodyColor,
                            callback: function (value, index, values) {
                                // Dividir el valor por un millón y formatear usando toLocaleString
                                return (value / 1000000).toLocaleString() + 'M';
                            },
                        },
                        beginAtZero: true                    
                    },
                },
                
                layout: {
                    padding: 0
                },
                    
                responsive: true,  
                maintainAspectRatio: false  

            }
        };

        // Obtener el contexto del lienzo
        var ctx = document.getElementById('facturacionChart').getContext('2d');

        // Crear el gráfico
        var myChart = new Chart(ctx, configuracion);








    });








}